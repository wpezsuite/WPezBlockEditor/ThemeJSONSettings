## Theme JSON Settings: Color

__Settings related to Color.__

https://developer.wordpress.org/block-editor/reference-guides/theme-json-reference/theme-json-living/#color

### A Basic Example

This example replicates the color settings from the Twenty Twenty-Four theme, sans the Booleans which will all be set to false.


```

// We need to "as" these *Boolean* classes because the class name - but not namespace - is the same 
// across the different settings types. We need a unique name for each.
use WPezBlockEditor\ThemeJSONSettings\Color\ClassBooleanPropertiesDefaults as ColorBoolPropsDefs;
use WPezBlockEditor\ThemeJSONSettings\Color\ClassBooleanProperties as ColorBoolProps;
use WPezBlockEditor\ThemeJSONSettings\Color\ClassBooleanPropertiesCollection as ColorBoolPropsCollection;

// These are unique to Color.
use WPezBlockEditor\ThemeJSONSettings\Color\ClassColorCollection;
use WPezBlockEditor\ThemeJSONSettings\Color\ClassDuotoneCollection;
use WPezBlockEditor\ThemeJSONSettings\Color\ClassGradientsCollection;
use WPezBlockEditor\ThemeJSONSettings\Color\ClassPaletteCollection;

// And this will wrap up all the above into the Color settings.
use WPezBlockEditor\ThemeJSONSettings\Color\ClassColorSettingsCollection;


add_filter( 'wp_theme_json_data_user', __NAMESPACE__ . '\fn_wp_theme_json_data_user' );

fn_wp_theme_json_data_user( $theme_json ) {
	
	// Let's begin...
	
	// Instantiate the Color's Boolean Props Defaults.
	// Note: The Boolean Props Defaults class provides a "canonical" set of bool defaults for a given Settings type. Settings type is color in this case.
	// Note: This class has a single method: updatePropDefault() for updating the default values.
	// It's possible that you might want to have your own GOTO set of defaults, so you can use the updatePropDefault() method to do that.
	$new_color_bool_props_defs = new ColorBoolPropsDefs();

	// Updating a default value would look something like this. Note: It's intentionally commented out. 
	// $new_color_bool_props_defs->updatePropDefault( 'link', true );
	
	// Next, instantiate the Color's Boolean Props and pass in the instance of the Bools Defaults.
	// This class has dedicated set (and get) methods for updating (and getting) the individual property values.
	// That is, your IDE / editor will be able to autocomplete the method names for each property. You don't have to remember them.
	$new_color_bool_props = new ColorBoolProps( $new_color_bool_props_defs );
	
	// Instantiate the Color's Boolean Props Collection and pass in the instance of the Bools Props.
	// The instance of $new_color_bool_props is passed in so the Collection is able to validate
	// that all the properties are legit / valid. *Defaults are the "template" for the boolean properties.
	$new_color_bools_collect = new ColorBoolPropsCollection( $new_color_bool_props_def );
	
	// Add a collection of booleans based on $new_color_bool_props and getBooleans(), name it 'bools_0'.
	// Note: The second $arg is an array. 
	$new_color_bools_collect->add( 'bools_0', $new_color_bool_props->getBooleans() );

	// If you want to create a second collection - or third, or fourth, etc. - of booleans, you can do that as well. 

	// -- All of this ^^^^ we've seen before in other examples of other types of settings.
	// -- However, what follows is what makes this approach so powerful...

	// Instantiate the Color Collection class.
	$new_color_color_collection = new ClassColorCollection();

	// Firsh, let's add the colors. 
	// Note: The added key of 'alias'. This gets used later for Duotones and Gradients (and being able to auto-create those 'name' values).
	// Note: You can add the one by one, or chain them together as shown here.

	// This collection will be any / all colors you use for palettes, duotones, and gradients.
	// The colors in this example are from Twenty Twenty-Four, the aliases are derived from the theme's gradient names.

	// 0ff-white
	$new_color_color_collection->add(
		'base',
		array(
			'name'  => 'Base',
			'color' => '#f9f9f9',
			'slug'  => 'base',
			'alias' => 'Off-white',
		)
	)
	// white
	->add(
		'base_2',
		array(
			'name'  => 'Base / Two',
			'color' => '#ffffff',
			'slug'  => 'base-2',
			'alias' => 'White',
		)
	)
	// black
	->add(
		'contrast',
		array(
			'name'  => 'Contrast',
			'color' => '#111111',
			'slug'  => 'contrast',
			'alias' => 'Black',
		)
	)
	// Dark-pewter. This wasn't used in the gradients so the name is "invented".
	->add(
		'contrast_2',
		array(
			'name'  => 'Contrast / Two',
			'color' => '#636363',
			'slug'  => 'contrast-2',
			'alias' => 'Dark pewter',
		)
	)
	// pewter
	->add(
		'contrast_3',
		array(
			'name'  => 'Contrast / Three',
			'color' => '#A4A4A4',
			'slug'  => 'contrast-3',
			'alias' => 'Pewter',
		)
	)
	// beige
	->add(
		'accent',
		array(
			'name'  => 'Accent',
			'color' => '#cfcabe',
			'slug'  => 'accent',
			'alias' => 'Beige',
		)
	)
	// sandstone
	->add(
		'accent_2',
		array(
			'name'  => 'Accent / Two',
			'color' => '#c2a990',
			'slug'  => 'accent-2',
			'alias' => 'Sandstone',
		)
	)
	// rust
	->add(
		'accent_3',
		array(
			'name'  => 'Accent / Three',
			'color' => '#d8613c',
			'slug'  => 'accent-3',
			'alias' => 'Rust',
		)
	)
	// sage
	->add(
		'accent_4',
		array(
			'name'  => 'Accent / Four',
			'color' => '#b1c5a4',
			'slug'  => 'accent-4',
			'alias' => 'Sage',
		)
	)
	// mint
	->add(
		'accent_5',
		array(
			'name'  => 'Accent / Five',
			'color' => '#b5bdbc',
			'slug'  => 'accent-5',
			'alias' => 'Mint',
		)
	);

	// Instantiate the Color Duotone Collection class, and pass in the Color Collection instance.
	$new_color_duotone = new ClassDuotoneCollection( $new_color_color_collection );

	// There are three ways to add Duotones.

	// - Method 1 - One by one, as the colors were added above. 
	$new_color_duotone->add(
		'duotone_1',
		array(
			'colors' => array( 'contrast', 'base_2' ),
			// 'name'   => 'Black and white',   << 'name' is optional. If it's not specified the class will create one.
			'slug'   => 'duotone-1',
		)
	)->add(
		'duotone_2',
		array(
			'colors' => array( 'contrast', 'accent_2' ),
			//'name'   => 'Black and sandstone',
			'slug'   => 'duotone-2',
		)
	)

	// - Method 2 - Put the unique values in a array and foreach() over them.
	$arr_duotones = array(
		'3' => array( 'contrast', 'accent_3' ),
		'4' => array( 'contrast', 'accent_4' ),
		'5' => array( 'contrast', 'accent_5' ),
	);

	foreach ( $arr_duotones as $key => $arr ) {
		$new_color_duotone->add(
			'duotone_' . $key,
			array(
				'colors' => $arr,
				'slug'   => 'duotone-' . $key,
			)
		);
	}

	// - Method 3 - Whatever works best for you and your agency.

	// -- At this point, we've added five Duotones. --

	// Instantiate the Color Gradients Collection class, and pass in the Color Collection instance.
	$new_color_gradients = new ClassGradientsCollection( $new_color_color_collection );

	// We'll skip Method 1 and use Method 2 for adding Gradients.
	// I trust you can see how this helps keep things consistent, clean, and easy to maintain.
	// Again, this will yield the gradients as seen in Twenty Twenty-Four.

	$format_0 = 'to bottom, %s 0%%, %s 100%%';
	$name_0   = 'Vertical soft %s to %s';
	$new_color_gradients->setNameFormat( $name_0 );

	$arr_gradients = array(
		'1' => array( 'accent', 'base' ),
		'2' => array( 'accent_2', 'base' ),
		'3' => array( 'accent_3', 'base' ),
		'4' => array( 'accent_4', 'base' ),
		'5' => array( 'accent_5', 'base' ),
		'6' => array( 'contrast_3', 'base' ),
	);
	foreach ( $arr_gradients as $key => $arr ) {
		$new_color_gradients->add(
			'gradient_' . $key,
			array(
				'type'   => 'linear',
				'format' => $format_0,
				'colors' => $arr,
				'slug'   => 'gradient-' . $key,
			)
		);
	}

	$format_1 = 'to bottom, %s 50%%, %s 50%%';
	$name_1   = 'Vertical hard %s to %s';
	$new_color_gradients->setNameFormat( $name_1 );

	$arr_gradients = array(
		'7'  => array( 'accent', 'base' ),
		'8'  => array( 'accent_2', 'base' ),
		'9'  => array( 'accent_3', 'base' ),
		'10' => array( 'accent_4', 'base' ),
		'11' => array( 'accent_5', 'base' ),
		'12' => array( 'contrast_3', 'base' ),
	);

	foreach ( $arr_gradients as $key => $arr ) {
		$new_color_gradients->add(
			'gradient_' . $key,
			array(
				'type'   => 'lin',
				'format' => $format_1,
				'colors' => $arr,
				'slug'   => 'gradient-' . $key,
			)
		);
	}

	// -- Duotones and Gradients completed!  Now add a Palette. -- 

	// Instantiate the Color Palette Collection class, and pass in the Color Collection instance.
	$new_color_palette = new ClassPaletteCollection( $new_color_color_collection );

	// Since we're using all the colors from the Color Collection, we can use the getKeys() method.
	// Note: If a particular block's palette only uses a subset of the colors, you can use the
	// getKeys( $arr_remove ) method where $arr_remove is an array of the colors you don't want to use.
	//
	// Or you can write out the array of colors (keys) to use.
	$arr_color_keys = $new_color_color_collection->getKeys();

	// Use those keys to add a palette named 'palette_0'.
	$new_color_palette->add( 'palette_0', array( 'colors' => $arr_color_keys ) );

	// Instantiate the Color Settings Collection class, and pass in the various Collection instances.
	// Note:  $new_bools_collect, $new_color_palette are required.
	// Note:  $new_color_duotone, $new_color_gradients are optional.
	$new_color_settings = new ClassColorSettingsCollection( $new_bools_collect, $new_color_palette, $new_color_duotone, $new_color_gradients );

	// For adding the args are a combo of keys and arrays.
	// TODO - Refactor so that it's all keys, and no arrays?
	$new_color_settings->add(
		'color_set_0',
		array(
			'key_bools'       => 'bools_0',
			'key_palette'     => 'palette_0',
			'array_duotone'   => $new_color_duotone->getKeys(),
			'array_gradients' => $new_color_gradients->getKeys(),
		)
	);

	// And this is how you would get the Color Settings for 'color_set_0'.
	$new_color_settings->get('color_set_0' ) 

}

```

### Basic Example - Using the Factory Class

```
use WPezBlockEditor\ThemeJSONSettings\Color\ClassColorFactory;

add_filter( 'wp_theme_json_data_user', __NAMESPACE__ . '\fn_wp_theme_json_data_user' );

fn_wp_theme_json_data_user( $theme_json ) {

	$new_color_fact = new ClassColorFactory();

	$new_color_settings_coll = $new_color_fact->createSettingsCollection();

	$new_color_bool_props = $new_color_fact->getBoolProps();

	$new_color_bool_props_coll = $new_color_fact->getBoolPropsCollection();

	$new_color_bool_props_coll->add( 'bools_1', $new_color_bool_props->getBooleans() );

	$new_color_bool_props->setLink();

	$new_color_bool_props_coll->add( 'bools_2', $new_color_bool_props->getBooleans() );

	$new_color_color_coll = $new_color_fact->getColorCollection();

	// 0ff-white
	$new_color_color_coll->add(
		'base',
		array(
			'name'  => 'Base',
			'color' => '#f9f9f9',
			'slug'  => 'base',
			'alias' => 'Off-white',
		)
	)
	// white
	->add(
		'base_2',
		array(
			'name'  => 'Base / Two',
			'color' => '#ffffff',
			'slug'  => 'base-2',
			'alias' => 'White',
		)
	)
	// black
	->add(
		'contrast',
		array(
			'name'  => 'Contrast',
			'color' => '#111111',
			'slug'  => 'contrast',
			'alias' => 'Black',
		)
	)
	->add(
		'contrast_2',
		array(
			'name'  => 'Contrast / Two',
			'color' => '#636363',
			'slug'  => 'contrast-2',
			'alias' => 'Dark pewter',
		)
	)
	// pewter
	->add(
		'contrast_3',
		array(
			'name'  => 'Contrast / Three',
			'color' => '#A4A4A4',
			'slug'  => 'contrast-3',
			'alias' => 'Pewter',
		)
	)
	// beige
	->add(
		'accent',
		array(
			'name'  => 'Accent',
			'color' => '#cfcabe',
			'slug'  => 'accent',
			'alias' => 'Beige',
		)
	)
	// sandstone
	->add(
		'accent_2',
		array(
			'name'  => 'Accent / Two',
			'color' => '#c2a990',
			'slug'  => 'accent-2',
			'alias' => 'Sandstone',
		)
	)
	// rust
	->add(
		'accent_3',
		array(
			'name'  => 'Accent / Three',
			'color' => '#d8613c',
			'slug'  => 'accent-3',
			'alias' => 'Rust',
		)
	)
	// sage
	->add(
		'accent_4',
		array(
			'name'  => 'Accent / Four',
			'color' => '#b1c5a4',
			'slug'  => 'accent-4',
			'alias' => 'Sage',
		)
	)
	// mint
	->add(
		'accent_5',
		array(
			'name'  => 'Accent / Five',
			'color' => '#b5bdbc',
			'slug'  => 'accent-5',
			'alias' => 'Mint',
		)
	);

	$new_color_fact->getPaletteCollection()
	->add( 'cpalette_1', array( 'items' => array( 'base', 'contrast' ) ) )
	->add( 'cpalette_2', array( 'items' => array( 'base_2', 'contrast_2' ) ) );

	$new_color_settings_coll->add(
		'color_1',
		array(
			'key_bools' => 'bools_1',
			'key_palette' => 'cpalette_2',
		)
	)->add(
		'color_2',
		array(
			'key_bools' => 'bools_2',
			'key_palette' => 'cpalette_1',
		)
	);

	// Since it's optional, we have to "force" the create for Duotone Collection. Otherwise, it won't be created. 
	$new_color_duo_coll = $new_color_fact->createDuotoneCollection();

	$new_color_duo_item_coll = $new_color_fact->getDuotoneItemCollection();

	$arr_duotones = array(
		'1' => array( 'contrast', 'base_2' ),
		'2' => array( 'contrast', 'accent_2' ),
		'3' => array( 'contrast', 'accent_3' ),
		'4' => array( 'contrast', 'accent_4' ),
		'5' => array( 'contrast', 'accent_5' ),
	);

	$arr_dtone_test_1 = array();
	foreach ( $arr_duotones as $key => $arr ) {
		$arr_dtone_test_1[] = 'duotone_' . $key;
		$new_color_duo_item_coll->add(
			'duotone_' . $key,
			array(
				'colors' => $arr,
				'slug'   => 'duotone-' . $key,
			)
		);
	}

	$new_color_duo_coll->add( 'dtone_odd', array( 'items' => array( 'duotone_1', 'duotone_3', 'duotone_5' ) ) );
	$new_color_duo_coll->add( 'dtone_even', array( 'items' => array( 'duotone_4', 'duotone_2' ) ) );

	// Since Duotone is optional, and we've already instantiated the Settings Collection, have to set it after the fact.
	$new_color_settings_coll->setDuotone( $new_color_duo_coll );

	$new_color_settings_coll->add(
		'color_3',
		array(
			'key_bools' => 'bools_1',
			'key_palette' => 'cpalette_2',
			'key_duotone' => 'dtone_odd',
		)
	);

	// Since it's optional, we have to "force" the create for Gradients Collection. Otherwise, it won't be created. 
	$new_color_grads_coll = $new_color_fact->createGradientsCollection();

	// When the Gradients Collection is created, it also creates the Gradients Item Collection, if it doesn't already exist, or is not passed in. 
	$new_color_grads_item_coll = $new_color_fact->getGradientsItemCollection();

	$new_color_settings_coll->setGradients( $new_color_grads_coll );

	$format_0 = 'to bottom, %s 0%%, %s 100%%';
	$name_0   = 'Vertical soft %s to %s';
	$new_color_grads_item_coll->setNameFormat( $name_0 );

	$arr_gradients = array(
		'1' => array( 'accent', 'base' ),
		'2' => array( 'accent_2', 'base' ),
		'3' => array( 'accent_3', 'base' ),
		'4' => array( 'accent_4', 'base' ),
		'5' => array( 'accent_5', 'base' ),
		'6' => array( 'contrast_3', 'base' ),
	);
	$arr_grad_test_1 = array();
	foreach ( $arr_gradients as $key => $arr ) {
		$arr_grad_test_1[] = 'gradient_' . $key;
		$new_color_grads_item_coll->add(
			'gradient_' . $key,
			array(
				'type'   => 'lin',
				'format' => $format_0,
				'colors' => $arr,
				'slug'   => 'gradient-' . $key,
			)
		);
	}

	$format_1 = 'to bottom, %s 50%%, %s 50%%';
	$name_1   = 'Vertical hard %s to %s';
	$new_color_grads_item_coll->setNameFormat( $name_1 );

	$arr_gradients = array(
		'7'  => array( 'accent', 'base' ),
		'8'  => array( 'accent_2', 'base' ),
		'9'  => array( 'accent_3', 'base' ),
		'10' => array( 'accent_4', 'base' ),
		'11' => array( 'accent_5', 'base' ),
		'12' => array( 'contrast_3', 'base' ),
	);

	$arr_grad_test_2 = array();
	foreach ( $arr_gradients as $key => $arr ) {
		$arr_grad_test_2[] = 'gradient_' . $key;
		$new_color_grads_item_coll->add(
			'gradient_' . $key,
			array(
				'type'   => 'lin',
				'format' => $format_1,
				'colors' => $arr,
				'slug'   => 'gradient-' . $key,
			)
		);
	}

	$new_color_settings_coll->setGradients( $new_color_grads_coll );

	$new_color_grads_coll->add( 
		'gradient_1',
		array(
			'items' => $arr_grad_test_2
		)
	);
	$new_color_grads_coll->add(
		'gradient_2',
		array(
			'items' => $arr_grad_test_1
		)
	);

	// Now let's add two more to the settings collection.
	$new_color_settings_coll->add(
		'color_4',
		array(
			'key_bools' => 'bools_1',
			'key_palette' => 'cpalette_1',
			'key_duotone' => 'dtone_odd',
			'key_gradients' => 'gradient_1',
		)
	)->add(
		'color_5',
		array(
			'key_bools' => 'bools_2',
			'key_palette' => 'cpalette_1',
			'key_duotone' => 'dtone_even',
			'key_gradients' => 'gradient_2',
		)
	);

	// $new_color_settings_coll->get( ??? );

}

```

Agreed. One one hand it feels verbose, and might take come getting used to. However, there are advantages:

- If a color changes, you only need to update it in one place.

- If a format for a grandient changes, you only need to update it in one place.

- Having different color settings for different blocks (at different times for different users) and being able to maintain and control that? Priceless. 

- Instead of trying to maintain sprawl-y JSON (which doesn't let you add comments to and is the antithesis of DRY), you can do things programmatically via PHP.

