<?php
/**
 * Class ClassDuotoneItemCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Color
 */

namespace WPezBlockEditor\ThemeJSONSettings\Color;

use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;
use WPezBlockEditor\ThemeJSONSettings\InterfaceCollectionBase;

/**
 * Class ClassFontFamiliesCollection extends ClassCollectionBase.
 */
class ClassDuotoneItemCollection extends ClassCollectionBase {

	/**
	 * Instance of a class that implements InterfaceCollectionBase.
	 *
	 * @var object
	 */
	protected $obj_color;


	/**
	 * Used to auto-create the 'name' (if it's not specified).
	 *
	 * @var string
	 */
	protected $str_format;

	/**
	 * The class constructor.
	 */
	public function __construct( InterfaceCollectionBase $color ) {

		$this->obj_color  = $color;
		$this->str_format = '%s and %s';
		$this->setProperties();
	}

	/**
	 * Sets the format for the Duotone name. The format must have two %s. The default is '%s and %s'.
	 *
	 * @param string $format The format for the duotone name.
	 *
	 * @return $this
	 */
	public function setFormat( string $format ): object {

		// The format must have two %s.
		if ( 2 === substr_count( $format, '%s' ) ) {
			$this->str_format = $format;
		}
		return $this;
	}

	/**
	 * Gets the format for the Duotone name.
	 *
	 * @return string
	 */
	public function getFormat(): string {
		return $this->str_format;
	}

	/**
	 * Adds a new duotone item to the "sub-collection".
	 *
	 * @param string $key  The unique identifier for sub-collection being added.
	 * @param array  $args The associative array of additional arguments / values for the duotone.
	 *  - 'colors' (array): The two color keys from the Color Collection for the duotone.
	 *  - 'name' (string): The name of the duotone. (Optional)
	 *  - 'slug' (string): The slug of the duotone.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {

		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}

		if ( $this->isValid( $args ) ) {

			$color_0 = $this->obj_color->get( $args['colors'][0] );
			$color_1 = $this->obj_color->get( $args['colors'][1] );

			// 'name' is optional, if it's not set
			if ( ! isset( $args['name'] ) || ! is_string( $args['name'] ) ) {
				if ( ! isset( $color_0['alias'], $color_1['alias'] ) ) {
					return $this;
				}
				$args['name'] = sprintf( $this->str_format, $color_0['alias'], $color_1['alias'] );
			}

			if ( ! isset( $color_0['color'], $color_1['color'] ) ) {
				return $this;
			}

			$this->arr_collection[ $key ] = array(
				'colors' => array( $color_0['color'], $color_1['color'] ),
				'name'   => $args['name'],
				'slug'   => $args['slug'],
			);
		}
		return $this;
	}

	/**
	 * Validates the arguments for the sub-collection being added.
	 *
	 * @param array $args The associative array of additional arguments / values for the duotone.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['colors'], $args['colors'][0], $args['colors'][1], $args['slug'] )
			|| ! is_array( $args['colors'] ) || ! is_string( $args['slug'] )
			|| ! is_string( $args['colors'][0] ) || ! is_string( $args['colors'][1] ) ) {
			return false;
		}
		return true;
	}
}
