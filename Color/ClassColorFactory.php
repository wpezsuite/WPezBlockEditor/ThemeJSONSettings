<?php
/**
 * Class ClassColorFactory, manages the creation of the various classes for Theme JSON settings: Color.
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Color
 */

namespace WPezBlockEditor\ThemeJSONSettings\Color;

// Base.
use WPezBlockEditor\ThemeJSONSettings\ClassFactoryBase;
use WPezBlockEditor\ThemeJSONSettings\InterfaceBooleanPropertiesDefaultsBase;

// Settings' Boolean.
use WPezBlockEditor\ThemeJSONSettings\Color\ClassBooleanPropertiesDefaults;
use WPezBlockEditor\ThemeJSONSettings\Color\ClassBooleanProperties;
use WPezBlockEditor\ThemeJSONSettings\Color\ClassBooleanPropertiesCollection;

// Settings' Unique.
use WPezBlockEditor\ThemeJSONSettings\Color\ClassColorCollection;
use WPezBlockEditor\ThemeJSONSettings\Color\ClassDuotoneItemCollection;
use WPezBlockEditor\ThemeJSONSettings\Color\ClassDuotoneCollection;
use WPezBlockEditor\ThemeJSONSettings\Color\ClassGradientsCollection;
use WPezBlockEditor\ThemeJSONSettings\Color\ClassPaletteCollection;
use WPezBlockEditor\ThemeJSONSettings\Color\ClassColorSettingsCollection as SettingsCollection;


/**
 * ClassBackgroundFactory
 */
class ClassColorFactory extends ClassFactoryBase {

	/**
	 * Instance of the Color's ClassColorCollection
	 *
	 * @var ClassColorCollection
	 */
	protected $new_color_collection;

	/**
	 * Instance of the Color's ClassDuotoneItemCollection
	 *
	 * @var ClassDuotoneItemCollection
	 */
	protected $new_duotone_item_collection;

	/**
	 * Instance of the Color's ClassDuotoneCollection
	 *
	 * @var ClassDuotoneCollection
	 */
	protected $new_duotone_collection;

	/**
	 * Instance of the Color's ClassGradientsItemCollection
	 *
	 * @var ClassGradientsItemCollection
	 */
	protected $new_gradients_item_collection;

	/**
	 * Instance of the Color's ClassGradientsCollection
	 *
	 * @var ClassGradientsCollection
	 */
	protected $new_gradients_collection;

	/**
	 * Instance of the Color's ClassPaletteCollection
	 *
	 * @var ClassPaletteCollection
	 */
	protected $new_palette_collection;

	/**
	 * Sets the properties for the class.
	 *
	 * @return void
	 */
	protected function setProperties() {

		parent::setProperties();

		$this->new_color_collection          = null;
		$this->new_duotone_item_collection   = null;
		$this->new_duotone_collection        = null;
		$this->new_gradients_item_collection = null;
		$this->new_gradients_collection      = null;
		$this->new_palette_collection        = null;
	}

	/**
	 * Creates the instance of the Color's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanPropertiesDefaults
	 */
	protected function newBooleanPropertiesDefaults(): ClassBooleanPropertiesDefaults {

		return new ClassBooleanPropertiesDefaults();
	}

	/**
	 * Creates the instance of the Color's ClassBooleanProperties.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults - Instance of the Color's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanProperties
	 */
	protected function newBooleanProperties( InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults ): ClassBooleanProperties {

		return new ClassBooleanProperties( $new_bool_props_defaults );
	}

	/**
	 * Creates the instance of the Color's ClassBooleanPropertiesCollection.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props - Instance of the Color's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanPropertiesCollection
	 */
	protected function newBooleanPropertiesCollection( InterfaceBooleanPropertiesDefaultsBase $new_bool_props ): ClassBooleanPropertiesCollection {

		return new ClassBooleanPropertiesCollection( $new_bool_props );
	}

	/**
	 * ----- ----- Methods that are unique to Color ----- -----
	 */

	/**
	 * Returns the property value of new_color_collection which is an instance of ClassColorCollection.
	 *
	 * @return ClassColorCollection
	 */
	public function createColorCollection(): ClassColorCollection {

		$this->new_color_collection = $this->newColorCollection();
		return $this->new_color_collection;
	}

	/**
	 * Instantiates ClassColorCollection and returns an instance.
	 *
	 * @return ClassColorCollection {
	 */
	protected function newColorCollection(): ClassColorCollection {

		return new ClassColorCollection();
	}

	/**
	 * Returns the property value of new_color_collection which is an instance of ClassColorCollection.
	 *
	 * @return ClassColorCollection
	 */
	public function getColorCollection(): ClassColorCollection {

		return $this->new_color_collection;
	}

	/**
	 * Returns the property value of new_duotone_item_collection which is an instance of ClassDuotoneItemCollection.
	 *
	 * @param ClassColorCollection $new_color_collection - Instance of ClassColorCollection.
	 *
	 * @return ClassDuotoneItemCollection
	 */
	public function createDuotoneItemCollection( ClassColorCollection $new_color_collection = null ): ClassDuotoneItemCollection {

		if ( null === $new_color_collection ) {

			if ( null === $this->new_color_collection ) {
				$this->new_color_collection = $this->createColorCollection();
			}

			$new_color_collection = $this->new_color_collection;
		}

		$this->new_duotone_item_collection = $this->newDuotoneItemCollection( $new_color_collection );
		return $this->new_duotone_item_collection;
	}

	/**
	 * Instantiates ClassDuotoneItemCollection and returns an instance.
	 *
	 * @param ClassColorCollection $new_color_collection - Instance of ClassColorCollection.
	 *
	 * @return ClassDuotoneItemCollection {
	 */
	protected function newDuotoneItemCollection( ClassColorCollection $new_color_collection ): ClassDuotoneItemCollection {

		return new ClassDuotoneItemCollection( $new_color_collection );
	}

	/**
	 * Returns the property value of new_duotone_item_collection which is an instance of ClassDuotoneItemCollection.
	 *
	 * @return ClassDuotoneItemCollection
	 */
	public function getDuotoneItemCollection(): ClassDuotoneItemCollection {

		return $this->new_duotone_item_collection;
	}

// -----------------------------------------

	/**
	 * Returns the property value of new_duotone_item_collection which is an instance of ClassDuotoneCollection.
	 *
	 * @param ClassDuotoneItemCollection|null $new_duotone_item_collection - An instance of ClassDuotoneItemCollection.
	 *
	 * @return ClassDuotoneCollection
	 */
	public function createDuotoneCollection( ClassDuotoneItemCollection $new_duotone_item_collection = null ): ClassDuotoneCollection {

		if ( null === $new_duotone_item_collection ) {

			if ( null === $this->new_duotone_item_collection ) {
				$this->new_duotone_item_collection = $this->createDuotoneItemCollection();
			}

			$new_duotone_item_collection = $this->new_duotone_item_collection;
		}

		$this->new_duotone_collection = $this->newDuotoneCollection( $new_duotone_item_collection );
		return $this->new_duotone_collection;
	}

	/**
	 * Instantiates ClassDuotoneCollection and returns an instance.
	 *
	 * @param ClassDuotoneItemCollection $new_duotone_item_collection - Instance of ClassDuotoneItemCollection.
	 *
	 * @return ClassDuotoneCollection
	 */
	protected function newDuotoneCollection( ClassDuotoneItemCollection $new_duotone_item_collection ): ClassDuotoneCollection {

		return new ClassDuotoneCollection( $new_duotone_item_collection );
	}

	/**
	 * Returns the property value of new_duotone_collection which is an instance of ClassDuotoneCollection.
	 *
	 * @return ClassDuotoneCollection
	 */
	public function getDuotoneCollection(): ClassDuotoneCollection {

		return $this->new_duotone_collection;
	}

	/**
	 * Returns the property value of new_gradients_item_collection which is an instance of ClassGradientsItemCollection.
	 *
	 * @param ClassColorCollection|null $new_color_collection - An instance of ClassColorCollection.
	 *
	 * @return ClassGradientsItemCollection
	 */
	public function createGradientsItemCollection( ClassColorCollection $new_color_collection = null ): ClassGradientsItemCollection {

		if ( null === $new_color_collection ) {

			if ( null === $this->new_color_collection ) {
				$this->new_color_collection = $this->createColorCollection();
			}

			$new_color_collection = $this->new_color_collection;
		}
		$this->new_gradients_item_collection = $this->newGradientsItemCollection( $new_color_collection );
		return $this->new_gradients_item_collection;
	}

	/**
	 * Instantiates ClassGradientsCollection and returns an instance.
	 *
	 * @param ClassColorCollection $new_color_collection - Instance of ClassColorCollection.
	 *
	 * @return ClassGradientsItemCollection
	 */
	protected function newGradientsItemCollection( ClassColorCollection $new_color_collection ): ClassGradientsItemCollection {

		return new ClassGradientsItemCollection( $new_color_collection );
	}

	/**
	 * Returns the property value of new_gradients_item_collection which is an instance of getGradientsCollection.
	 *
	 * @return ClassGradientsItemCollection
	 */
	public function getGradientsItemCollection(): ClassGradientsItemCollection {

		return $this->new_gradients_item_collection;
	}

	/**
	 * Returns the property value of new_gradients_collection which is an instance of ClassGradientsCollection.
	 *
	 * @param ClassGradientsItemCollection|null $new_gradients_item_collection - An instance of ClassGradientsItemCollection.
	 *
	 * @return ClassGradientsCollection
	 */
	public function createGradientsCollection( ClassGradientsItemCollection $new_gradients_item_collection = null ): ClassGradientsCollection {

		if ( null === $new_gradients_item_collection ) {

			if ( null === $this->new_gradients_item_collection ) {
				$this->new_gradients_item_collection = $this->createGradientsItemCollection();
			}

			$new_gradients_item_collection = $this->new_gradients_item_collection;
		}
		$this->new_gradients_collection = $this->newGradientsCollection( $new_gradients_item_collection );
		return $this->new_gradients_collection;
	}

	/**
	 * Instantiates ClassGradientsCollection and returns an instance.
	 *
	 * @param ClassGradientsItemCollection $new_gradients_item_collection - Instance ClassGradientsItemCollection.
	 *
	 * @return ClassGradientsCollection
	 */
	protected function newGradientsCollection( ClassGradientsItemCollection $new_gradients_item_collection ): ClassGradientsCollection {

		return new ClassGradientsCollection( $new_gradients_item_collection );
	}

	/**
	 * Returns the property value of new_gradients_collection which is an instance of getGradientsCollection.
	 *
	 * @return ClassGradientsCollection
	 */
	public function getGradientsCollection(): ClassGradientsCollection {

		return $this->new_gradients_collection;
	}



	/**
	 * Returns the property value of new_palette_collection which is an instance of ClassPaletteCollection.
	 *
	 * @param ClassColorCollection|null $new_color_collection - An instance of ClassColorCollection.
	 *
	 * @return ClassPaletteCollection
	 */
	public function createPaletteCollection( ClassColorCollection $new_color_collection = null ): ClassPaletteCollection {

		if ( null === $new_color_collection ) {

			if ( null === $this->new_color_collection ) {
				$this->new_color_collection = $this->createColorCollection();
			}

			$new_color_collection = $this->new_color_collection;
		}
		$this->new_palette_collection = $this->newPaletteCollection( $new_color_collection );
		return $this->new_palette_collection;
	}

	/**
	 * Instantiates ClassPaletteCollection and returns an instance.
	 *
	 * @param ClassColorCollection $new_color_collection - Instance ofClassColorCollection.
	 *
	 * @return ClassPaletteCollection
	 */
	protected function newPaletteCollection( ClassColorCollection $new_color_collection ): ClassPaletteCollection {

		return new ClassPaletteCollection( $new_color_collection );
	}

	/**
	 * Returns the property value of new_palette_collection which is an instance of ClassPaletteCollection.
	 *
	 * @return ClassPaletteCollection
	 */
	public function getPaletteCollection(): ClassPaletteCollection {

		return $this->new_palette_collection;
	}


	/**
	 * Creates an instance of ClassColorSettingsCollection, assigns it to $new_settings_collection, and returns that instance.
	 *
	 * @param ClassBooleanPropertiesCollection|null $new_bool_props_collection - An instance of ClassBooleanPropertiesCollection.
	 * @param ClassPaletteCollection|null           $new_palette_collection    - An instance of ClassPaletteCollection.
	 * @param ClassDuotoneCollection|null           $new_duotone_collection    - An instance of ClassDuotoneCollection.
	 * @param ClassGradientsCollection|null         $new_gradients_collection  - An instance of ClassGradientsCollection.
	 *
	 * @return ClassColorSettingsCollection
	 */
	public function createSettingsCollection( ClassBooleanPropertiesCollection $new_bool_props_collection = null, ClassPaletteCollection $new_palette_collection = null, ClassDuotoneCollection $new_duotone_collection = null, ClassGradientsCollection $new_gradients_collection = null ): ClassColorSettingsCollection {

		if ( null === $new_bool_props_collection ) {

			// Has Bool Props Collection been created yet?
			if ( null === $this->new_bool_props_collection ) {
				$this->new_bool_props_collection = $this->newBooleanPropertiesCollection( $this->new_bool_props_defaults );
			}
			$new_bool_props_collection = $this->new_bool_props_collection;
		}

		if ( null === $new_palette_collection ) {

			// Has Palette Collection been created yet?
			if ( null === $this->new_palette_collection ) {
				$this->new_palette_collection = $this->createPaletteCollection();
			}
			$new_palette_collection = $this->new_palette_collection;
		}

		// Important - Duotones are optional, if you need them you'll need to create them. We're not going to auto-create them.
		// You'll either have to do that prior to using this method and pass in an instance of ClassDuotoneCollection, or there's a setDuotone() method in the ClassColorSettingsCollection.
		if ( null === $new_duotone_collection ) {
			if ( null !== $this->new_duotone_collection ) {
				$new_duotone_collection = $this->new_duotone_collection;
			}
		}

		// Important - Gradients are optional, if you need them you'll need to create them. We're not going to auto-create them.
		// You'll either have to do that prior to using this method and pass in an instance of ClassGradientsCollection, or there's a setGradients() method in the ClassColorSettingsCollection.
		if ( null === $new_gradients_collection ) {
			if ( null !== $this->new_gradients_collection ) {
				$new_gradients_collection = $this->new_gradients_collection;
			}
		}

		$this->new_settings_collection = $this->newSettingsCollection( $new_bool_props_collection, $new_palette_collection, $new_duotone_collection, $new_gradients_collection );

		return $this->new_settings_collection;
	}

	/**
	 * Returns the property value of new_settings_collection which is an instance of ClassColorSettingsCollection.
	 *
	 * @return SettingsCollection
	 */
	public function getSettingsCollection(): SettingsCollection {

		return $this->new_settings_collection;
	}

	/**
	 * Creates the instance of ClassColorSettingsCollection.
	 *
	 * @param ClassBooleanPropertiesCollection $new_bool_props_collection - Instance of ClassBooleanPropertiesCollection.
	 * @param ClassPaletteCollection           $new_palette_collection    - Instance of ClassPaletteCollection.
	 * @param ClassDuotoneCollection           $new_duotone_collection    - (Optional) Instance of ClassDuotoneCollection.
	 * @param ClassGradientsCollection         $new_gradients_collection  - (Optional) Instance of ClassGradientsCollection.
	 *
	 * @return ClassColorSettingsCollection
	 */
	protected function newSettingsCollection( ClassBooleanPropertiesCollection $new_bool_props_collection, ClassPaletteCollection $new_palette_collection, ClassDuotoneCollection $new_duotone_collection = null, ClassGradientsCollection $new_gradients_collection = null ): SettingsCollection {

		return new SettingsCollection( $new_bool_props_collection, $new_palette_collection, $new_duotone_collection, $new_gradients_collection );
	}
}
