<?php
/**
 * Class ClassColorSettingsCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Color
 */

namespace WPezBlockEditor\ThemeJSONSettings\Color;

use WPezBlockEditor\ThemeJSONSettings\InterfaceCollectionBase;
use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;

/**
 * Class ClassColorSettingsCollection extends ClassCollectionBase.
 */
class ClassColorSettingsCollection extends ClassCollectionBase {

	/**
	 * The boolean properties collection.
	 *
	 * @var object
	 */
	protected $obj_booleans;

	/**
	 * The duotone collection.
	 *
	 * @var object
	 */
	protected $obj_duotone;

	/**
	 * The gradients collection.
	 *
	 * @var object
	 */
	protected $obj_gradients;

	/**
	 * The palette collection.
	 *
	 * @var object
	 */
	protected $obj_palette;

	/**
	 * Class constructor.
	 *
	 * @param InterfaceCollectionBase      $booleans  Instance of the collection class for booleans that implements InterfaceCollectionBase.
	 * @param InterfaceCollectionBase      $palette   Instance  of the collection class for palette that implements InterfaceCollectionBase.
	 * @param null|InterfaceCollectionBase $duotone   Instance  of the collection class for duotone that implements InterfaceCollectionBase. (Optional).
	 * @param null|InterfaceCollectionBase $gradients Instance  of the collection class for gradients that implements InterfaceCollectionBase. (Optional).
	 */
	public function __construct( InterfaceCollectionBase $booleans, InterfaceCollectionBase $palette, InterfaceCollectionBase $duotone = null, InterfaceCollectionBase $gradients = null ) {

		$this->obj_booleans = $booleans;
		$this->obj_palette  = $palette;

		// We'll make duotones and gradients optional as some themes might not need them.
		$this->obj_duotone = null;
		if ( $duotone instanceof InterfaceCollectionBase ) {
			$this->obj_duotone = $duotone;
		}
		$this->obj_gradients = null;
		if ( $gradients instanceof InterfaceCollectionBase ) {
			$this->obj_gradients = $gradients;
		}

		$this->setProperties();
	}

	/**
	 * Sets the Duotone Collection - This is helpful if you need to add a duotone collection after the class has been instantiated.
	 *
	 * @param InterfaceCollectionBase $duotone Instance of the collection class for duotone that implements InterfaceCollectionBase.
	 *
	 * @return void
	 */
	public function setDuotone( InterfaceCollectionBase $duotone ) {
		$this->obj_duotone = $duotone;
	}

	/**
	 * Sets the Gradients Collection - This is helpful if you need to add a gradients collection after the class has been instantiated.	
	 *
	 * @param InterfaceCollectionBase $gradients Instance of the collection class for gradients that implements InterfaceCollectionBase.
	 *
	 * @return void
	 */
	public function setGradients( InterfaceCollectionBase $gradients ) {
		$this->obj_gradients = $gradients;
	}

	/**
	 * Adds a item to the collection.
	 *
	 * @param string $key  The array index key of the collection being added.
	 * @param array  $args The additional arguments (value pairs) of the collection being added.
	 * - 'key_bools' (string): The key of the booleans collection.
	 * - 'key_palette' (string): The key of palette collection.
	 * - 'key_duotone' (string): The key of duotone collection. (Optional).
	 * - 'key_gradients' (string): The key of gradients collection. (Optional).
	 *
	 * @return object $this
	 */
	public function add( string $key, array $args ): object {

		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}
		if ( $this->isValid( $args ) ) {

			$arr_props            = $this->obj_booleans->get( $args['key_bools'] );
			$arr_props['palette'] = $this->obj_palette->get( $args['key_palette'] );

			// Since duotone and gradients are optional, we need to check if they are set before adding them to the collection.
			$arr_props['duotone'] = array();
			if ( null !== $this->obj_duotone && isset( $args['key_duotone'] ) && is_string( $args['key_duotone'] ) ) {

				$arr_props['duotone'] = $this->obj_duotone->get( $args['key_duotone'] );
			}

			$arr_props['gradients'] = array();
			if ( null !== $this->obj_gradients && isset( $args['key_gradients'] ) && is_string( $args['key_gradients'] ) ) {

				$arr_props['gradients'] = $this->obj_gradients->get( $args['key_gradients'] );
			}

			$this->arr_collection[ $key ] = $arr_props;
		}
		return $this;
	}


	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The additional arguments / values of the collection being added.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['key_bools'], $args['key_palette'] ) ) {
			return false;
		}
		if ( ! is_string( $args['key_bools'] ) || ! is_string( $args['key_palette'] ) ) {
			return false;
		}
		return true;
	}
}
