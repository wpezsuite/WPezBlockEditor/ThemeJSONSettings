<?php
/**
 * Class ClassPaletteCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Color
 */

namespace WPezBlockEditor\ThemeJSONSettings\Color;

use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;
use WPezBlockEditor\ThemeJSONSettings\InterfaceCollectionBase;


/**
 * Class ClassPaletteCollection extends ClassCollectionBase.
 */
class ClassPaletteCollection extends ClassCollectionBase {

	/**
	 * Instance of ClassColorCollection - a class that implements InterfaceCollectionBase.
	 *
	 * @var object
	 */
	protected $obj_color;

	/**
	 * The class constructor.
	 *
	 * @param InterfaceCollectionBase $color Instance of ClassColorCollection - a class that implements InterfaceCollectionBase.
	 */
	public function __construct( InterfaceCollectionBase $color ) {

		$this->obj_color = $color;
		$this->setProperties();
	}

	/**
	 * Adds a new palette to the collection.
	 *
	 * @param string $key  The unique identifier for collection being added.
	 * @param array  $args The associative array of additional arguments / values for the palette.
	 * - 'items' (array): The colors keys of from the Color Collection for the palette.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {
		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}
		if ( $this->isValid( $args ) ) {

			$arr_palette = array();
			foreach ( $args['items'] as $color_key ) {
				if ( ! is_string( $color_key ) ) {
					continue;
				}
				$arr_temp = $this->obj_color->get( $color_key );
				if ( ! empty( $arr_temp ) ) {
					// TODO - unset the 'alias'? Or will WP simply ignore it?
					$arr_palette[] = $arr_temp;
				}
			}

			$this->arr_collection[ $key ] = $arr_palette;
		}
		return $this;
	}

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The associative array of additional arguments / values for the gradient.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['items'] ) || ! is_array( $args['items'] ) ) {
			return false;
		}
		return true;
	}
}
