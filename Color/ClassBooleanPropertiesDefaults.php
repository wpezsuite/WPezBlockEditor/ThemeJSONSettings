<?php
/**
 * Class ClassBooleanPropertiesDefaults
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Color
 */

namespace WPezBlockEditor\ThemeJSONSettings\Color;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesDefaultsBase;

class ClassBooleanPropertiesDefaults extends ClassBooleanPropertiesDefaultsBase {

	/**
	 * Set property defaults, specialized for this class / Theme JSON object type.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_defaults = array(

			'bool_background'        => array(
				'block_prop' => 'background',
				'get'        => 'getBackground',
				'default'    => false,
			),
			'bool_custom'            => array(
				'block_prop' => 'custom',
				'get'        => 'getCustom',
				'default'    => false,
			),
			'bool_custom_duotone'    => array(
				'block_prop'  => 'customDuotone',
				'get'         => 'getCustomDuotone',
				'default'     => false,
			),
			'bool_custom_gradient'   => array(
				'block_prop' => 'customGradient',
				'get'        => 'getCustomGradient',
				'default'    => false,
			),
			'bool_default_duotone'   => array(
				'block_prop' => 'defaultDuotone',
				'get'        => 'getDefaultDuotone',
				'default'    => false,
			),
			'bool_default_gradients' => array(
				'block_prop' => 'defaultGradients',
				'get'        => 'getDefaultGradients',
				'default'    => false,
			),
			'bool_default_palette'   => array(
				'block_prop' => 'defaultPalette',
				'get'        => 'getDefaultPalette',
				'default'    => false,
			),
			'bool_link'              => array(
				'block_prop' => 'link',
				'get'        => 'getLink',
				'default'    => false,
			),
			'bool_text'              => array(
				'block_prop' => 'text',
				'get'        => 'getText',
				'default'    => false,
			),
		);
	}
}
