<?php
/**
 * Class ClassGradientsItemCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Color
 */

namespace WPezBlockEditor\ThemeJSONSettings\Color;

use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;
use WPezBlockEditor\ThemeJSONSettings\InterfaceCollectionBase;

/**
 * Class ClassGradientsItemCollection extends ClassCollectionBase.
 */
class ClassGradientsItemCollection extends ClassCollectionBase {

	/**
	 * Instance of a class that implements InterfaceCollectionBase.
	 *
	 * @var object
	 */
	protected $obj_color;

	/**
	 * Used to auto-create the 'name' (if it's not specified).
	 *
	 * @var string
	 */
	protected $str_name_format;


	/**
	 * The valid gradient types.
	 *
	 * @var array
	 */
	protected $arr_valid_types;

	/**
	 * The class constructor.
	 *
	 * @param InterfaceCollectionBase $color The color collection.
	 */
	public function __construct( InterfaceCollectionBase $color ) {

		// $this->arr_subcollection_items = array();
		$this->obj_color               = $color;
		$this->str_name_format         = '%s to %s';
		$this->arr_valid_types         = array(
			'lin'     => 'linear',
			'rad'     => 'radial',
			'con'     => 'conic',
			'rep-lin' => 'repeating-linear',
			'rep-rad' => 'repeating-radial',
			'rep-con' => 'repeating-conic',
		);
		$this->setProperties();
	}

	/**
	 * Sets the format for the Gradient name.
	 *
	 * @param string $format The format for the duotone name.
	 *
	 * @return $this
	 */
	public function setNameFormat( string $format ): object {

		// The format must have two %s.
		if ( 2 === substr_count( $format, '%s' ) ) {
			$this->str_name_format = $format;
		}
		return $this;
	}

	/**
	 * Gets the format for the Gradient name.
	 *
	 * @return string
	 */
	public function getNameFormat(): string {
		return $this->str_name_format;
	}

	/**
	 * Adds a new gradient to the collection.
	 *
	 * @param string $key  The unique identifier for collection being added.
	 * @param array  $args The associative array of additional arguments / values for the gradient.
	 * - 'type' (string): The type of gradient abbreviated. Allowed values: 'lin', 'rad', 'con', 'rep-lin', 'rep-rad', 'rep-con'.
	 * - 'format' (string): The gradient string - the bit between the ( . . . ) - including %s for vsprintf()'ing in the colors.
	 * - 'colors' (array): The colors keys of from the Color Collection for the gradient.
	 * - 'name' (string): The name of the gradient. (Optional). If there's not name specified, one will be auto-generated using the 'str_name_format'.
	 * - 'slug' (string): The slug of the gradient.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {

		if ( $this->isValid( $args ) ) {

			$arr_colors  = array();
			$arr_aliases = array();
			foreach ( $args['colors'] as $color_key ) {
				if ( ! is_string( $color_key ) ) {
					continue;
				}
				if ( ! isset( $this->obj_color->get( $color_key )['color'] ) ) {
					continue;
				}
				$arr_color     = $this->obj_color->get( $color_key );
				$arr_colors[]  = $arr_color['color'];
				$arr_aliases[] = $arr_color['alias'];
			}

			$str_type = strtolower( trim( $args['type'] ) );

			if ( isset( $this->arr_valid_types[ $str_type ] ) && count( $arr_colors ) === substr_count( $args['format'], '%s' ) ) {

				if ( ! isset( $args['name'] ) ) {

					$str_name = implode( ' to ', $arr_aliases );
					if ( count( $arr_aliases ) === substr_count( $this->str_name_format, '%s' ) ) {
						$str_name = vsprintf( $this->str_name_format, $arr_aliases );
					}
					$args['name'] = $str_name;
				}

				// Decode the abbreviated gradient type to the full proper string.
				$str_type = $this->arr_valid_types[ $str_type ];

				$this->arr_collection[ $key ] = array(
					'gradient' => esc_attr( $str_type ) . '-gradient(' . vsprintf( $args['format'], $arr_colors ) . ')',
					'name'     => $args['name'],
					'slug'     => $args['slug'],
				);
			};
		}
		return $this;
	}

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The associative array of additional arguments / values for the gradient.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['type'], $args['colors'], $args['format'], $args['slug'] )
			|| ! is_string( $args['type'] ) || ! is_array( $args['colors'] ) || ! is_string( $args['format'] ) || ! is_string( $args['slug'] ) ) {
			return false;
		}
		return true;
	}
}
