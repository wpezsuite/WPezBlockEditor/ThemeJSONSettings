<?php
/**
 * Class ClassDuotoneCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Color
 */

namespace WPezBlockEditor\ThemeJSONSettings\Color;

use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;
use WPezBlockEditor\ThemeJSONSettings\InterfaceCollectionBase;

/**
 * Class ClassFontFamiliesCollection extends ClassCollectionBase.
 */
class ClassDuotoneCollection extends ClassCollectionBase {

	/**
	 * Instance of a class that implements InterfaceCollectionBase.
	 *
	 * @var object
	 */
	protected $obj_item;

	/**
	 * The class constructor.
	 *
	 * @param InterfaceCollectionBase $item Instance of the ClassDuotoneItemCollection.
	 */
	public function __construct( InterfaceCollectionBase $item ) {

		$this->obj_item = $item;
		$this->setProperties();
	}

	/**
	 * Adds a new duotone item to the collection.
	 *
	 * @param string $key  The unique identifier for sub-collection being added.
	 * @param array  $args The associative array of additional arguments / values for the duotone.
	 *  - 'items' (array): And array of item keys from the Duotone Item Collection to be combined into a duotone collection.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {

		if ( $this->isValid( $args ) ) {

			foreach ( $args['items'] as $item_key ) {
				if ( ! is_string( $item_key ) || empty( $this->obj_item->get( $item_key ) ) ) {
					continue;
				}
				$this->arr_collection[ $key ][] = $this->obj_item->get( $item_key );
			}
		}
		return $this;
	}

	/**
	 * Validates the arguments for the sub-collection being added.
	 *
	 * @param array $args The associative array of additional arguments / values for the duotone.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['items'] ) || ! is_array( $args['items'] ) ) {
			return false;
		}
		return true;
	}
}
