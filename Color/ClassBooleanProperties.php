<?php
/**
 * Class ClassBooleanProperties
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Color
 */

namespace WPezBlockEditor\ThemeJSONSettings\Color;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesBase;

/**
 * Class ClassBooleanProperties extends ClassBooleanPropertiesBase.
 */
class ClassBooleanProperties extends ClassBooleanPropertiesBase {

	/**
	 * Whether to use the background.
	 *
	 * @var bool
	 */
	protected $bool_background;

	/**
	 * Whether to use the custom (color).
	 *
	 * @var bool
	 */
	protected $bool_custom;

	/**
	 * Whether to use the custom duotone.
	 *
	 * @var bool
	 */
	protected $bool_custom_duotone;

	/**
	 * Whether to use the custom gradient.
	 *
	 * @var bool
	 */
	protected $bool_custom_gradient;

	/**
	 * Whether to use the default duotone.
	 *
	 * @var bool
	 */
	protected $bool_default_duotone;

	/**
	 * Whether to use the default gradients. Note the plural.
	 *
	 * @var bool
	 */
	protected $bool_default_gradients;

	/**
	 * Whether to use the default palette.
	 *
	 * @var bool
	 */
	protected $bool_default_palette;

	/**
	 * Whether to use the link.
	 *
	 * @var bool
	 */
	protected $bool_link;

	/**
	 * Whether to use the text.
	 *
	 * @var bool
	 */
	protected $bool_text;

	/**
	 * Set property bool_background.
	 *
	 * @param bool $bool_background Value to set for bool_background.
	 *
	 * @return object $this.
	 */
	public function setBackground( bool $bool_background = true ): object {

		$this->bool_background = $bool_background;
		return $this;
	}

	/**
	 * Get property bool_background.
	 *
	 * @return bool
	 */
	public function getBackground(): bool {

		return $this->bool_background;
	}

	/**
	 * Set property bool_custom.
	 *
	 * @param bool $bool_custom Value to set for bool_custom.
	 *
	 * @return object $this
	 */
	public function setCustom( bool $bool_custom = true ): object {

		$this->bool_custom = $bool_custom;
		return $this;
	}

	/**
	 * Get property bool_custom.
	 *
	 * @return bool
	 */
	public function getCustom(): bool {

		return $this->bool_custom;
	}

	/**
	 * Set property bool_custom_duotone.
	 *
	 * @param bool $bool_custom_duotone Value to set for bool_custom_duotone.
	 *
	 * @return object $this
	 */
	public function setCustomDuotone( bool $bool_custom_duotone = true ): object {
		
		$this->bool_custom_duotone = $bool_custom_duotone;
		return $this;
	}

	/**
	 * Get property bool_custom_duotone.
	 *
	 * @return bool
	 */
	public function getCustomDuotone(): bool {
		
		return $this->bool_custom_duotone;
	}

	/**
	 * Set property bool_custom_gradient.
	 *
	 * @param bool $bool_custom_gradient Value to set for bool_custom_gradient.
	 *
	 * @return object $this
	 */
	public function setCustomGradient( bool $bool_custom_gradient = true ): object {
		
		$this->bool_custom_gradient = $bool_custom_gradient;
		return $this;
	}

	/**
	 * Get property bool_custom_gradient.
	 *
	 * @return bool
	 */
	public function getCustomGradient(): bool {
		
		return $this->bool_custom_gradient;
	}

	/**
	 * Set property bool_default_duotone.
	 *
	 * @param bool $bool_default_duotone Value to set for bool_default_duotone.
	 *
	 * @return object $this
	 */
	public function setDefaultDuotone( bool $bool_default_duotone = true ): object {
		
		$this->bool_default_duotone = $bool_default_duotone;
		return $this;
	}

	/**
	 * Get property bool_default_duotone.
	 *
	 * @return bool
	 */
	public function getDefaultDuotone(): bool {
		
		return $this->bool_default_duotone;
	}

	/**
	 * Set property bool_default_gradients.
	 *
	 * @param bool $bool_default_gradients Value to set for bool_default_gradients.
	 *
	 * @return object $this
	 */
	public function setDefaultGradients( bool $bool_default_gradients = true ): object {
		
		$this->bool_default_gradients = $bool_default_gradients;
		return $this;
	}

	/**
	 * Get property bool_default_gradients.
	 *
	 * @return bool
	 */
	public function getDefaultGradients(): bool {
		
		return $this->bool_default_gradients;
	}

	/**
	 * Set property bool_default_palette.
	 *
	 * @param bool $bool_default_palette Value to set for bool_default_palette.
	 *
	 * @return object $this
	 */
	public function setDefaultPalette( bool $bool_default_palette = true ): object {
		
		$this->bool_default_palette = $bool_default_palette;
		return $this;
	}

	/**
	 * Get property bool_default_palette.
	 *
	 * @return bool
	 */
	public function getDefaultPalette(): bool {
		
		return $this->bool_default_palette;
	}

	/**
	 * Set property bool_link.
	 *
	 * @param bool $bool_link Value to set for bool_link.
	 *
	 * @return object $this
	 */
	public function setLink( bool $bool_link = true ): object {
		
		$this->bool_link = $bool_link;
		return $this;
	}

	/**
	 * Get property bool_link.
	 *
	 * @return bool
	 */
	public function getLink(): bool {
		
		return $this->bool_link;
	}

	/**
	 * Set property bool_text.
	 *
	 * @param bool $bool_text Value to set for bool_text.
	 *
	 * @return object $this
	 */
	public function setText( bool $bool_text = true ): object {
		
		$this->bool_text = $bool_text;
		return $this;
	}

	/**
	 * Get property bool_text.
	 *
	 * @return bool
	 */
	public function getText(): bool {
		
		return $this->bool_text;
	}
}
