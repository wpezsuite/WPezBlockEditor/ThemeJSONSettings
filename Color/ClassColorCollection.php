<?php
/**
 * Class ClassColorCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Color
 */

namespace WPezBlockEditor\ThemeJSONSettings\Color;

use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;

/**
 * Class ClassColorCollection extends ClassCollectionBase.
 */
class ClassColorCollection extends ClassCollectionBase {

	/**
	 * Adds a new palette to the collection.
	 *
	 * @param string $key  The unique identifier for collection being added.
	 * @param array  $args The associative array of additional arguments / values for the palette.
	 * - 'color' (string): The color for the color.
	 * - 'name' (string): The name of the color.
	 * - 'slug' (string): The slug of the color.
	 * - 'alias' (string): The alias of the color. For example, the name is 'main' but the alias is 'red' (or something that reflects the actual color).
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {
		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}
		if ( $this->isValid( $args ) ) {

			$this->arr_collection[ $key ] = array(
				'color' => $args['color'],
				'name'  => $args['name'],
				'slug'  => $args['slug'],
				'alias' => $args['alias'],
			);
		}
		return $this;
	}

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The associative array of additional arguments / values for the gradient.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['color'], $args['name'], $args['slug'], $args['alias'] )
			|| ! is_string( $args['color'] ) || ! is_string( $args['name'] ) || ! is_string( $args['slug'] ) || ! is_string( $args['alias'] ) ) {
			return false;
		}
		return true;
	}
}
