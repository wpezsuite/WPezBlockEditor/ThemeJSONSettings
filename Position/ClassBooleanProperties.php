<?php
/**
 * Class ClassBooleanProperties
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Position
 */

namespace WPezBlockEditor\ThemeJSONSettings\Position;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesBase;

/**
 * Class ClassBooleanProperties
 */
class ClassBooleanProperties extends ClassBooleanPropertiesBase {

	/**
	 * Flag for sticky property Theme JSON settings: Position.
	 *
	 * @var boolean
	 */
	protected $bool_sticky;

	/**
	 * Sets the sticky property.
	 *
	 * @param boolean $bool_sticky The value of the sticky property.
	 *
	 * @return object $this
	 */
	public function setSticky( bool $bool_sticky = true ): object {
		
		$this->bool_sticky = $bool_sticky;
		return $this;
	}

	/**
	 * Gets the sticky property.
	 *
	 * @return boolean
	 */
	public function getSticky(): bool {
		
		return $this->bool_sticky;
	}
}
