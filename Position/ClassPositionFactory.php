<?php
/**
 * Class ClassPositionFactory, manages the creation of the various classes for Theme JSON settings: Position.
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Position
 */

namespace WPezBlockEditor\ThemeJSONSettings\Position;

// Base.
use WPezBlockEditor\ThemeJSONSettings\ClassFactoryBase;
use WPezBlockEditor\ThemeJSONSettings\InterfaceBooleanPropertiesDefaultsBase;

// Settings' Boolean.
use WPezBlockEditor\ThemeJSONSettings\Position\ClassBooleanPropertiesDefaults;
use WPezBlockEditor\ThemeJSONSettings\Position\ClassBooleanProperties;
use WPezBlockEditor\ThemeJSONSettings\Position\ClassBooleanPropertiesCollection;

// Settings' Unique.
use WPezBlockEditor\ThemeJSONSettings\Position\ClassPositionSettingsCollection as SettingsCollection;

/**
 * ClassPositionFactory
 */
class ClassPositionFactory extends ClassFactoryBase {

	/**
	 * Instance of the Position's ClassBooleanPropertiesDefaults
	 *
	 * @return ClassBooleanPropertiesDefaults
	 */
	protected function newBooleanPropertiesDefaults(): ClassBooleanPropertiesDefaults {

		return new ClassBooleanPropertiesDefaults();
	}

	/**
	 * Creates the instance of the Position's ClassBooleanProperties.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults Instance of the Position's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanProperties
	 */
	protected function newBooleanProperties( InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults ): ClassBooleanProperties {

		return new ClassBooleanProperties( $new_bool_props_defaults );
	}

	/**
	 * Creates the instance of the Position's ClassBooleanPropertiesCollection.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props Instance of the Position's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanPropertiesCollection
	 */
	protected function newBooleanPropertiesCollection( InterfaceBooleanPropertiesDefaultsBase $new_bool_props ): ClassBooleanPropertiesCollection {

		return new ClassBooleanPropertiesCollection( $new_bool_props );
	}

	/**
	 * Creates the instance of the Position's ClassPositionSettingsCollection.
	 *
	 * @param ClassBooleanPropertiesCollection $new_bool_props_collection Instance of the Position's ClassBooleanPropertiesCollection.
	 *
	 * @return SettingsCollection
	 */
	public function createSettingsCollection( ClassBooleanPropertiesCollection $new_bool_props_collection = null ): SettingsCollection {

		if ( null === $new_bool_props_collection ) {
			$this->new_settings_collection = $this->newSettingsCollection( $this->new_bool_props_collection );
		} else {
			$this->new_settings_collection = $this->newSettingsCollection( $new_bool_props_collection );
		}

		return $this->new_settings_collection;
	}

	/**
	 * Returns the property value of new_settings_collection which is an instance of ClassLayoutSettingsCollection.
	 *
	 * @return SettingsCollection
	 */
	public function getSettingsCollection(): SettingsCollection {

		return $this->new_settings_collection;
	}

	/**
	 * Creates the instance of the Position's ClassPositionSettingsCollection.
	 *
	 * @param ClassBooleanPropertiesCollection $new_bool_props_collection Instance of the Position's ClassBooleanPropertiesCollection.
	 *
	 * @return SettingsCollection
	 */
	protected function newSettingsCollection( $new_bool_props_collection ) {

		return new SettingsCollection( $new_bool_props_collection );
	}
}
