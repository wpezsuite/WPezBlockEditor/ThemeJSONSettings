<?php
/**
 * Class ClassBackgroundFactory, manages the creation of the various classes for Theme JSON settings: Background.
 *
 * @package WPezBlockEditor\ThemeJSONSettings
 */

namespace WPezBlockEditor\ThemeJSONSettings;

use WPezBlockEditor\ThemeJSONSettings\InterfaceBooleanPropertiesDefaultsBase;


/**
 * ClassBackgroundFactory
 */
abstract class ClassFactoryBase {

	/**
	 * Instance of the settings type's ClassBooleanPropertiesDefaults.
	 *
	 * @var object Settings type's ClassBooleanPropertiesDefaults
	 */
	protected $new_bool_props_defaults;

	/**
	 * Instance of the settings type's ClassBooleanProperties.
	 *
	 * @var object Settings type's ClassBooleanProperties
	 */
	protected $new_bool_props;

	/**
	 * Instance of the settings type's ClassBooleanPropertiesCollection.
	 *
	 * @var object Settings type's ClassBooleanPropertiesCollection
	 */
	protected $new_bool_props_collection;

	/**
	 * Instance of the settings type's primary settings collection
	 *
	 * @var object Settings type's primary settings collection
	 */
	protected $new_settings_collection;

	// Note: Additional properties can be added to the class that extends this class.

	/**
	 * ClassFactoryBase constructor.
	 */
	public function __construct() {

		$this->setProperties();
	}

	/**
	 * Sets the properties for the class.
	 */
	protected function setProperties() {

		$this->new_bool_props_defaults   = $this->createBoolPropsDefaults();
		$this->new_bool_props            = $this->createBoolProps( $this->new_bool_props_defaults );
		$this->new_bool_props_collection = null;
		$this->new_settings_collection   = null;
	}


	/**
	 * ----- ----- Boolean Properties Defaults ----- -----
	 */

	/**
	 * Creates an instance of ClassBooleanPropertiesDefaults, assigns it to $new_bool_props_defaults, and returns that instance.
	 *
	 * @return object Settings type's ClassBooleanPropertiesDefaults
	 */
	public function createBoolPropsDefaults(): object {

		$this->new_bool_props_defaults = $this->newBooleanPropertiesDefaults();
		return $this->new_bool_props_defaults;
	}

	/**
	 * An abstraction for creating an instance of ClassBooleanPropertiesDefaults that's specific to the current settings type.
	 *
	 * @return object Settings type's ClassBooleanPropertiesDefaults
	 */
	abstract protected function newBooleanPropertiesDefaults();

	/**
	 * Returns the instance of ClassBooleanPropertiesDefaults as currently assigned to the property: new_bool_props_defaults.
	 *
	 * @return object Settings type's ClassBooleanPropertiesDefaults
	 */
	public function getBoolPropsDefaults(): object {

		return $this->new_bool_props_defaults;
	}


	/**
	 * ----- ----- Boolean Properties ----- -----
	 */

	/**
	 * Creates an instance of ClassBooleanProperties, assigns it to $new_bool_props, and returns that instance.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase|null $new_bool_props_defaults An instance of ClassBooleanPropertiesDefaults.
	 *
	 * @return object Settings type's ClassBooleanProperties
	 */
	public function createBoolProps( InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults = null ): object {

		if ( null === $new_bool_props_defaults ) {
			$this->new_bool_props = $this->newBooleanProperties( $this->new_bool_props_defaults );
		} else {
			$this->new_bool_props = $this->newBooleanProperties( $new_bool_props_defaults );
		}
		return $this->new_bool_props;
	}

	/**
	 * An abstraction for creating an instance of ClassBooleanProperties that's specific to the current settings type.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults An instance of ClassBooleanPropertiesDefaults.
	 *
	 * @return object Settings type's ClassBooleanProperties.
	 */
	abstract protected function newBooleanProperties( InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults );

	/**
	 * Returns the property new_bool_props, which is an instance of ClassBooleanProperties for the settings type.
	 *
	 * @return object Settings type's ClassBooleanProperties.
	 */
	public function getBoolProps(): object {

		return $this->new_bool_props;
	}


	/**
	 * ----- ----- Boolean Properties Collection ----- -----
	 */

	/**
	 * Creates an instance of ClassBooleanPropertiesCollection, assigns it to $new_bool_props_collection, and returns that instance.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase|null $new_bool_props_defaults An instance of ClassBooleanPropertiesDefaults.
	 *
	 * @return object Settings type's ClassBooleanPropertiesCollection
	 */
	public function createBoolPropsCollection( InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults = null ): object {

		if ( null === $new_bool_props_defaults ) {
			$this->new_bool_props_collection = $this->newBooleanPropertiesCollection( $this->new_bool_props_defaults );
		} else {
			$this->new_bool_props_collection = $this->newBooleanPropertiesCollection( $new_bool_props_defaults );
		}

		return $this->new_bool_props_collection;
	}

	/**
	 * An abstraction for creating an instance of ClassBooleanPropertiesCollection that's specific to the current settings being created.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults An instance of ClassBooleanPropertiesDefaults.
	 *
	 * @return object Settings type's ClassBooleanPropertiesCollection.
	 */
	abstract protected function newBooleanPropertiesCollection( InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults ): object;

	/**
	 * Returns the property new_bool_props_collection, which is an instance of ClassBooleanPropertiesCollection.
	 *
	 * @return object Settings type's ClassBooleanPropertiesCollection.
	 */
	public function getBoolPropsCollection(): object {

		return $this->new_bool_props_collection;
	}
}
