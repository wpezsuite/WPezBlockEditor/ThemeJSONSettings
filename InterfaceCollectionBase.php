<?php
/**
 * Interface InterfaceCollectionBase
 *
 * @package WPezBlockEditor\ThemeJSONSettings
 */

namespace WPezBlockEditor\ThemeJSONSettings;

/**
 * Interface InterfaceCollectionBase
 */
interface InterfaceCollectionBase {

	/**
	 * Given a $ky, return the collection item.
	 *
	 * @param string $key The array index $key of the collection item to return.
	 */
	public function get( string $key ): array;

}
