<?php
/**
 * Class ClassTypographySettingsCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Typography
 */

namespace WPezBlockEditor\ThemeJSONSettings\Typography;

use WPezBlockEditor\ThemeJSONSettings\InterfaceCollectionBase;
use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;

/**
 * Class ClassTypographyCollection extends ClassCollectionBase.
 */
class ClassTypographySettingsCollection extends ClassCollectionBase {

	/**
	 * The boolean properties collection.
	 *
	 * @var object
	 */
	protected $obj_booleans;

	/**
	 * The font sizes collection.
	 *
	 * @var object
	 */
	protected $obj_font_sizes;

	/**
	 * The font families collection.
	 *
	 * @var object
	 */
	protected $obj_font_families;


	/**
	 * Class constructor.
	 *
	 * @param InterfaceCollectionBase $booleans      Instance of the collection class for booleans that implements InterfaceCollectionBase.
	 * @param InterfaceCollectionBase $font_sizes    Instance  of the collection class for font sizes that implements InterfaceCollectionBase.
	 * @param InterfaceCollectionBase $font_families Instance  of the collection class for font families that implements InterfaceCollectionBase.
	 */
	public function __construct( InterfaceCollectionBase $booleans, InterfaceCollectionBase $font_sizes, InterfaceCollectionBase $font_families ) {

		$this->obj_booleans      = $booleans;
		$this->obj_font_sizes    = $font_sizes;
		$this->obj_font_families = $font_families;

		$this->setProperties();
	}

	/**
	 * Adds a item to the collection.
	 *
	 * @param string $key The array index key of the collection being added.
	 * @param array  $args The additional arguments / values of the collection being added.
	 * - 'key_bools' (string): The key of the boolean collection.
	 * - 'array_sizes' (array): The array of font size keys.
	 * - 'array_families' (array): The array of font family keys.
	 *
	 * @return object $this
	 */
	public function add( string $key, array $args ): object {

		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}
		if ( $this->isValid( $args ) ) {

			$arr_props = $this->obj_booleans->get( $args['key_bools'] );

			$arr_sizes = array();
			foreach ( $args['array_sizes'] as $key_size ) {
				$arr_sizes[] = $this->obj_font_sizes->get( $key_size );
			}
			$arr_props['fontSizes'] = $arr_sizes;

			$arr_families = array();
			foreach ( $args['array_families'] as $key_family ) {
				$arr_families[] = $this->obj_font_families->get( $key_family );
			}
			$arr_props['fontFamilies'] = $arr_families;

			$this->arr_collection[ $key ] = $arr_props;
		}
		return $this;
	}


	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The additional arguments / values of the collection being added.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['key_bools'], $args['array_sizes'], $args['array_families'] ) ) {
			return false;
		}
		if ( ! is_string( $args['key_bools'] ) ) {
			return false;
		}
		if ( ! is_array( $args['array_sizes'] ) || ! is_array( $args['array_families'] ) ) {
			return false;
		}
		return true;
	}
}
