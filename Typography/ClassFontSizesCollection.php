<?php
/**
 * Class ClassFontSizesCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Typography
 */

namespace WPezBlockEditor\ThemeJSONSettings\Typography;

use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;

/**
 * Class ClassFontSizesCollection  extends ClassCollectionBase.
 */
class ClassFontSizesCollection extends ClassCollectionBase {

	/**
	 * Adds a new font size to the collection.
	 *
	 * @param string $key The unique identifier for collection being added. If blank, the args' slug will be used.
	 * @param array  $args The associative array of additional arguments / values for the font size.
	 * - 'fluid' (array): Optional - Whether or not the font size is fluid. Default is false.
	 * - 'name' (string): The name of the font size.
	 * - 'size' (string): Optional ( but if fluid not not set, size is required). The size of the font size. Defaults to fluid's 'max'.
	 * - 'slug' (string): The slug of the font size.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {
		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}
		if ( $this->isValid( $args ) ) {

			$temp_fluid = false;
			if ( isset( $args['fluid'] ) ) {
				$temp_fluid = $args['fluid'];
				$temp_size  = $args['fluid']['max'];
			} else {
				$temp_size = $args['size'];
			}

			$this->arr_collection[ $key ] = array(
				'fluid' => $temp_fluid,
				'name'  => $args['name'],
				'size'  => $temp_size,
				'slug'  => $args['slug'],
			);
		}
		return $this;
	}

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The associative array of additional arguments / values for the font size.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['name'], $args['slug'] )
			|| ! is_string( $args['name'] ) || ! is_string( $args['slug'] )
			|| ( isset( $args['fluid'] ) && ! isset( $args['fluid']['min'], $args['fluid']['max'] ) )
			|| ( ! isset( $args['fluid'] ) && ! isset( $args['size'] ) ) ) {
			return false;
		}
		return true;
	}
}
