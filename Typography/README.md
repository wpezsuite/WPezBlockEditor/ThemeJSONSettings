## Theme JSON Settings: Typography

__Settings related to Typography.__

https://developer.wordpress.org/block-editor/reference-guides/theme-json-reference/theme-json-living/#typography

### A Basic Example

This example replicates the Typography settings from the Twenty Twenty-Four theme, sans the Booleans which will all be set to false.


```

// We need to "as" these *Boolean* classes because the class name - but not namespace - is the same 
// across the different settings types. We need a unique name for each.
use WPezBlockEditor\ThemeJSONSettings\Typography\ClassBooleanPropertiesDefaults as TypeBoolPropsDefs;
use WPezBlockEditor\ThemeJSONSettings\Typography\ClassBooleanProperties as TypeBoolProps;
use WPezBlockEditor\ThemeJSONSettings\Typography\ClassBooleanPropertiesCollection as TypeBoolPropsCollection;

// These are unique to Typography.
use WPezBlockEditor\ThemeJSONSettings\Typography\ClassFontFaceCollection;
use WPezBlockEditor\ThemeJSONSettings\Typography\ClassFontFamiliesCollection;
use WPezBlockEditor\ThemeJSONSettings\Typography\ClassFontSizesCollection;


// And this will wrap up all the above into the Typography settings.
use WPezBlockEditor\ThemeJSONSettings\Typography\ClassTypographySettingsCollection;


add_filter( 'wp_theme_json_data_user', __NAMESPACE__ . '\fn_wp_theme_json_data_user' );

fn_wp_theme_json_data_user() {

	// Let's begin...
		
	// Instantiate the Typography's Boolean Props Defaults.
	// Note: The Boolean Props Defaults class provides a "canonical" set of bool defaults for a given Settings type. Settings type is Typography in this case.
	// Note: This class has a single method: updatePropDefault() for updating the default values.
	// It's possible that you might want to have your own GOTO set of defaults, so you can use the updatePropDefault() method to do that.
	$new_type_bools_defs = new TypeBoolPropsDefs();

	// Updating a default value would look something like this. Note: It's intentionally commented out. 
	// $new_type_bool_props_defs->updatePropDefault( 'bool_letter_spacing', true );

	// Next, instantiate the Typography's Boolean Props and pass in the instance of the Bools Defaults.
	// This class has dedicated set (and get) methods for updating (and getting) the individual property values.
	// That is, your IDE / editor will be able to autocomplete the method names for each property. You don't have to remember them.
	$new_type_bools_props = new TypeBoolProps( $new_type_bools_defs );

	// Instantiate the Typography's Boolean Props Collection and pass in the instance of the Bools Props.
	// The instance of $new_type_bool_props is passed in so the Collection is able to validate
	// that all the properties are legit / valid. *Defaults are the "template" for the boolean properties.
	$new_type_bools_collect = new TypeBoolPropsCollection( $new_type_bools_defs );

	// Add a collection of booleans based on $new_type_bool_props and getBooleans(), name it 'type_bools_0'.
	// Note: The second $arg is an array. 
	$new_type_bools_collect->add( 'type_bools_0', $new_type_bools_props->getBooleans() );

	// Instantiate the Typography's Font Face Collection.
	$new_type_font_face_collect = new ClassFontFaceCollection();

	// This class works a bit differently than the other collection classes.
	// Since many of the properties' values repeat (i.e., don't change) from one "row" to the next,
	// you set X, set Y, set Z, add(). Then set X, set Z, add(). And so on.
	// You only set the properties that change from one "row" to the next, the previous values are retained.
	// Note: There's also a method: resetProperties() that resets all the properties - Family, Stretch, Style and Weight - to their default values.
	$new_type_font_face_collect
	->setFamily( 'Inter' )
	->setStretch( 'normal' )
	->setStyle( 'normal' )
	->setWeight( '300 900' )
	->setSrc( array( 'file:./assets/fonts/inter/Inter-VariableFont_slnt,wght.woff2' ) )
	// When using the addThis() method, the current property values are added to the collection.
	// If the key / name is not specified, then it is created from the property values: {family}_{style}_{weight}
	->addThis()
	->setFamily( 'Cardo' )
	// this will reset stretch to ''.
	->setStretch()
	->setWeight( '400' )
	->setSrc( array( 'file:./assets/fonts/cardo/cardo_normal_400.woff2' ) )
	->addThis()
	->setStyle( 'italic' )
	->addThis()
	->setStyle( 'normal' )
	->setWeight( '700' )
	->setSrc( array( 'file:./assets/fonts/cardo/cardo_normal_700.woff2' ) )
	->addThis();

	// Instantiate the Typography's Font Families Collection.
	$new_type_font_fam_collect = new ClassFontFamiliesCollection( $new_type_font_face_collect );

	// Note: the face's 'Inter_normal_300 900' {family}_{style}_{weight}
	$arr_inter = array(
		'face'   => array( 'Inter_normal_300 900' ),
		'family' => '\"Inter\", sans-serif',
		'name'   => 'Inter',
		'slug'   => 'body',
	);
	
	// add inter 
	$new_type_font_fam_collect->add( 'inter', $arr_inter );

	$arr_cardo = array(
		'face'   => array( 'Cardo_normal_400', 'Cardo_italic_400', 'Cardo_normal_700' ),
		'family' => 'Cardo',
		'name'   => 'Cardo',
		'slug'   => 'heading',
	);
	
	// add cardo
	$new_type_font_fam_collect->add( 'cardo', $arr_cardo );

	$arr_sys_sans_serif = array(
		'family' => '-apple-system, BlinkMacSystemFont, avenir next, avenir, segoe ui, helvetica neue, helvetica, Cantarell, Ubuntu, roboto, noto, arial, sans-serif',
		'name'   => 'System Sans-serif',
		'slug'   => 'system-sans-serif',
	);

	// add: sys_sans_serif
	$new_type_font_fam_collect->add( 'sys_sans_serif', $arr_sys_sans_serif );

	$arr_sys_sans_serif = array(
		'family' => 'Iowan Old Style, Apple Garamond, Baskerville, Times New Roman, Droid Serif, Times, Source Serif Pro, serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol',
		'name'   => 'System Serif',
		'slug'   => 'system-serif',
	);

	// sys_serif
	$new_type_font_fam_collect->add( 'sys_serif', $arr_sys_sans_serif );

	// Instantiate the Typography's Font Sizes Collection.
	$new_type_font_sizes_collect = new ClassFontSizesCollection();

	$arr_font_sizes = array(
		array(
			// if 'fluid' is not set, it defaults to false
			'name' => 'Small',
			'size' => '0.9rem',
			'slug' => 'small',
		),
		array(
			'name' => 'Medium',
			'size' => '1.05rem',
			'slug' => 'medium',
		),
		array(
			'fluid' => array(
				'min' => '1.39rem',
				'max' => '1.85rem',
			),
			// if fluid is set, then the value 'max' is used for the value 'size'. 
			'name'  => 'Large',
			'slug'  => 'large',
		),
		array(
			'fluid' => array(
				'min' => '1.85em',
				'max' => '2.5rem',
			),
			'name'  => 'Extra Large',
			'slug'  => 'x-large',
		),
		array(
			'fluid' => array(
				'min' => '2.5rem',
				'max' => '3.27rem',
			),
			'name'  => 'Extra Extra Large',
			'slug'  => 'xx-large',
		),

	);
	
	// Add by looping over the above array 
	foreach ( $arr_font_sizes as $arr_font_size ) {
		$new_type_font_sizes_collect->add( $arr_font_size['slug'], $arr_font_size );
	}
	
	// Instantiate the Typography Collection and pass in the instances of the Bool Props Collection, Font Families Collection and Font Sizes Collection.
	$new_type_settings = new ClassTypographySettingsCollection( $new_type_bools_collect, $new_type_font_sizes_collect, $new_type_font_fam_collect );

	$arr_args_type_1 = array(
		'key_bools'      => 'type_bools_0',
		'array_sizes'    => $new_type_font_sizes_collect->getKeys(),
		'array_families' => $new_type_font_fam_collect->getKeys(),
	);

	// Add a collection - 'type_1' - along with an array of args that define this collection.
	$new_type_settings->add( 'type_1', $arr_args_type_1 );


	// That's it, now you can use get() to get the collection you want to use as part of your Theme JSON
	$new_type_settings->get( 'type_1' );


}

```