<?php
/**
 * Class ClassBooleanPropertiesDefaults
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Typography
 */

namespace WPezBlockEditor\ThemeJSONSettings\Typography;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesDefaultsBase;

class ClassBooleanPropertiesDefaults extends ClassBooleanPropertiesDefaultsBase {

	/**
	 * Set property defaults, specialized for this class / Theme JSON object type.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_defaults = array(

			'bool_custom_font_size' => array(
				'block_prop' => 'customFontSize',
				'get'        => 'getCustomFontSize',
				'default'    => false,
			),
			'bool_font_style'       => array(
				'block_prop' => 'fontStyle',
				'get'        => 'getFontStyle',
				'default'    => false,
			),
			'bool_font_weight'      => array(
				'block_prop' => 'fontWeight',
				'get'        => 'getFontWeight',
				'default'    => false,
			),
			'bool_fluid'            => array(
				'block_prop' => 'fluid',
				'get'        => 'getFluid',
				'default'    => false,
			),
			'bool_letter_spacing'   => array(
				'block_prop' => 'letterSpacing',
				'get'        => 'getLetterSpacing',
				'default'    => false,
			),
			'bool_line_height'      => array(
				'block_prop' => 'lineHeight',
				'get'        => 'getLineHeight',
				'default'    => false,
			),
			'bool_text_columns'     => array(
				'block_prop' => 'textColumns',
				'get'        => 'getTextColumns',
				'default'    => false,
			),
			'bool_text_decoration'  => array(
				'block_prop' => 'textDecoration',
				'get'        => 'getTextDecoration',
				'default'    => false,
			),
			'bool_writing_mode'     => array(
				'block_prop' => 'writingMode',
				'get'        => 'getWritingMode',
				'default'    => false,
			),
			'bool_text_transform'   => array(
				'block_prop' => 'textTransform',
				'get'        => 'getTextTransform',
				'default'    => false,
			),
			'bool_drop_cap'         => array(
				'block_prop' => 'dropCap',
				'get'        => 'getDropCap',
				'default'    => false,
			),
		);
	}
}
