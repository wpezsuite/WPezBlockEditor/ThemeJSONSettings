<?php
/**
 * Class ClassBooleanProperties
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Typography
 */

namespace WPezBlockEditor\ThemeJSONSettings\Typography;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesBase;

/**
 * Class ClassBooleanProperties
 */
class ClassBooleanProperties extends ClassBooleanPropertiesBase {

	/**
	 * Whether to use the custom font size.
	 *
	 * @var bool
	 */
	protected $bool_custom_font_size;

	/**
	 * Whether to use the font style.
	 *
	 * @var bool
	 */
	protected $bool_font_style;

	/**
	 * Whether to use the font weight.
	 *
	 * @var bool
	 */
	protected $bool_font_weight;

	/**
	 * Whether to use the fluid.
	 *
	 * @var bool
	 */
	protected $bool_fluid;

	/**
	 * Whether to use the letter spacing.
	 *
	 * @var bool
	 */
	protected $bool_letter_spacing;

	/**
	 * Whether to use the line height.
	 *
	 * @var bool
	 */
	protected $bool_line_height;

	/**
	 * Whether to use the text columns.
	 *
	 * @var bool
	 */
	protected $bool_text_columns;

	/**
	 * Whether to use the text decoration.
	 *
	 * @var bool
	 */
	protected $bool_text_decoration;

	/**
	 * Whether to use the writing mode.
	 *
	 * @var bool
	 */
	protected $bool_writing_mode;

	/**
	 * Whether to use the text transform.
	 *
	 * @var bool
	 */
	protected $bool_text_transform;

	/**
	 * Whether to use the drop cap.
	 *
	 * @var bool
	 */
	protected $bool_drop_cap;

	/**
	 * Set property: bool_custom_font_size.
	 *
	 * @param boolean $bool_custom_font_size Whether to use the custom font size.
	 *
	 * @return object $this
	 */
	public function setCustomFontSize( bool $bool_custom_font_size = true ): object {

		$this->bool_custom_font_size = $bool_custom_font_size;
		return $this;
	}

	/**
	 * Get property: bool_custom_font_size.
	 *
	 * @return bool
	 */
	public function getCustomFontSize(): bool {

		return $this->bool_custom_font_size;
	}

	/**
	 * Set property: bool_font_style.
	 *
	 * @param boolean $bool_font_style Whether to use the font style.
	 *
	 * @return object $this
	 */
	public function setFontStyle( bool $bool_font_style = true ): object {

		$this->bool_font_style = $bool_font_style;
		return $this;
	}

	/**
	 * Get property: bool_font_style.
	 *
	 * @return bool
	 */
	public function getFontStyle(): bool {

		return $this->bool_font_style;
	}

	/**
	 * Set property: bool_font_weight.
	 *
	 * @param boolean $bool_font_weight Whether to use the font weight.
	 *
	 * @return object $this
	 */
	public function setFontWeight( bool $bool_font_weight = true ): object {

		$this->bool_font_weight = $bool_font_weight;
		return $this;
	}

	/**
	 * Get property: bool_font_weight.
	 *
	 * @return bool
	 */
	public function getFontWeight(): bool {

		return $this->bool_font_weight;
	}

	/**
	 * Set property: bool_fluid.
	 *
	 * @param boolean $bool_fluid Whether to use the fluid.
	 *
	 * @return object $this
	 */
	public function setFluid( bool $bool_fluid = true ): object {

		$this->bool_fluid = $bool_fluid;
		return $this;
	}

	/**
	 * Get property: bool_fluid.
	 *
	 * @return bool
	 */
	public function getFluid(): bool {

		return $this->bool_fluid;
	}

	/**
	 * Set property: bool_letter_spacing.
	 *
	 * @param boolean $bool_letter_spacing Whether to use the letter spacing.
	 *
	 * @return object $this
	 */
	public function setLetterSpacing( bool $bool_letter_spacing = true ): object {

		$this->bool_letter_spacing = $bool_letter_spacing;
		return $this;
	}

	/**
	 * Get property: bool_letter_spacing.
	 *
	 * @return bool
	 */
	public function getLetterSpacing(): bool {

		return $this->bool_letter_spacing;
	}

	/**
	 * Set property: bool_line_height.
	 *
	 * @param boolean $bool_line_height Whether to use the line height.
	 *
	 * @return object $this
	 */
	public function setLineHeight( bool $bool_line_height = true ): object {

		$this->bool_line_height = $bool_line_height;
		return $this;
	}

	/**
	 * Get property: bool_line_height.
	 *
	 * @return bool
	 */
	public function getLineHeight(): bool {

		return $this->bool_line_height;
	}

	/**
	 * Set property: bool_text_columns.
	 *
	 * @param boolean $bool_text_columns Whether to use the text columns.
	 *
	 * @return object $this
	 */
	public function setTextColumns( bool $bool_text_columns = true ): object {

		$this->bool_text_columns = $bool_text_columns;
		return $this;
	}

	/**
	 * Get property: bool_text_columns.
	 *
	 * @return bool
	 */
	public function getTextColumns(): bool {

		return $this->bool_text_columns;
	}

	/**
	 * Set property: bool_text_decoration.
	 *
	 * @param boolean $bool_text_decoration Whether to use the text decoration.
	 *
	 * @return object $this
	 */
	public function setTextDecoration( bool $bool_text_decoration = true ): object {

		$this->bool_text_decoration = $bool_text_decoration;
		return $this;
	}

	/**
	 * Get property: bool_text_decoration.
	 *
	 * @return bool
	 */
	public function getTextDecoration(): bool {

		return $this->bool_text_decoration;
	}

	/**
	 * Set property: bool_writing_mode.
	 *
	 * @param boolean $bool_writing_mode Whether to use the writing mode.
	 *
	 * @return object $this
	 */
	public function setWritingMode( bool $bool_writing_mode = true ): object {

		$this->bool_writing_mode = $bool_writing_mode;
		return $this;
	}

	/**
	 * Get property: bool_writing_mode.
	 *
	 * @return bool
	 */
	public function getWritingMode(): bool {

		return $this->bool_writing_mode;
	}

	/**
	 * Set property: bool_text_transform.
	 *
	 * @param boolean $bool_text_transform Whether to use the text transform.
	 *
	 * @return object $this
	 */
	public function setTextTransform( bool $bool_text_transform = true ): object {

		$this->bool_text_transform = $bool_text_transform;
		return $this;
	}

	/**
	 * Get property: bool_text_transform.
	 *
	 * @return bool
	 */
	public function getTextTransform(): bool {

		return $this->bool_text_transform;
	}

	/**
	 * Set property: bool_drop_cap.
	 *
	 * @param boolean $bool_drop_cap Whether to use the drop cap.
	 *
	 * @return object $this
	 */
	public function setDropCap( bool $bool_drop_cap = true ): object {

		$this->bool_drop_cap = $bool_drop_cap;
		return $this;
	}

	/**
	 * Get property: bool_drop_cap.
	 *
	 * @return bool
	 */
	public function getDropCap(): bool {

		return $this->bool_drop_cap;
	}
}
