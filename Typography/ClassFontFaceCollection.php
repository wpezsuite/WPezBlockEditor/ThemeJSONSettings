<?php
/**
 * Class ClassFontFaceCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Typography
 */

namespace WPezBlockEditor\ThemeJSONSettings\Typography;

use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;

/**
 * Class ClassFontFamiliesCollection extends ClassCollectionBase.
 */
class ClassFontFaceCollection extends ClassCollectionBase {

	/**
	 * The font face family
	 *
	 * @var string
	 */
	protected $str_family;

	/**
	 * The font face stretch
	 *
	 * @var string
	 */
	protected $str_stretch;

	/**
	 * The font face style
	 *
	 * @var string
	 */
	protected $str_style;

	/**
	 * The font face weight
	 *
	 * @var string
	 */
	protected $str_weight;

	/**
	 * The font face src
	 *
	 * @var array
	 */
	protected $arr_src;

	/**
	 * ClassFontFaceCollection constructor.
	 */
	public function __construct() {

		$this->setProperties();
	}

	/**
	 * Sets the properties.
	 */
	protected function setProperties() {

		$this->arr_collection      = array();
		$this->bool_overwrite_dupe = true;

		$this->resetProperties();
	}

	/**
	 * Resets the properties.
	 */
	public function resetProperties() {

		$this->str_family  = '';
		$this->str_stretch = '';
		$this->str_style   = '';
		$this->str_weight  = '';
		$this->arr_src     = array();
	}

	/**
	 * Set property str_family.
	 *
	 * @param string $str_family Value to set for str_family.
	 *
	 * @return object $this.
	 */
	public function setFamily( string $str_family = '' ): object {
		$this->str_family = $str_family;
		return $this;
	}

	/**
	 * Get property str_family.
	 *
	 * @return string
	 */
	public function getFamily(): string {
		return $this->str_family;
	}

	/**
	 * Set property str_stretch.
	 *
	 * @param string $str_stretch Value to set for str_stretch.
	 *
	 * @return object $this.
	 */
	public function setStretch( string $str_stretch = '' ): object {
		$this->str_stretch = $str_stretch;
		return $this;
	}

	/**
	 * Get property str_stretch.
	 *
	 * @return string
	 */
	public function getStretch(): string {
		return $this->str_stretch;
	}

	/**
	 * Set property str_style.
	 *
	 * @param string $str_style Value to set for str_style.
	 *
	 * @return object $this.
	 */
	public function setStyle( string $str_style = '' ): object {
		$this->str_style = $str_style;
		return $this;
	}

	/**
	 * Get property str_style.
	 *
	 * @return string
	 */
	public function getStyle(): string {
		return $this->str_style;
	}

	/**
	 * Set property str_weight.
	 *
	 * @param string $str_weight Value to set for str_weight.
	 *
	 * @return object $this.
	 */
	public function setWeight( string $str_weight = '' ): object {
		$this->str_weight = $str_weight;
		return $this;
	}

	/**
	 * Get property str_weight.
	 *
	 * @return string
	 */
	public function getWeight(): string {
		return $this->str_weight;
	}

	/**
	 * Set property arr_src.
	 *
	 * @param array $arr_src Value to set for arr_src.
	 *
	 * @return object $this.
	 */
	public function setSrc( array $arr_src = array() ): object {
		$this->arr_src = $arr_src;
		return $this;
	}

	/**
	 * Get property arr_src.
	 *
	 * @return array
	 */
	public function getSrc(): array {
		return $this->arr_src;
	}

	/**
	 * Add the font face to the collection based on the current property values.
	 *
	 * @param string $key The key to use for the collection.
	 *
	 * @return object $this
	 */
	public function addThis( string $key = '' ): object {

		if ( empty( $key ) ) {
			$key = $this->getFamily() . '_' . $this->getStyle() . '_' . $this->getWeight();
		}

		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}

		$this->arr_collection[ $key ] = array(
			'fontFamily'  => $this->getFamily(),
			'fontStretch' => $this->getStretch(),
			'fontStyle'   => $this->getStyle(),
			'fontWeight'  => $this->getWeight(),
			'src'         => $this->getSrc(),
		);
		if ( empty( $this->str_stretch ) ) {
			unset( $this->arr_collection[ $key ]['fontStretch'] );
		}

		return $this;
	}

	/**
	 * Alternative to addThis(), adds a new font face to the collection.
	 *
	 * @param string $key  The unique identifier for collection being added.
	 * @param array  $args The associative array of additional arguments / values for the font face.
	 * - 'family' (string): The font family.
	 * - 'stretch' (string): Optional - The font stretch.
	 * - 'style' (string): The font style.
	 * - 'weight' (string): The font weight.
	 * - 'src' (array): The font src.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {

		if ( $this->isValid( $args ) ) {

			$this->setFamily( $args['family'] );
			$this->setStretch( $args['stretch'] );
			$this->setStyle( $args['style'] );
			$this->setWeight( $args['weight'] );
			$this->setSrc( $args['src'] );

			$this->addThis( $key );
		}

		return $this;
	}

	/**
	 * Whether the font face args are valid.
	 *
	 * @param array $args The associative array of additional arguments / values for the font face.
	 * - 'family' (string): The font family.
	 * - 'stretch' (string): Optional - The font stretch.
	 * - 'style' (string): The font style.
	 * - 'weight' (string): The font weight.
	 * - 'src' (array): The font src.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {
		if ( ! isset( $args['family'], $args['style'], $args['weight'], $args['src'] )
		|| empty( $args['family'] ) || empty( $args['style'] ) || empty( $args['weight'] ) || empty( $args['src'] )
		|| ! is_string( $args['family'] ) || ! is_string( $args['style'] ) || ! is_string( $args['weight'] ) || ! is_array( $args['src'] )
		|| ( isset( $args['stretch'] ) && ( ! is_string( $args['stretch'] ) || empty( $args['stretch'] ) ) ) ) {
			return false;
		}
		return true;
	}
}
