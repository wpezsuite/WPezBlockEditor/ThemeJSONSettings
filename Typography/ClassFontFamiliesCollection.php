<?php
/**
 * Class ClassFontFamiliesCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Typography
 */

namespace WPezBlockEditor\ThemeJSONSettings\Typography;

use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;

/**
 * Class ClassFontFamiliesCollection extends ClassCollectionBase.
 */
class ClassFontFamiliesCollection extends ClassCollectionBase {


	/**
	 * The Font Face collection object.
	 *
	 * @var object
	 */
	protected $obj_font_face;


	/**
	 * ClassFontFamiliesCollection constructor.
	 *
	 * @param ClassCollectionBase $obj_font_face The Font Face collection object.
	 */
	public function __construct( ClassCollectionBase $obj_font_face ) {

		$this->setProperties();
		$this->obj_font_face = $obj_font_face;
	}

	/**
	 * Adds a new font size to the collection.
	 *
	 * @param string $key  The unique identifier for collection being added.
	 * @param array  $args The associative array of additional arguments / values for the font family.
	 * - 'face' (array): Array of Font Face keys.
	 * - 'family' (string): The font family.
	 * - 'name' (string): The name of the font family.
	 * - 'slug' (string): The slug of the font family.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {
		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}

		if ( $this->isValid( $args ) ) {

			$arr_face = array();
			if ( isset( $args['face'] ) && is_array( $args['face'] ) ) {
				foreach ( $args['face'] as $key_face ) {
					$arr_temp = $this->obj_font_face->get( $key_face );
					if ( ! empty( $arr_temp ) ) {
						$arr_face[] = $arr_temp;
					}
				}
			}

			$this->arr_collection[ $key ] = array(
				'fontFace'   => $arr_face,
				'fontFamily' => $args['family'],
				'name'       => $args['name'],
				'slug'       => $args['slug'],
			);

			if ( empty( $arr_face ) ) {
				unset( $this->arr_collection[ $key ]['fontFace'] );
			}
		}
		return $this;
	}

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The associative array of additional arguments / values for the font family.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['family'], $args['name'], $args['slug'] )
			|| ! is_string( $args['family'] ) || ! is_string( $args['name'] ) || ! is_string( $args['slug'] ) ) {
			return false;
		}
		return true;
	}
}
