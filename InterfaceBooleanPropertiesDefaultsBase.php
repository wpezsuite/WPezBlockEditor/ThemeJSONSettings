<?php
/**
 * Interface InterfaceBooleanPropertiesDefaultsBase
 *
 * @package WPezBlockEditor\ThemeJSONSettings
 */
namespace WPezBlockEditor\ThemeJSONSettings;

/**
 * Interface InterfaceBooleanPropertiesDefaultBase
 */
interface InterfaceBooleanPropertiesDefaultsBase {

	/**
	 * Return the property $arr_defaults array.
	 *
	 * @return array An associative array of property defaults.
	 */
	public function getAll(): array;

	/**
	 * Return an array of the the block_prop values.
	 *
	 * @return array An array of the the block_prop values. 
	 */
	public function getBlockProps(): array;
}
