<?php
/**
 * Class ClassBooleanProperties
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Layout
 */

namespace WPezBlockEditor\ThemeJSONSettings\Layout;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesBase;

/**
 * Class ClassBooleanProperties
 */
class ClassBooleanProperties extends ClassBooleanPropertiesBase {

	/**
	 * Flag for allowEditing property Theme JSON settings: Layout.
	 *
	 * @var boolean
	 */
	protected $bool_allow_editing;

	/**
	 * Flag for allowCustomContentAndWideSize property Theme JSON settings: Layout.
	 *
	 * @var boolean
	 */
	protected $bool_allow_custom_content_and_wide_size;

	/**
	 * Sets the bool_allow_editing property.
	 *
	 * @param boolean $bool_allow_editing The value of the bool_allow_editing property.
	 *
	 * @return object $this
	 */
	public function setAllowEditing( bool $bool_allow_editing = true ): object {

		$this->bool_allow_editing = $bool_allow_editing;
		return $this;
	}

	/**
	 * Gets the bool_allow_editing property.
	 *
	 * @return boolean
	 */
	public function getAllowEditing(): bool {

		return $this->bool_allow_editing;
	}

	/**
	 * Sets the bool_allow_custom_content_and_wide_size property.
	 *
	 * @param boolean $bool_allow_custom_content_and_wide_size The value of the bool_allow_custom_content_and_wide_size property.
	 *
	 * @return object $this
	 */
	public function setAllowCustomContentAndWideSize( bool $bool_allow_custom_content_and_wide_size = true ): object {

		$this->bool_allow_custom_content_and_wide_size = $bool_allow_custom_content_and_wide_size;
		return $this;
	}

	/**
	 * Gets the bool_allow_custom_content_and_wide_size property.
	 *
	 * @return boolean
	 */
	public function getAllowCustomContentAndWideSize(): bool {

		return $this->bool_allow_custom_content_and_wide_size;
	}
}
