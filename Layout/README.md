## Theme JSON Settings: Layout

__Settings related to Layout.__

https://developer.wordpress.org/block-editor/reference-guides/theme-json-reference/theme-json-living/#layout

### A Basic Example

In addition to the Booleans classes, there is also a Sizes Collection class. This class is used to define the various sizes that are available for a given setting. 

```

// We'll skip the traditional example and do an example using the factory class. 

// We only need a single use statement as the factory encapsulates all of the classes we need.
use WPezBlockEditor\ThemeJSONSettings\Layout\ClassLayoutFactory;

add_filter( 'wp_theme_json_data_user', __NAMESPACE__ . '\fn_wp_theme_json_data_user' );

fn_wp_theme_json_data_user() {

	// Instantiate the factory class.
	// Automatically, it'll also instantiate the Bool Props Defaults class and the Bool Props class.
	$lay_fact = new ClassLayoutFactory();

	// Create the Settings Collection class, in this case Layout Settings Collection class.
	$lay_set_coll = $lay_fact->createSettingsCollection();

	// Get the instance of the Layout Boolean Properties class.
	// Note: Technically, we don't need this class, but it makes creating the value pairs 10x easier.
	$lay_bool_props = $lay_fact->getBoolProps();

	// Get the instance of the Layout Boolean Properties Collection class.
	$lay_bool_props_coll = $lay_fact->getBoolPropsCollection();

	// Add a collection, name it 'key_1'
	$lay_bool_props_coll->add( 'key_1', $lay_bool_props->getBooleans() );

	// Update the bool props instance to allow editing.
	$lay_bool_props->setAllowEditing( true );

	// Add another collection, name it 'key_2'
	$lay_bool_props_coll->add( 'key_2', $lay_bool_props->getBooleans() );
		
	// get the instance of the Layout Sizes Collection class.
	$lay_sizes_coll = $x->getSizesCollection();

	// Add a sizes collection, name it 'sizes_1'
	// Note: The values of content and wide are arbitrary. They can be anything you want.
	$lay_sizes_coll->add(
		'sizes_1',
		array(
			'content' => '50%',
			'wide'    => '100%',
		)
	);

	// Add another sizes collection, name it 'sizes_2'
	$lay_sizes_coll->add(
		'sizes_2',
		array(
			'content' => '1000px',
			'wide'    => '1400px',
		)
	);

	// Add a layout setting collection, name it 'layout_1'
	$lay_set_coll->add(
		'layout_1',
		array(
			'key_bools' => 'key_2',
			'key_sizes' => 'sizes_1',
		)
	);
	$lay_set_coll->add(
		'layout_2',
		array(
			'key_bools' => 'key_2',
			'key_sizes' => 'sizes_2',
		)
	);

	// As you need them, get the layout setting collection you want to use.
	// The array returned is formatted for use in the theme.json file, and just needs to be dropped into place.
	$lay_set_coll->get( 'layout_1' );

}

```