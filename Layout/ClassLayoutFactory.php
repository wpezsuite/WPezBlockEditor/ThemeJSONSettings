<?php
/**
 * Class ClassBackgroundFactory, manages the creation of the various classes for Theme JSON settings: Layout.
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Layout
 */

namespace WPezBlockEditor\ThemeJSONSettings\Layout;

// Base.
use WPezBlockEditor\ThemeJSONSettings\ClassFactoryBase;
use WPezBlockEditor\ThemeJSONSettings\InterfaceBooleanPropertiesDefaultsBase;

// Settings' Boolean.
use WPezBlockEditor\ThemeJSONSettings\Layout\ClassBooleanPropertiesDefaults;
use WPezBlockEditor\ThemeJSONSettings\Layout\ClassBooleanProperties;
use WPezBlockEditor\ThemeJSONSettings\Layout\ClassBooleanPropertiesCollection;

// Settings' Unique.
use WPezBlockEditor\ThemeJSONSettings\Layout\ClassSizesCollection;
use WPezBlockEditor\ThemeJSONSettings\Layout\ClassLayoutSettingsCollection as SettingsCollection;


/**
 * ClassBackgroundFactory
 */
class ClassLayoutFactory extends ClassFactoryBase {

	/**
	 * Instance of the Layout's ClassSizesCollection
	 *
	 * @var ClassSizesCollection
	 */
	protected $new_sizes_collection;

	/**
	 * Sets the properties for the class.
	 *
	 * @return void
	 */
	protected function setProperties() {

		parent::setProperties();

		$this->new_sizes_collection = null;
	}

	/**
	 * Creates the instance of the Layout's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanPropertiesDefaults
	 */
	protected function newBooleanPropertiesDefaults(): ClassBooleanPropertiesDefaults {

		return new ClassBooleanPropertiesDefaults();
	}

	/**
	 * Creates the instance of the Layout's ClassBooleanProperties.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults - Instance of the Layout's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanProperties
	 */
	protected function newBooleanProperties( InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults ): ClassBooleanProperties {

		return new ClassBooleanProperties( $new_bool_props_defaults );
	}

	/**
	 * Creates the instance of the Layout's ClassBooleanPropertiesCollection.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props - Instance of the Layout's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanPropertiesCollection
	 */
	protected function newBooleanPropertiesCollection( InterfaceBooleanPropertiesDefaultsBase $new_bool_props ): ClassBooleanPropertiesCollection {

		return new ClassBooleanPropertiesCollection( $new_bool_props );
	}

	/**
	 * ----- ----- Methods that are unique to Layout ----- -----
	 */

	/**
	 * Returns the property value of new_sizes_collection which is an instance of ClassSizesCollection.
	 *
	 * @return ClassSizesCollection
	 */
	public function createSizesCollection(): ClassSizesCollection {

		$this->new_sizes_collection = $this->newSizesCollection();
		return $this->new_sizes_collection;
	}

	/**
	 * Returns the property value of new_bool_props_defaults which is an instance of ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassSizesCollection {
	 */
	protected function newSizesCollection(): ClassSizesCollection {

		return new ClassSizesCollection();
	}

	/**
	 * Returns the property value of new_sizes_collection which is an instance of ClassSizesCollection.
	 *
	 * @return ClassSizesCollection
	 */
	public function getSizesCollection(): ClassSizesCollection {

		return $this->new_sizes_collection;
	}

	/**
	 * Creates an instance of ClassLayoutSettingsCollection, assigns it to $new_settings_collection, and returns that instance.
	 *
	 * @param ClassBooleanPropertiesCollection|null $new_bool_props_collection - An instance of ClassBooleanPropertiesCollection.
	 * @param ClassSizesCollection|null             $new_sizes_collection - An instance of ClassSizesCollection.
	 *
	 * @return ClassLayoutSettingsCollection
	 */
	public function createSettingsCollection( ClassBooleanPropertiesCollection $new_bool_props_collection = null, ClassSizesCollection $new_sizes_collection = null ): ClassLayoutSettingsCollection {

		if ( null === $new_bool_props_collection ) {

			// Has Bool Props Collection been created yet?
			if ( null === $this->new_bool_props_collection ) {
				$this->new_bool_props_collection = $this->newBooleanPropertiesCollection( $this->new_bool_props_defaults );
			}
			$new_bool_props_collection = $this->new_bool_props_collection;
		}
		if ( null === $new_sizes_collection ) {

			// Has Sizes Collection been created yet?
			if ( null === $this->new_sizes_collection ) {
				$this->new_sizes_collection = $this->createSizesCollection();
			}
			$new_sizes_collection = $this->new_sizes_collection;
		}

		$this->new_settings_collection = $this->newSettingsCollection( $new_bool_props_collection, $new_sizes_collection );

		return $this->new_settings_collection;
	}

	/**
	 * Returns the property value of new_settings_collection which is an instance of ClassLayoutSettingsCollection.
	 *
	 * @return SettingsCollection
	 */
	public function getSettingsCollection(): ClassLayoutSettingsCollection {

		return $this->new_settings_collection;
	}

	/**
	 * Creates the instance of ClassLayoutSettingsCollection.
	 *
	 * @param ClassBooleanPropertiesCollection $new_bool_props_collection - Instance of ClassBooleanPropertiesCollection.
	 * @param ClassSizesCollection             $new_sizes_collection      - Instance of ClassSizesCollection.
	 *
	 * @return ClassLayoutSettingsCollection
	 */
	protected function newSettingsCollection( ClassBooleanPropertiesCollection $new_bool_props_collection, ClassSizesCollection $new_sizes_collection ): ClassLayoutSettingsCollection {

		return new ClassLayoutSettingsCollection( $new_bool_props_collection, $new_sizes_collection );
	}
}
