<?php
/**
 * Class ClassBooleanPropertiesDefaults
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Layout
 */

namespace WPezBlockEditor\ThemeJSONSettings\Layout;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesDefaultsBase;

/**
 * Class ClassBooleanPropertiesDefaults extends ClassBooleanPropertiesDefaultsBase.
 */
class ClassBooleanPropertiesDefaults extends ClassBooleanPropertiesDefaultsBase {

	/**
	 * Set property defaults, specialized for this class / Theme JSON settings type.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_defaults = array(

			'bool_allow_editing'                      => array(
				'block_prop' => 'allowEditing',
				'get'        => 'getAllowEditing',
				'default'    => false,
			),
			'bool_allow_custom_content_and_wide_size' => array(
				'block_prop' => 'allowCustomContentAndWideSize',
				'get'        => 'getAllowCustomContentAndWideSize',
				'default'    => false,
			),
		);
	}
}
