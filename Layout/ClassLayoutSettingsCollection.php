<?php
/**
 * Class ClassDimensionsSettingsCollection manages a collection for the Theme JSON settings: Layout.
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Layout
 */

namespace WPezBlockEditor\ThemeJSONSettings\Layout;

use WPezBlockEditor\ThemeJSONSettings\InterfaceCollectionBase;
use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;

/**
 * ClassLayoutSettingsCollection extends ClassCollectionBase.
 */
class ClassLayoutSettingsCollection extends ClassCollectionBase {

	/**
	 * Instance of a class that implements InterfaceCollectionBase.
	 *
	 * @var object
	 */
	protected $obj_booleans;

	/**
	 * Instance of a class that implements InterfaceCollectionBase.
	 *
	 * @var object
	 */
	protected $obj_sizes;

	/**
	 * Class constructor.
	 *
	 * @param InterfaceCollectionBase $booleans Instance of a class that implements InterfaceCollectionBase.
	 * @param InterfaceCollectionBase $sizes Instance of a class that implements InterfaceCollectionBase.
	 */
	public function __construct( InterfaceCollectionBase $booleans, InterfaceCollectionBase $sizes ) {

		$this->obj_booleans = $booleans;
		$this->obj_sizes    = $sizes;

		$this->setProperties();
	}

	/**
	 * Adds an item to the collection.
	 *
	 * @param string $key The array index key of the collection being added.
	 * @param array  $args The additional arguments / values of the collection being added.
	 * - 'key_bools' (string): The key of the boolean collection.
	 * - 'key_sizes' (string): The key of the sizes collection.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {

		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}
		if ( $this->isValid( $args ) ) {

			$arr_props                    = $this->obj_booleans->get( $args['key_bools'] );
			$arr_sizes                    = $this->obj_sizes->get( $args['key_sizes'] );
			$this->arr_collection[ $key ] = array_merge( $arr_props, $arr_sizes );
		}
		return $this;
	}

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The additional arguments / values of the collection being added.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['key_bools'], $args['key_sizes'] )
		|| ! is_string( $args['key_bools'] ) || ! is_string( $args['key_sizes'] ) ) {
			return false;
		}
		return true;
	}
}
