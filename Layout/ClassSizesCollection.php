<?php
/**
 * Class ClassDuotoneCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Layout
 */

namespace WPezBlockEditor\ThemeJSONSettings\Layout;

use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;

/**
 * Class ClassSizesCollection extends ClassCollectionBase.
 */
class ClassSizesCollection extends ClassCollectionBase {

	/**
	 * Adds a new Sizes (content and wide) to the collection.
	 *
	 * @param string $key  The unique identifier for collection being added.
	 * @param array  $args The associative array of additional arguments / values for the duotone.
	 *  - 'content' (string): The contentSize
	 *  - 'wide' (string): The wideSize.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {
		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}

		if ( $this->isValid( $args ) ) {

			$this->arr_collection[ $key ] = array(
				'contentSize' => $args['content'],
				'wideSize'    => $args['wide'],
			);
		}
		return $this;
	}

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The associative array of additional arguments / values for the duotone.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['content'], $args['wide'] )
			|| ! is_string( $args['content'] ) || ! is_string( $args['wide'] ) ) {
			return false;
		}
		return true;
	}
}
