## WPezBlockEditor \ ThemeJSONSettings

__A collection of classes to create and/or manipulate the theme.json's Settings via WordPress' server-side filters.__

If you've read this:

https://developer.wordpress.org/news/2023/07/how-to-modify-theme-json-data-using-server-side-filters/

and you got excited at the possibilities then you've come to the right place. Hopefully.

_Note: This project is currently experimental / POC / WIP. The general idea is sound but I'm still slogging through the Block's docs and the associate devil and his details._

### The WP Filters

- __wp_theme_json_data_default__ - Hooks into the default data provided by WordPress : https://developer.wordpress.org/reference/hooks/wp_theme_json_data_default/

- __wp_theme_json_data_blocks__ - Hooks into the data provided by the blocks : https://developer.wordpress.org/reference/hooks/wp_theme_json_data_blocks/

- __wp_theme_json_data_theme__ - Hooks into the data provided by the theme : https://developer.wordpress.org/reference/hooks/wp_theme_json_data_theme/

- __wp_theme_json_data_user__ - Hooks into the data provided by the user : https://developer.wordpress.org/reference/hooks/wp_theme_json_data_user/

_Note: At this time, I believe the best filter to use is wp_theme_json_data_user, but I've also been experimenting with completely stripped down theme.json (that only has the version property), then everything else is built dynamically server-side using the wp_theme_json_data_theme. But maybe not? 


### ThemeJSONSettings Supported
See: https://developer.wordpress.org/block-editor/reference-guides/theme-json-reference/theme-json-living/#settings


- appearanceTools - TODO / additional discovery needed
- useRootPaddingAwareAlignments - TODO / additional discovery needed
- background
- behaviors - TODO  / additional discovery needed
- border
- color
- custom - TODO  / additional discovery needed
- dimensions
- layout
- lightbox
- position
- shadow
- spacing - TODO working example
- typography



### A Basic Example

```
// Please see the individual READMEs for more detailed examples.
```


### Under The Hood

A given object type (e.g., Shadow - https://developer.wordpress.org/block-editor/reference-guides/theme-json-reference/theme-json-living/#shadow) is composed by two types of properties:

- boolean(s)
- array(s)

Note / TODO - there seem to be instance where null is used, instead of boolean. I'm still exploring those exceptions and how to work with them. 

For boolean there are three abstract classes and an interface (in the root):
- ClassBooleanPropertiesDefaultsBase - Once extended, the concrete implementation will define the boolean properties for the object type.
- ClassBooleanPropertiesBase - Once extended, the concrete implementation takes the concrete Defaults class, sets the actual properties and eventually can return an array of the properties => bool value that'll make up the object type's boolean properties.
- ClassBooleanPropertiesCollectionBase - This is a subclass of the superclass ClassCollectionsBase because Booleans are slightly different. See the code, please.
- InterfaceBooleanPropertiesDefaultsBase - Necessary for injecting into instances of ClassBooleanPropertiesDefaultsBase and ClassBooleanPropertiesBase

For array there is a single abstract classes and an interface (in the root):
- ClassCollectionsBase - Once extended, the concrete implementation will define the properties (with setters and getters), the add() method, as well as the isValid() method.
- InterfaceCollectionsBase - Necessary for injecting into instances of the class that builds out the actual object type's array object. This class is also a collection.  


### FAQ

__1) Why?__

Three reasons. There are plenty of projects where giving the client's content team (or at least certain team members) too much Block setting's power is unnecessary, if not dangerous. Instead, now that it's easier to do, I wanted a definitive and IDE-friendly way to create and manipulate the theme.json. The goal is to: 
- simplify the UX by only exposing a given Block(s)'s settings that are needed;
- allow the content team to focus on getting the content and layout done, with minimal design friction.

For example, for the paragraph block, remove the ability to change the Color of Text, Link and/or Background. Or maybe this simplifying is done for Contributors, but Editors still have access to the Color control(s). Another possibility is to have different color pallets for different blocks. While Block still lack the ability to define / enforce a brand's style guide, this type of level of control is at least a little closer.    

Ultimately, it's about enabling the content team to focus on the content and layout, while the theme maintains the styling (as traditional themes used to do). If it helps, imagine using ACF and flexible content, but for Blocks; kinda sorta. That is, Blocks less as a full-blown page builder where the content team is taxed with setting padding, margins, etc., and more of a basic content and layout tool - sans as much styling as possible.

The second (minor-ish) reason is the json format is fairly limiting. You can't add comments. You can't build a library of JS (sub) objects as building blocks and then stitch those pieces together in the theme.json. That is, reuse of json pieces is not possible. There is no DRY in json. Json can also be messy when using Git, and pull requests / merges. 

The third and final reason is that now any theme.json creation and manipulation can be decouple from the theme and maintained in a free-standing plugin. The theme is the presentation and styling, with the theme.json decoupled. This is generally an architecture that's easier to maintain and reuse. Or perhaps you establish a boilerplate theme.json plugin that your agency doesn't have to reinvent for each build? 

In summary, the benefits are:
- Improved editor UX (by only exposing the-absolutely necessary)
- Less in the UI also means less client education needed
- Empowers engineers with more control of the UX they provide
- Commenting (because it's PHP not json)
- Decoupling the theme.json part of the project into its own dedicated plugin

__2) OK, what else?__

Overall, the basic philosophy is for these classes to ignore any/all block defaults and start with everything turned off (so to speak). With the initial slate being set to none (again, so to speak), the engineering team can be 100% intentional about the block editor experience they need to craft. Make WP's defaults irrelevant. The only things that matter are what engineering says should matter.



### HELPFUL LINKS

- https://developer.wordpress.org/news/2023/07/how-to-modify-theme-json-data-using-server-side-filters/

- https://developer.wordpress.org/block-editor/reference-guides/core-blocks/

- https://developer.wordpress.org/block-editor/reference-guides/theme-json-reference/theme-json-living/


### TODO

- Review the individual READMEs to make sure they're accurate, detailed enough, helpful, etc. 


### CHANGE LOG

- v0.0.0 - Thur 7 Dec 2023
  - Added the concept of a Factory class. This class encapsulates all of the classes needed to create a given object type. This makes it easier to create the object type and its properties. Currently, there are two Factory classes: Layout and Background; more to come.

- v0.0.0 - Tue 28 Nov 2023
  - Lots of bugs fixes, refactoring, README examples, etc. Still keeping it at v0.0.0, but it's finally on the cusp of v0.0.1.

- v0.0.0 - Wed 8 Nov 2023
 - Significant refactoring. Abstraction, collections, interfaces, etc. Still keeping it at v0.0.0, for now.

- v0.0.0 - Thur 5 Oct 2023
  - Add the ClassAutoload to autoload the classes. Staying at v0.0.0, for now.

- v0.0.0 - Wed 4 Oct 2023
  - Cleanup and refactoring. Still keeping it at v0.0.0, for now. 

- v0.0.0 - Wed 13 Sept 2023
  - Finally an initial commit and push.


