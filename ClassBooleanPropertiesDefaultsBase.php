<?php
/**
 * Class ClassBooleanPropertiesDefaultsBase
 *
 * @package WPezBlockEditor\ThemeJSONSettings
 */

namespace WPezBlockEditor\ThemeJSONSettings;

use WPezBlockEditor\ThemeJSONSettings\InterfaceBooleanPropertiesDefaultsBase;

/**
 * Class ClassBooleanPropertiesDefaultsBase implements InterfaceBooleanPropertiesDefaultsBase.
 */
abstract class ClassBooleanPropertiesDefaultsBase implements InterfaceBooleanPropertiesDefaultsBase {

	/**
	 * An associative array of property defaults.
	 * - key: The property name as used in the class
	 * - array: An array of these key => value pairs:
	 * - block_prop: (string) The property name WordPress will use in the JSON.
	 * - get: (string) The method name in the class to get the value of the property.
	 * - default: (bool) The default value of the property.
	 *
	 * @var array
	 */
	protected $arr_defaults;

	/**
	 * The class constructor.
	 */
	public function __construct() {

		$this->setPropertyDefaults();
	}

	/**
	 * In this method, assign the value to the property: arr_defaults.
	 * 
	 * @return void Nothing returned
	 */
	abstract protected function setPropertyDefaults();

	/*
	-----------------

	Sample of that the $arr_defaults array should look like in the setPropertyDefaults() method.

	$this->arr_defaults= array(
		'property_0' => array(
			'prop'    => 'blockPropertyName',
			'get'     => 'getMethodForThisValue',
			'default' => false,
		),
		'property_1' => array(
			'prop'    => 'blockPropertyName',
			'get'     => 'getMethodForThisValue',
			'default' => false,
		),
	);

	-----------------
	 */

	/**
	 * Return the property $arr_defaults array.
	 *
	 * @return array An associative array of property defaults.
	 */
	public function getAll(): array {

		return $this->arr_defaults;
	}

	/**
	 * Return an array of the the block_prop values.
	 *
	 * @return array An array of the the block_prop values.
	 */
	public function getBlockProps(): array {

		$arr_props = array();
		foreach ( $this->arr_defaults as $this_prop => $arr_args ) {

			$arr_props[] = $arr_args['block_prop'];
		}

		return $arr_props;
	}

	/**
	 * Update the default value of a property.
	 *
	 * @param string $this_prop The property name as used in the class.
	 * @param bool  $bool_default The value to assign to the default index
	 *
	 * @return object $this.
	 */
	public function updatePropDefault( string $this_prop, bool $bool_default = true ): object {

		if ( isset( $this->arr_defaults[ $this_prop ] ) ) {
			$this->arr_defaults[ $this_prop ]['default'] = $bool_default;
		}

		return $this;
	}
}
