<?php
/**
 * Class ClassLightboxFactory, manages the creation of the various classes for Theme JSON settings: Lightbox.
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Lightbox
 */

namespace WPezBlockEditor\ThemeJSONSettings\Lightbox;

// Base.
use WPezBlockEditor\ThemeJSONSettings\ClassFactoryBase;
use WPezBlockEditor\ThemeJSONSettings\InterfaceBooleanPropertiesDefaultsBase;

// Settings' Boolean.
use WPezBlockEditor\ThemeJSONSettings\Lightbox\ClassBooleanPropertiesDefaults;
use WPezBlockEditor\ThemeJSONSettings\Lightbox\ClassBooleanProperties;
use WPezBlockEditor\ThemeJSONSettings\Lightbox\ClassBooleanPropertiesCollection;

// Settings' Unique.
use WPezBlockEditor\ThemeJSONSettings\Lightbox\ClassLightboxSettingsCollection as SettingsCollection;

/**
 * ClassLightboxFactory
 */
class ClassLightboxFactory extends ClassFactoryBase {

	/**
	 * Instance of the Lightbox's ClassBooleanPropertiesDefaults
	 *
	 * @return ClassBooleanPropertiesDefaults
	 */
	protected function newBooleanPropertiesDefaults(): ClassBooleanPropertiesDefaults {

		return new ClassBooleanPropertiesDefaults();
	}

	/**
	 * Creates the instance of the Lightbox's ClassBooleanProperties.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults Instance of the Lightbox's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanProperties
	 */
	protected function newBooleanProperties( InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults ): ClassBooleanProperties {

		return new ClassBooleanProperties( $new_bool_props_defaults );
	}

	/**
	 * Creates the instance of the Lightbox's ClassBooleanPropertiesCollection.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props Instance of the Lightbox's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanPropertiesCollection
	 */
	protected function newBooleanPropertiesCollection( InterfaceBooleanPropertiesDefaultsBase $new_bool_props ): ClassBooleanPropertiesCollection {

		return new ClassBooleanPropertiesCollection( $new_bool_props );
	}

	/**
	 * Creates the instance of the Lightbox's ClassLightboxSettingsCollection.
	 *
	 * @param ClassBooleanPropertiesCollection $new_bool_props_collection Instance of the Lightbox's ClassBooleanPropertiesCollection.
	 *
	 * @return SettingsCollection
	 */
	public function createSettingsCollection( ClassBooleanPropertiesCollection $new_bool_props_collection = null ): SettingsCollection {

		if ( null === $new_bool_props_collection ) {
			$this->new_settings_collection = $this->newSettingsCollection( $this->new_bool_props_collection );
		} else {
			$this->new_settings_collection = $this->newSettingsCollection( $new_bool_props_collection );
		}

		return $this->new_settings_collection;
	}

	/**
	 * Returns the property value of new_settings_collection which is an instance of ClassLayoutSettingsCollection.
	 *
	 * @return SettingsCollection
	 */
	public function getSettingsCollection(): SettingsCollection {

		return $this->new_settings_collection;
	}

	/**
	 * Creates the instance of the Lightbox's ClassLightboxSettingsCollection.
	 *
	 * @param ClassBooleanPropertiesCollection $new_bool_props_collection Instance of the Lightbox's ClassBooleanPropertiesCollection.
	 *
	 * @return SettingsCollection
	 */
	protected function newSettingsCollection( $new_bool_props_collection ) {

		return new SettingsCollection( $new_bool_props_collection );
	}
}
