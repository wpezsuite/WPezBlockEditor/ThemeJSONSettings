<?php
/**
 * Class ClassBooleanPropertiesDefaults
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Lightbox
 */

namespace WPezBlockEditor\ThemeJSONSettings\Lightbox;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesDefaultsBase;

/**
 * Class ClassBooleanPropertiesDefaults extends ClassBooleanPropertiesDefaultsBase.
 */
class ClassBooleanPropertiesDefaults extends ClassBooleanPropertiesDefaultsBase {

	/**
	 * Set property defaults, specialized for this class / Theme JSON settings type.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_defaults = array(

			'boo_enabled' => array(
				'block_prop' => 'enabled',
				'get'        => 'getEnabled',
				'default'    => false,
			),
			'bool_allow_editing' => array(
				'block_prop' => 'allowEditing',
				'get'        => 'getAllowEditing',
				'default'    => false,
			),
		);
	}
}
