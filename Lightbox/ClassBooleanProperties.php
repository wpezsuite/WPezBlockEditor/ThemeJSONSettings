<?php
/**
 * Class ClassBooleanProperties
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Lightbox
 */

namespace WPezBlockEditor\ThemeJSONSettings\Lightbox;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesBase;

/**
 * Class ClassBooleanProperties extends ClassBooleanPropertiesBase.
 */
class ClassBooleanProperties extends ClassBooleanPropertiesBase {

	/**
	 * Flag for enabled property of the Theme JSON settings: Lightbox.
	 *
	 * @var boolean
	 */
	protected $bool_enabled;

	/**
	 * Flag for allowEditing property of the Theme JSON settings: Lightbox.
	 *
	 * @var boolean
	 */
	protected $bool_allow_editing;


	/**
	 * Sets the enabled property.
	 *
	 * @param boolean $bool_enabled The value of the enabled property.
	 *
	 * @return object $this
	 */
	public function setEnabled( bool $bool_enabled = true ): object {

		$this->bool_enabled = $bool_enabled;
		return $this;
	}

	/**
	 * Gets the enabled property.
	 *
	 * @return boolean
	 */
	public function getEnabled(): bool {

		return $this->bool_enabled;
	}

	/**
	 * Sets the allowEditing property.
	 *
	 * @param boolean $bool_allow_editing The value of the allowEditing property.
	 *
	 * @return object $this
	 */
	public function setAllowEditing( bool $bool_allow_editing = true ): object {

		$this->bool_allow_editing = $bool_allow_editing;
		return $this;
	}

	/**
	 * Gets the allowEditing property.
	 *
	 * @return boolean
	 */
	public function getAllowEditing(): bool {

		return $this->bool_allow_editing;
	}
}
