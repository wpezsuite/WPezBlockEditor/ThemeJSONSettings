<?php
/**
 * Class ClassBooleanPropertiesCollectionBase
 *
 * @package WPezBlockEditor\ThemeJSONSettings
 */

namespace WPezBlockEditor\ThemeJSONSettings;

/**
 * Class ClassCollectionBase  extends ClassCollectionBase.
 */
abstract class ClassBooleanPropertiesCollectionBase extends ClassCollectionBase {

	/**
	 * Instance of a class that implements InterfaceBooleanPropertiesDefaultsBase.
	 *
	 * @var object
	 */

	protected $obj_boolean_properties_defaults;

	/**
	 * Array from the above object using the getBlockProps() method.
	 *
	 * @var array
	 */
	protected $arr_properties;

	/**
	 * The class constructor.
	 *
	 * @param object $obj_boolean_properties_defaults Instance of a class that implements InterfaceBooleanPropertiesDefaultsBase.
	 */
	public function __construct( InterfaceBooleanPropertiesDefaultsBase $obj_boolean_properties_defaults ) {

		$this->obj_boolean_properties_defaults = $obj_boolean_properties_defaults;

		$this->setProperties();
	}

	/**
	 * Initializes the properties of the collection class.
	 */
	protected function setProperties() {

		$this->arr_collection      = array();
		$this->bool_overwrite_dupe = true;
		$this->arr_properties      = $this->obj_boolean_properties_defaults->getBlockProps();
	}

	/**
	 * Adds a new item to the collection.
	 *
	 * @param string $key  The unique identifier for collection being added.
	 * @param array  $args The associative array of additional arguments / values for the collection being added.
	 * - 'key' is the block property name => value is a boolean value.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {
		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}

		if ( $this->isValid( $args ) ) {
			$this->arr_collection[ $key ] = $args;
		}
		return $this;
	}

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The arguments / pairs of the collection being added.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		// $arr_properties serves as a "required" list of keys
		// Note: there can be additional keys in $args, but they will be ignored later.
		$missing_keys = array_diff( $this->arr_properties, array_keys( $args ) );
		if ( empty( $missing_keys ) ) {
			// Are all the value bool?
			$invalid_values = array_filter(
				$args,
				function ( $value ) {
					return ! is_bool( $value );
				}
			);

			if ( empty( $invalid_values ) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Appends collection items from the provided array to the $key's collection.
	 *
	 * @param array $arr_items The collection of items to append.
	 *
	 * @return $this
	 */
	public function appendCollection( array $arr_items ): object {
		foreach ( $arr_items as $key => $arr_item ) {
			if ( ! $this->isValid( $arr_item ) ) {
				continue;
			}
			$this->add( $key, $arr_item );
		}
		return $this;
	}
}
