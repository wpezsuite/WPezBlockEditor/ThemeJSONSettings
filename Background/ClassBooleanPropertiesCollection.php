<?php
/**
 * Class ClassBooleanPropertiesCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Background
 */

namespace WPezBlockEditor\ThemeJSONSettings\Background;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesCollectionBase;

/**
 * Class ClassFontSizesCollection manages a collection of font sizes.
 */
class ClassBooleanPropertiesCollection extends ClassBooleanPropertiesCollectionBase {
	// It's all about the namespace and having an instance of ClassBooleanPropertiesCollection
	// for this Theme JSON settings type.
}
