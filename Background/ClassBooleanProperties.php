<?php
/**
 * Class ClassBooleanProperties
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Background
 */

namespace WPezBlockEditor\ThemeJSONSettings\Background;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesBase;

/**
 * Class ClassBooleanProperties
 */
class ClassBooleanProperties extends ClassBooleanPropertiesBase {

	/**
	 * Flag for backgroundImage property Theme JSON settings: Background.
	 *
	 * @var boolean
	 */
	protected $bool_background_image;

	/**
	 * Sets the backgroundImage property.
	 *
	 * @param boolean $bool_background_image The value of the backgroundImage property.
	 *
	 * @return object $this
	 */
	public function setBackgroundImage( bool $bool_background_image = true ): object {

		$this->bool_background_image = $bool_background_image;
		return $this;
	}

	/**
	 * Gets the backgroundImage property.
	 *
	 * @return boolean
	 */
	public function getBackgroundImage(): bool {

		return $this->bool_background_image;
	}
}
