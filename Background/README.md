## Theme JSON Settings: Background

__Settings related to Background.__

https://developer.wordpress.org/block-editor/reference-guides/theme-json-reference/theme-json-living/#background

### Basic Example - Traditional

Note: Since the Background object is very simple, this doesn't necessary give you the best indication of the power of these various classes. But it's a start.


```
// We need to create an alias (via "as") for these *Boolean* classes because the class name - but not namespace - is the same 
// across the different settings types. We need a unique name for each.
use WPezBlockEditor\ThemeJSONSettings\Background\ClassBooleanPropertiesDefaults as BackgroundBoolPropsDefs;
use WPezBlockEditor\ThemeJSONSettings\Background\ClassBooleanProperties as BackgroundBoolProps;
use WPezBlockEditor\ThemeJSONSettings\Background\ClassBooleanPropertiesCollection as BackgroundBoolPropsCollection;

use WPezBlockEditor\ThemeJSONSettings\Background\ClassBackgroundSettingsCollection as BackgroundSettingsCollection;


add_filter( 'wp_theme_json_data_user', __NAMESPACE__ . '\fn_wp_theme_json_data_user' );

fn_wp_theme_json_data_user() {
	
    // Instantiate the Background's Boolean Props Defaults.
    // Note: This class has a method: updatePropDefault() for updating the default values.
    // It's possible that you might want to have your own GOTO set of defaults.
    $new_bool_props_defs = new BackgroundBoolPropsDefs();
	
    // Instantiate the Background's Boolean Props and pass in the instance of the Defaults.
    // This class has a method(s) for updating the default values. 
    $new_bool_props = new BackgroundBoolProps( $new_bool_props_defs );
	
    // Instantiate the Background's Boolean Props Collection and pass in the instance of the Bool Props.
    $new_bools_collect_background = new BackgroundBoolPropsCollection( $new_bool_props_defs );

    // Add a collection of bools - 'key_1' - to the collection using the Bool Props instance and the getBooleans() method. 
    $new_bools_collect_background->add( 'key_1', $new_bool_props->getBooleans() );
	
    // Update the value of a specific property, in this case backgourndImage.
    $new_bool_props->setBackgroundImage( true );

    // Add a second collection - ' key_2' - to the collection. 
    $new_bools_collect_background->add( 'key_2', $new_bool_props->getBooleans() );

    // Instantiate the Background Collection and pass in the instance of the Bool Props Collection. 
    $new_type_background = new BackgroundSettingsCollection( $new_bools_collect_background );

    // Add a collection - 'main_1' - along with an array of args that define the collection.
    $new_type_background->add( 'bg_1', array( 'key_bools' => 'key_2' ) );

    // Add a second collection - 'main_2' - along with an array of args that define this collection.
    $new_type_background->add( 'bg_2', array( 'key_bools' => 'key_1' ) );

    // Now use the get() method to get the collection we want to use as part of your Theme JSON
    [Some Theme JSON array...]['background'] = new_type_background->get( 'bg_2' );

}
```

new_type_background->get( 'bg_2' ); will return:

```
array:1 [▶
  'backgroundImage' => true,
]
```

### Basic Example - Using the Factory Class

```

use WPezBlockEditor\ThemeJSONSettings\Background\ClassBackgroundFactory;

add_filter( 'wp_theme_json_data_user', __NAMESPACE__ . '\fn_wp_theme_json_data_user' );

fn_wp_theme_json_data_user() {

    // Instantiate the factory class.
    $back_fact = new ClassBackgroundFactory();
	
    // Get the instance of the Background Boolean Properties class.
    $back_bool_props = $back_fact->getBoolProps();
	
    // Create / get the instance of the Background Boolean Properties Collection class.
    $back_bool_props_coll = $back_fact->createBoolPropsCollection();
	
    // Add a collection, name it 'key_1'
    $back_bool_props_coll->add( 'key_1', $back_bool_props->getBooleans() );
	
    // Update the bool props instance for background image.
    $back_bool_props->setBackgroundImage( true );
	
    // Add another collection, name it 'key_2'
    $back_bool_props_coll->add( 'key_2', $back_bool_props->getBooleans() );

    // Create / get the instance of the Background Settings Collection class.
    $back_coll = $back_fact->createSettingsCollection();

    // Add a collection, name it 'back_1'
    $back_coll->add( 'back_1', array( 'key_bools' => 'key_2' ) );

    // Add another collection, name it 'back_2'
    $back_coll->add( 'back_2', array( 'key_bools' => 'key_1' ) );

    // Use the get() method to get the collection you need when you need it.
    // $back_coll->get( 'back_1' );	
}


```

You might be thinking that this is a lot of work to get back such a small and basic array. In the case of Background, that might be true. That said, you also have control and consistency when you need to set a property (e.g., Background Image) in a number of different places, or the properties of the object type gets updated in the future.

This mindset makes more sense the more complex the object type is. For example, see: Color or Typography.

The fact, that all the booleans default to false is also a very good thing. This forces you / your team to be intentional about what you do want, and what role(s) gets what, where and when. Also, this pro-actively shields you from possible theme and/or core changes in their defaults. Should those change, they won't break the experience you were intentional in creating.  

Regardless, you can't build your own DRY-y reference library of collections using JSON. but you can with these classes.
