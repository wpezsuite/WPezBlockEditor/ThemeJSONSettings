<?php
/**
 * Class ClassBooleanPropertiesDefaults
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Background
 */

namespace WPezBlockEditor\ThemeJSONSettings\Background;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesDefaultsBase;

/**
 * Class ClassBooleanPropertiesDefaults
 */
class ClassBooleanPropertiesDefaults extends ClassBooleanPropertiesDefaultsBase {

	/**
	 * Set property defaults, specialized for this class / Theme JSON settings type.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_defaults = array(

			'bool_background_image' => array(
				'block_prop' => 'backgroundImage',
				'get'        => 'getBackgroundImage',
				'default'    => false,
			),
		);
	}
}
