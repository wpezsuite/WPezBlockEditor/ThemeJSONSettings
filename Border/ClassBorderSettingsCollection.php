<?php
/**
 * Class ClassBorderCollection manages a collection for the Theme JSON object type: Border.
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Border
 */

namespace WPezBlockEditor\ThemeJSONSettings\Border;

use WPezBlockEditor\ThemeJSONSettings\InterfaceCollectionBase;
use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;

/**
 * Class ClassBorderSettingsCollection extends ClassCollectionBase.
 */
class ClassBorderSettingsCollection extends ClassCollectionBase {

	/**
	 * Instance of a class that implements InterfaceCollectionBase.
	 *
	 * @var object
	 */
	protected $obj_booleans;

	/**
	 * Class constructor.
	 *
	 * @param InterfaceCollectionBase $booleans Instance of a class that implements InterfaceCollectionBase.
	 */
	public function __construct( InterfaceCollectionBase $booleans ) {

		$this->obj_booleans = $booleans;

		$this->setProperties();
	}

	/**
	 * Adds a item to the collection.
	 *
	 * @param string $key The array index key of the collection being added.
	 * @param array  $args The additional arguments / values of the collection being added.
	 * - 'key_bools' (string): The key of the boolean collection.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {

		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}
		if ( $this->isValid( $args ) ) {

			$arr_props                    = $this->obj_booleans->get( $args['key_bools'] );
			$this->arr_collection[ $key ] = $arr_props;
		}
		return $this;
	}

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The additional arguments / values of the collection being added.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['key_bools'] ) || ! is_string( $args['key_bools'] ) ) {
			return false;
		}
		return true;
	}
}
