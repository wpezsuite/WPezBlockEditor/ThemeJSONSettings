<?php
/**
 * Class ClassBooleanPropertiesDefaults
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Border
 */

namespace WPezBlockEditor\ThemeJSONSettings\Border;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesDefaultsBase;

/**
 * Class ClassBooleanPropertiesDefaults extends ClassBooleanPropertiesDefaultsBase.
 */
class ClassBooleanPropertiesDefaults extends ClassBooleanPropertiesDefaultsBase {

	/**
	 * Set property defaults, specialized for this class / Theme JSON object type.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_defaults = array(

			'bool_color' => array(
				'block_prop' => 'color',
				'get'        => 'getColor',
				'default'    => false,
			),
			'bool_radius' => array(
				'block_prop' => 'radius',
				'get'        => 'getRadius',
				'default'    => false,
			),
			'bool_style' => array(
				'block_prop' => 'style',
				'get'        => 'getStyle',
				'default'    => false,
			),
			'bool_width' => array(
				'block_prop' => 'width',
				'get'        => 'getWidth',
				'default'    => false,
			),
		);
	}
}
