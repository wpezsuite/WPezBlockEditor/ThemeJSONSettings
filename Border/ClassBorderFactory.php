<?php
/**
 * Class ClassBorderFactory, manages the creation of the various classes for Theme JSON settings: Border.
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Border
 */

namespace WPezBlockEditor\ThemeJSONSettings\Border;

// Base.
use WPezBlockEditor\ThemeJSONSettings\ClassFactoryBase;
use WPezBlockEditor\ThemeJSONSettings\InterfaceBooleanPropertiesDefaultsBase;

// Settings' Boolean.
use WPezBlockEditor\ThemeJSONSettings\Border\ClassBooleanPropertiesDefaults;
use WPezBlockEditor\ThemeJSONSettings\Border\ClassBooleanProperties;
use WPezBlockEditor\ThemeJSONSettings\Border\ClassBooleanPropertiesCollection;

// Settings' Unique.
use WPezBlockEditor\ThemeJSONSettings\Border\ClassBorderSettingsCollection as SettingsCollection;

/**
 * ClassBorderFactory
 */
class ClassBorderFactory extends ClassFactoryBase {

	/**
	 * Instance of the Border's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanPropertiesDefaults
	 */
	protected function newBooleanPropertiesDefaults(): ClassBooleanPropertiesDefaults {

		return new ClassBooleanPropertiesDefaults();
	}

	/**
	 * Creates the instance of the Border's ClassBooleanProperties.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults Instance of the Border's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanProperties
	 */
	protected function newBooleanProperties( InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults ): ClassBooleanProperties {

		return new ClassBooleanProperties( $new_bool_props_defaults );
	}

	/**
	 * Creates the instance of the Border's ClassBooleanPropertiesCollection.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props Instance of the Border's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanPropertiesCollection
	 */
	protected function newBooleanPropertiesCollection( InterfaceBooleanPropertiesDefaultsBase $new_bool_props ): ClassBooleanPropertiesCollection {

		return new ClassBooleanPropertiesCollection( $new_bool_props );
	}

	/**
	 * Creates the instance of the Border's ClassBorderSettingsCollection.
	 *
	 * @param ClassBooleanPropertiesCollection $new_bool_props_collection Instance of the Border's ClassBooleanPropertiesCollection.
	 *
	 * @return SettingsCollection
	 */
	public function createSettingsCollection( ClassBooleanPropertiesCollection $new_bool_props_collection = null ): SettingsCollection {

		if ( null === $new_bool_props_collection ) {
			$this->new_settings_collection = $this->newSettingsCollection( $this->new_bool_props_collection );
		} else {
			$this->new_settings_collection = $this->newSettingsCollection( $new_bool_props_collection );
		}

		return $this->new_settings_collection;
	}

	/**
	 * Creates the instance of the Border's ClassBorderSettingsCollection.
	 *
	 * @param ClassBooleanPropertiesCollection $new_bool_props_collection Instance of the Border's ClassBooleanPropertiesCollection
	 *
	 * @return SettingsCollection
	 */
	protected function newSettingsCollection( $new_bool_props_collection ): SettingsCollection {

		return new SettingsCollection( $new_bool_props_collection );
	}

	/**
	 * Returns the property value of new_settings_collection which is an instance of ClassLayoutSettingsCollection.
	 *
	 * @return SettingsCollection
	 */
	public function getSettingsCollection(): SettingsCollection {

		return $this->new_settings_collection;
	}
}
