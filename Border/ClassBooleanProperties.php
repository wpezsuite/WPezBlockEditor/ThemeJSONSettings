<?php
/**
 * Class ClassBooleanProperties
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Border
 */

namespace WPezBlockEditor\ThemeJSONSettings\Border;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesBase;

/**
 * Class ClassBooleanProperties extends ClassBooleanPropertiesBase.
 */
class ClassBooleanProperties extends ClassBooleanPropertiesBase {

	/**
	 * Flag for color property of the Theme JSON settings: Border.
	 *
	 * @var boolean
	 */
	protected $bool_color;

	/**
	 * Flag for radius property of the Theme JSON settings: Border.
	 *
	 * @var boolean
	 */
	protected $bool_radius;

	/**
	 * Flag for style property of the Theme JSON settings: Border.
	 *
	 * @var boolean
	 */
	protected $bool_style;

	/**
	 * Flag for width property of the Theme JSON settings: Border.
	 *
	 * @var boolean
	 */
	protected $bool_width;

	/**
	 * Sets the color property.
	 *
	 * @param boolean $bool_color The value of the color property.
	 *
	 * @return $this
	 */
	public function setColor( bool $bool_color = true ): object {

		$this->bool_color = $bool_color;
		return $this;
	}

	/**
	 * Gets the color property.
	 *
	 * @return boolean
	 */
	public function getColor(): bool {

		return $this->bool_color;
	}

	/**
	 * Sets the radius property.
	 *
	 * @param boolean $bool_radius The value of the radius property.
	 *
	 * @return $this
	 */
	public function setRadius( bool $bool_radius = true ): object {

		$this->bool_radius = $bool_radius;
		return $this;
	}

	/**
	 * Gets the radius property.
	 *
	 * @return boolean
	 */
	public function getRadius(): bool {

		return $this->bool_radius;
	}

	/**
	 * Sets the style property.
	 *
	 * @param boolean $bool_style The value of the style property.
	 *
	 * @return $this
	 */
	public function setStyle( bool $bool_style = true ): object {

		$this->bool_style = $bool_style;
		return $this;
	}

	/**
	 * Gets the radius property.
	 *
	 * @return boolean
	 */
	public function getStyle(): bool {

		return $this->bool_style;
	}

	/**
	 * Sets the width property.
	 *
	 * @param boolean $bool_width The value of the width property.
	 *
	 * @return $this
	 */
	public function setWidth( bool $bool_width = true ): object {

		$this->bool_width = $bool_width;
		return $this;
	}

	/**
	 * Gets the width property.
	 *
	 * @return boolean
	 */
	public function getWidth(): bool {

		return $this->bool_width;
	}
}
