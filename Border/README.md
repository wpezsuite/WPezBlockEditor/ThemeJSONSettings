## Theme JSON Settings: Border

__Settings related to Borders.__

https://developer.wordpress.org/block-editor/reference-guides/theme-json-reference/theme-json-living/#border

### A Basic Example

Note: Since the Border object is simple,  this doesn't necessary give you the best indication of the power of these various classes. But it's slightly better than the Background example.


```
// We need to create an alias (via "as") for these *Boolean* classes because the class name - but not namespace - is the same 
// across the different settings types. We need a unique name for each.
use WPezBlockEditor\ThemeJSONSettings\Border\ClassBooleanPropertiesDefaults as BorderBoolPropsDefs;
use WPezBlockEditor\ThemeJSONSettings\Border\ClassBooleanProperties as BorderBoolProps;
use WPezBlockEditor\ThemeJSONSettings\Border\ClassBooleanPropertiesCollection as BorderBoolPropsCollection;

use WPezBlockEditor\ThemeJSONSettings\Border\ClassBorderCollection as BorderCollection;


add_filter( 'wp_theme_json_data_user', __NAMESPACE__ . '\fn_wp_theme_json_data_user' );

fn_wp_theme_json_data_user() {
	
	// Instantiate the Border's Boolean Props Defaults.
	// Note: This class has a method: updatePropDefault() for updating the default values.
	// It's possible that you might want to have your own GOTO set of defaults.
	$new_bool_props_defs = new BorderBoolPropsDefs();

	// In fact, let's update the default value of bool_color to true.
	$new_bool_props_defs->updatePropDefault( 'bool_color', true );
	
	// Instantiate the Border's Boolean Props and pass in the instance of the Defaults.
	// This class has a method(s) for updating the default values. 
	$new_bool_props = new BorderBoolProps( $new_bool_props_defs );
	
	// Instantiate the Border's Boolean Props Collection and pass in the instance of the Bool Props.
	$new_bools_collect_border = new BorderBoolPropsCollection( $new_bool_props_defs );
	
	// Add a collection of bools - 'key_1' - to the collection using the Bool Props instance and the getBooleans() method. 
	$new_bools_collect_border->add( 'key_1', $new_bool_props->getBooleans() );

	// Change the width to true.
	$new_bool_props->setWidth( true );
	
	// Add a second collection - ' key_2' - to the collection. 
	$new_bools_collect_border->add( 'key_2', $new_bool_props->getBooleans() );

	// Change the width back to false.
	$new_bool_props->setWidth( false );
	
	// And change the radius to true
	$new_bool_props->setRadius( true );

	// Add a second collection - ' key_3' - to the collection. 
	$new_bools_collect_border->add( 'key_3', $new_bool_props->getBooleans() );

	// Instantiate the Border Collection and pass in the instance of the Bool Props Collection. 
	$new_type_border = new BorderCollection( $new_bools_collect_border );

	// Add a collection - 'brdr_1' - along with an array of args that define the collection.
	$new_type_border->add( 'brdr_1', array( 'key_bools' => 'key_2' ) );

	// Add a second collection - 'main_2' - along with an array of args that define this collection.
	$new_type_border->add( 'brdr_2', array( 'key_bools' => 'key_3' ) );

	// Note: We did add a multiple Boolean collections but - for this example - we're only going to use one.

	// Now use the get() method to get the collection we want to use as part of your Theme JSON
	... ['border'] = new_type_border->get( 'brdr_2' );

}

```

new_type_border->get( 'bg_2' ); will return:

```
array: 4 [▶
  'color' => true,
  'radius' => true,
  'style' => false,
  'width' => false,
]
```

### A Basic Example - Using the Factory Class

```

use WPezBlockEditor\ThemeJSONSettings\Border\ClassBorderFactory;

add_filter( 'wp_theme_json_data_user', __NAMESPACE__ . '\fn_wp_theme_json_data_user' );

fn_wp_theme_json_data_user( $theme_json ) {

    $border_fact = new ClassBorderFactory();

    $border_bool_props = $border_fact->getBoolProps();

    $border_bool_props_coll = $border_fact->createBoolPropsCollection()->add( 'key_1', $border_bool_props->getBooleans() );

    $border_bool_props->setColor( true )->setStyle( true );

    $border_bool_props_coll->add( 'key_2', $border_bool_props->getBooleans() );

	// Note: You do the create sooner and the associated Booleans classes will be auto-created.
	// Then use the get method to get the instance you need.
    $border_coll = $border_fact->createSettingsCollection()->add( 'border_1', array( 'key_bools' => 'key_2' ) )->add( 'border_2', array( 'key_bools' => 'key_1' ) );

    // And then get the collection you need
    // $border_coll->get( 'border_1' );

    // Or
    // $border_coll->get( 'border_2' );
}

```


You might be thinking that this is a lot of work to get back such a small and basic array. In the case of Border, that might be true. That said, you also have control and consistency when you need to set a property (e.g., Border Color) in a number of different places, or the properties of the object type gets updated in the future.

This mindset makes more sense the more complex the object type is. For example, see: Color or Typography.

The fact, that all the booleans default to false is also a very good thing. This forces you / your team to be intentional about what you do want, and what role(s) gets what, where and when. Also, this pro-actively shields you from possible theme and/or core changes in their defaults. Should those change, they won't break the experience you were intentional in creating.  

Regardless, you can't build your own DRY-y reference library of collections using JSON. but you can with these classes.

