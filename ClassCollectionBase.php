<?php
/**
 * Class ClassCollectionBase.
 *
 * @package WPezBlockEditor\ThemeJSONSettings
 */

namespace WPezBlockEditor\ThemeJSONSettings;

/**
 * Class ClassCollectionBase implements InterfaceCollectionBase.
 */
abstract class ClassCollectionBase implements InterfaceCollectionBase {
	/**
	 * The collection of items.
	 *
	 * @var array
	 */
	protected $arr_collection;

	/**
	 * Determines if a duplicate item should be overwritten (true) or simply ignored (false).
	 *
	 * @var bool
	 */
	protected $bool_overwrite_dupe;

	/**
	 * The class constructor.
	 */
	public function __construct() {
		$this->setProperties();
	}

	/**
	 * Initializes the properties of the collection class.
	 */
	protected function setProperties() {

		$this->arr_collection      = array();
		$this->bool_overwrite_dupe = true;
	}

	/**
	 * Adds a item to the collection.
	 *
	 * @param string $key The array index key of the collection being added.
	 * @param array  $args The additional arguments / values of the collection being added.
	 *
	 * @return $this
	 */
	abstract public function add( string $key, array $args ): object;

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The additional arguments / values of the collection being added.
	 *
	 * @return bool
	 */
	abstract public function isValid( array $args ): bool;

	/**
	 * Change the value of the property that determines if duplicate items should be overwritten.
	 *
	 * @param boolean $bool_overwrite_dupe The value to set for the overwrite dupe property.
	 *
	 * @return object
	 */
	public function setOverwriteDupe( bool $bool_overwrite_dupe = true ): object {

		$this->bool_overwrite_dupe = $bool_overwrite_dupe;
		return $this;
	}

	/**
	 * Retrieves an item from the collection based on the provided $key.
	 *
	 * @param string $key The unique identifier for the item in the collection.
	 *
	 * @return array
	 */
	public function get( string $key ): array {
		if ( $this->keyExists( $key ) ) {
			return $this->arr_collection[ $key ];
		}
		return array();
	}

	/**
	 * Determines if a item exists in the collection based on the provided $key.
	 *
	 * @param string $key The slug key to check for.
	 *
	 * @return boolean
	 */
	public function keyExists( string $key ): bool {
		return isset( $this->arr_collection[ $key ] );
	}

	/**
	 * Removes an item from the collection based on the provided $key.
	 *
	 * @param string $key The unique identifier for the collection.
	 *
	 * @return $this
	 */
	public function unset( string $key ): object {
		if ( $this->keyExists( $key ) ) {
			unset( $this->arr_collection[ $key ] );
		}

		return $this;
	}

	/**
	 * Returns the array of keys for the collection.
	 *
	 * @param array $arr_remove_keys The array of keys to remove from the collection's keys. (Optional). This does NOT unset, it simply removes from the returned array.
	 *
	 * @return array The array of keys.
	 */
	public function getKeys( array $arr_remove_keys = array() ): array {
		return array_diff( array_keys( $this->arr_collection ), $arr_remove_keys );
	}

	/**
	 * Returns the entire collection.
	 *
	 * @return array The collection of items.
	 */
	public function getCollection(): array {

		return $this->arr_collection;
	}

	/**
	 * Resets the collection to an empty array.
	 *
	 * @return $this
	 */
	public function resetCollection(): object {

		$this->arr_collection = array();
		return $this;
	}

	/**
	 * Sets the collection of items using the provided array.
	 *
	 * @param array $arr_collection The collection of items.
	 *
	 * @return $this
	 */
	public function setCollection( array $arr_collection ): object {
		$this->resetCollection();
		$this->appendCollection( $arr_collection );
		return $this;
	}

	/**
	 * Appends items from the provided array to the current collection.
	 *
	 * @param array $arr_items The collection of items to append.
	 *
	 * @return $this
	 */
	public function appendCollection( array $arr_items ): object {
		foreach ( $arr_items as $key => $arr_item ) {
			if ( ! $this->isValid( $arr_item ) ) {
				continue;
			}
			$this->add( $key, $arr_item );
		}
		return $this;
	}
}
