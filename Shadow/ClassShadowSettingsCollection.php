<?php
/**
 * Class ClassShadowSettingsCollection manages a collection for the Theme JSON settings: Shadow.
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Shadow
 */

namespace WPezBlockEditor\ThemeJSONSettings\Shadow;

use WPezBlockEditor\ThemeJSONSettings\InterfaceCollectionBase;
use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;

/**
 * ClassShadowSettingsCollection extends ClassCollectionBase.
 */
class ClassShadowSettingsCollection extends ClassCollectionBase {

	/**
	 * Instance of a class that implements InterfaceCollectionBase.
	 *
	 * @var object
	 */
	protected $obj_booleans;

	/**
	 * Instance of a class that implements InterfaceCollectionBase.
	 *
	 * @var object
	 */
	protected $obj_presets;

	/**
	 * Class constructor.
	 *
	 * @param InterfaceCollectionBase $booleans Instance of a class that implements InterfaceCollectionBase.
	 * @param InterfaceCollectionBase $presets Instance of a class that implements InterfaceCollectionBase.
	 */
	public function __construct( InterfaceCollectionBase $booleans, InterfaceCollectionBase $presets ) {

		$this->obj_booleans = $booleans;
		$this->obj_presets  = $presets;

		$this->setProperties();
	}

	/**
	 * Adds an item to the collection.
	 *
	 * @param string $key  The array index key of the collection being added.
	 * @param array  $args The additional arguments / values of the collection being added.
	 * - 'key_bools' (string): The key of the boolean collection.
	 * - 'key_presets' (string): The key of the Presets collection.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {

		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}
		if ( $this->isValid( $args ) ) {

			$arr_props = $this->obj_booleans->get( $args['key_bools'] );

			$arr_props['presets'] = array();
			if ( null !== $this->obj_presets && isset( $args['key_presets'] ) && is_string( $args['key_presets'] ) ) {

				$arr_props['presets'] = $this->obj_presets->get( $args['key_presets'] );
			}

			$this->arr_collection[ $key ] = $arr_props;
		}
		return $this;
	}

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The additional arguments / values of the collection being added.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['key_bools'], $args['key_presets'] )
		|| ! is_string( $args['key_bools'] ) || ! is_string( $args['key_presets'] ) ) {
			return false;
		}
		return true;
	}
}
