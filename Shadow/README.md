## Theme JSON Settings: Shadow

__Settings related to Shadows.__

https://developer.wordpress.org/block-editor/reference-guides/theme-json-reference/theme-json-living/#shadow

### A Basic Example

```

use WPezBlockEditor\ThemeJSONSettings\Shadow\ClassShadowFactory;

add_filter( 'wp_theme_json_data_user', __NAMESPACE__ . '\fn_wp_theme_json_data_user' );

fn_wp_theme_json_data_user( $theme_json ) {

    // Instantiate the ClassShadowFactory
    $shad_fact = new ClassShadowFactory();

    // Get the ClassShadowSettingsCollection
    $shad_set_coll = $shad_set_coll->createSettingsCollection();

    // Get the ClassBooleanProperties
    $shad_bool_props = $shad_set_coll->getBoolProps();

    // Get the ClassBooleanPropertiesCollection
    $shad_bool_props_coll = $shad_set_coll->getBoolPropsCollection();

    //  Get the current array of values from ClassBooleanProperties and add them to the ClassBooleanPropertiesCollection
    $shad_bool_props_coll->add( 'bools_1', $shad_bool_props->getBooleans() );

    // Update one of the values in the ClassBooleanProperties
    $shad_bool_props->setDefaultPresets( true );

    // Add another collection to the ClassBooleanPropertiesCollection 
    $shad_bool_props_coll->add( 'bools_2', $shad_bool_props->getBooleans() );

    // Get the ClassPresetsItemCollection
    shad_presets_item_coll = $shad_set_coll->getPresetsItemCollection();

    // Note: These sample settings from from the Power Studio theme (https://powderstudio.com/)
    $shad_presets_item_coll->add(
        'item_1',
            array(
                'name'   => 'Light',
                'shadow' => '0 0 10px rgb(10, 10, 10, 0.1);',
                'slug'   => 'light',
            )
        );
    $shad_presets_item_coll->add(
        'item_2',
            array(
                'name'   => 'Solid',
                'shadow' => '8px 8px 0 rgb(10, 10, 10, 1)',
                'slug'   => 'solid',
            )
        );

    // Get the ClassPresetsCollection
    $shad_presets_item_coll2 = $shad_set_coll->getPresetsCollection();

    // Using the items, add some presets  to the ClassPresetsCollection
    $shad_presets_item_coll2->add( 'shad_A', array( 'items' => array( 'item_1' ) ) );
    $shad_presets_item_coll2->add( 'shad_B', array( 'items' => array( 'item_2' ) ) );
    $shad_presets_item_coll2->add( 'shad_C', array( 'items' => array( 'item_1', 'item_2' ) ) );

    // Combined it all together and add to the ClassShadowSettingsCollection
    $shad_set_coll->add(
        'shad_test_1',
        array(
            'key_bools'   => 'bools_2',
            'key_presets' => 'shad_C',
		)
	);
    $shad_set_coll->add(
        'shad_test_2',
        array(
            'key_bools'    => 'bools_1',
            'key_presets' => 'shad_A',
        )
    );

    // Get the collection you want to use
    $my_shad = $shad_set_coll->get( 'shad_test_2' );
}


```
