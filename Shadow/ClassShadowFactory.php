<?php
/**
 * Class ClassShadowFactory, manages the creation of the various classes for Theme JSON settings: Shadow.
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Shadow
 */

namespace WPezBlockEditor\ThemeJSONSettings\Shadow;

// Base.
use WPezBlockEditor\ThemeJSONSettings\ClassFactoryBase;
use WPezBlockEditor\ThemeJSONSettings\InterfaceBooleanPropertiesDefaultsBase;

// Settings' Boolean.
use WPezBlockEditor\ThemeJSONSettings\Shadow\ClassBooleanPropertiesDefaults;
use WPezBlockEditor\ThemeJSONSettings\Shadow\ClassBooleanProperties;
use WPezBlockEditor\ThemeJSONSettings\Shadow\ClassBooleanPropertiesCollection;

// Settings' Unique.
use WPezBlockEditor\ThemeJSONSettings\Shadow\ClassPresetsItemCollection;
use WPezBlockEditor\ThemeJSONSettings\Shadow\ClassPresetsCollection;
use WPezBlockEditor\ThemeJSONSettings\Shadow\ClassShadowSettingsCollection as SettingsCollection;


/**
 * ClassBackgroundFactory
 */
class ClassShadowFactory extends ClassFactoryBase {

	/**
	 * Instance of the Shadow's ClassPresetsItemCollection
	 *
	 * @var ClassPresetsItemCollection
	 */
	protected $new_presets_item_collection;

	/**
	 * Instance of the Shadow's ClassPresetsCollection
	 *
	 * @var ClassPresetsCollection
	 */
	protected $new_presets_collection;

	/**
	 * Sets the properties for the class.
	 *
	 * @return void
	 */
	protected function setProperties() {

		parent::setProperties();

		$this->new_presets_item_collection = null;
		$this->new_presets_collection      = null;
	}

	/**
	 * Creates the instance of the Shadow's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanPropertiesDefaults
	 */
	protected function newBooleanPropertiesDefaults(): ClassBooleanPropertiesDefaults {

		return new ClassBooleanPropertiesDefaults();
	}

	/**
	 * Creates the instance of the Shadow's ClassBooleanProperties.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults - Instance of the Shadow's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanProperties
	 */
	protected function newBooleanProperties( InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults ): ClassBooleanProperties {

		return new ClassBooleanProperties( $new_bool_props_defaults );
	}

	/**
	 * Creates the instance of the Shadow's ClassBooleanPropertiesCollection.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props - Instance of the Shadow's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanPropertiesCollection
	 */
	protected function newBooleanPropertiesCollection( InterfaceBooleanPropertiesDefaultsBase $new_bool_props ): ClassBooleanPropertiesCollection {

		return new ClassBooleanPropertiesCollection( $new_bool_props );
	}

	/**
	 * ----- ----- Methods that are unique to Shadow ----- -----
	 */

	/**
	 * Returns the property value of new_presets_item_collection which is an instance of ClassPresetsItemCollection.
	 *
	 * @return ClassPresetsItemCollection
	 */
	public function createPresetsItemCollection(): ClassPresetsItemCollection {

		$this->new_presets_item_collection = $this->newPresetsItemCollection();
		return $this->new_presets_item_collection;
	}

	/**
	 * Instantiates ClassPresetsItemCollection and returns an instance.
	 *
	 * @return ClassSizesCollection {
	 */
	protected function newPresetsItemCollection(): ClassPresetsItemCollection {

		return new ClassPresetsItemCollection();
	}

	/**
	 * Returns the property value of new_presets_item_collection which is an instance of ClassPresetsItemCollection.
	 *
	 * @return ClassPresetsItemCollection
	 */
	public function getPresetsItemCollection(): ClassPresetsItemCollection {

		return $this->new_presets_item_collection;
	}

	/**
	 * Returns the property value of new_presets_item_collection which is an instance of ClassPresetsItemCollection.
	 *
	 * @param ClassPresetsItemCollection|null $new_presets_items_collection - An instance of $ClassPresetsItemCollection.
	 *
	 * @return ClassPresetsItemCollection
	 */
	public function createPresetsCollection( ClassPresetsItemCollection $new_presets_items_collection = null ): ClassPresetsCollection {

		if ( null === $new_presets_items_collection ) {

			if ( null === $this->new_presets_item_collection ) {
				$this->new_presets_item_collection = $this->createPresetsItemCollection();
			}

			$new_presets_items_collection = $this->new_presets_item_collection;
		}

		$this->new_presets_collection = $this->newPresetsCollection( $new_presets_items_collection );
		return $this->new_presets_collection;
	}

	/**
	 * Instantiates ClassPresetsItemCollection and returns an instance.
	 *
	 * @param ClassPresetsItemCollection $new_presets_items_collection instance of ClassPresetsItemCollection.
	 *
	 * @return ClassSizesCollection {
	 */
	protected function newPresetsCollection( ClassPresetsItemCollection $new_presets_items_collection ): ClassPresetsCollection {

		return new ClassPresetsCollection( $new_presets_items_collection );
	}

	/**
	 * Returns the property value of new_presets_item_collection which is an instance of ClassPresetsItemCollection.
	 *
	 * @return ClassPresetsCollection
	 */
	public function getPresetsCollection(): ClassPresetsCollection {

		return $this->new_presets_collection;
	}

	/**
	 * Creates an instance of ClassShadowSettingsCollection, assigns it to $new_settings_collection, and returns that instance.
	 *
	 * @param ClassBooleanPropertiesCollection|null $new_bool_props_collection - An instance of ClassBooleanPropertiesCollection.
	 * @param ClassSizesCollection|null             $new_presets_collection - An instance of ClassSizesCollection.
	 *
	 * @return ClassShadowSettingsCollection
	 */
	public function createSettingsCollection( ClassBooleanPropertiesCollection $new_bool_props_collection = null, ClassPresetsCollection $new_presets_collection = null ): ClassShadowSettingsCollection {

		if ( null === $new_bool_props_collection ) {

			// Has Bool Props Collection been created yet?
			if ( null === $this->new_bool_props_collection ) {
				$this->new_bool_props_collection = $this->newBooleanPropertiesCollection( $this->new_bool_props_defaults );
			}
			$new_bool_props_collection = $this->new_bool_props_collection;
		}

		if ( null === $new_presets_collection ) {

			// Has Presets Collection been created yet?
			if ( null === $this->new_presets_collection ) {
				$this->new_presets_collection = $this->createPresetsCollection();
			}
			$new_presets_collection = $this->new_presets_collection;
		}

		$this->new_settings_collection = $this->newSettingsCollection( $new_bool_props_collection, $new_presets_collection );

		return $this->new_settings_collection;
	}

	/**
	 * Returns the property value of new_settings_collection which is an instance of ClassShadowSettingsCollection.
	 *
	 * @return SettingsCollection
	 */
	public function getSettingsCollection(): ClassShadowSettingsCollection {

		return $this->new_settings_collection;
	}

	/**
	 * Creates the instance of ClassShadowSettingsCollection.
	 *
	 * @param ClassBooleanPropertiesCollection $new_bool_props_collection - Instance of ClassBooleanPropertiesCollection.
	 * @param ClassPresetsCollection           $new_presets_collection    - Instance of ClassPresetsCollection.
	 *
	 * @return ClassShadowSettingsCollection
	 */
	protected function newSettingsCollection( ClassBooleanPropertiesCollection $new_bool_props_collection, ClassPresetsCollection $new_presets_collection ): ClassShadowSettingsCollection {

		return new ClassShadowSettingsCollection( $new_bool_props_collection, $new_presets_collection );
	}
}
