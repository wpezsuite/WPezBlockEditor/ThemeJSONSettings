<?php
/**
 * Class ClassBooleanProperties
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Shadow
 */

namespace WPezBlockEditor\ThemeJSONSettings\Shadow;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesBase;

/**
 * Class ClassBooleanProperties
 */
class ClassBooleanProperties extends ClassBooleanPropertiesBase {

	/**
	 * Flag for defaultPresets property Theme JSON settings: Shadow.
	 *
	 * @var boolean
	 */
	protected $bool_default_presets;

	/**
	 * Sets the defaultPresets property.
	 *
	 * @param boolean $bool_default_presets The value of the defaultPresets property.
	 *
	 * @return object $this
	 */
	public function setDefaultPresets( bool $bool_default_presets = true ): object {

		$this->bool_default_presets = $bool_default_presets;
		return $this;
	}

	/**
	 * Gets the defaultPresets property.
	 *
	 * @return boolean
	 */
	public function getDefaultPresets(): bool {

		return $this->bool_default_presets;
	}

}
