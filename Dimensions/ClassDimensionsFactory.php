<?php
/**
 * Class ClassDimensionsFactory, manages the creation of the various classes for Theme JSON settings: Dimensions.
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Dimensions
 */

namespace WPezBlockEditor\ThemeJSONSettings\Dimensions;

// Base.
use WPezBlockEditor\ThemeJSONSettings\ClassFactoryBase;
use WPezBlockEditor\ThemeJSONSettings\InterfaceBooleanPropertiesDefaultsBase;

// Settings' Boolean.
use WPezBlockEditor\ThemeJSONSettings\Dimensions\ClassBooleanPropertiesDefaults;
use WPezBlockEditor\ThemeJSONSettings\Dimensions\ClassBooleanProperties;
use WPezBlockEditor\ThemeJSONSettings\Dimensions\ClassBooleanPropertiesCollection;

// Settings' Unique.
use WPezBlockEditor\ThemeJSONSettings\Dimensions\ClassDimensionsSettingsCollection as SettingsCollection;

/**
 * ClassDimensionsFactory
 */
class ClassDimensionsFactory extends ClassFactoryBase {

	/**
	 * Instance of the Dimensions' ClassBooleanPropertiesDefaults
	 *
	 * @return ClassBooleanPropertiesDefaults
	 */
	protected function newBooleanPropertiesDefaults(): ClassBooleanPropertiesDefaults {

		return new ClassBooleanPropertiesDefaults();
	}

	/**
	 * Creates the instance of the Dimensions' ClassBooleanProperties.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults Instance of the Dimensions' ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanProperties
	 */
	protected function newBooleanProperties( InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults ): ClassBooleanProperties {

		return new ClassBooleanProperties( $new_bool_props_defaults );
	}

	/**
	 * Creates the instance of the Dimensions' ClassBooleanPropertiesCollection.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props Instance of the Dimensions' ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanPropertiesCollection
	 */
	protected function newBooleanPropertiesCollection( InterfaceBooleanPropertiesDefaultsBase $new_bool_props ): ClassBooleanPropertiesCollection {

		return new ClassBooleanPropertiesCollection( $new_bool_props );
	}

	/**
	 * Creates the instance of the Dimensions' ClassDimensionsSettingsCollection.
	 *
	 * @param ClassBooleanPropertiesCollection $new_bool_props_collection Instance of the Dimensions' ClassBooleanPropertiesCollection.
	 *
	 * @return SettingsCollection
	 */
	public function createSettingsCollection( ClassBooleanPropertiesCollection $new_bool_props_collection = null ): SettingsCollection {

		if ( null === $new_bool_props_collection ) {
			$this->new_settings_collection = $this->newSettingsCollection( $this->new_bool_props_collection );
		} else {
			$this->new_settings_collection = $this->newSettingsCollection( $new_bool_props_collection );
		}

		return $this->new_settings_collection;
	}

	/**
	 * Returns the property value of new_settings_collection which is an instance of ClassLayoutSettingsCollection.
	 *
	 * @return SettingsCollection
	 */
	public function getSettingsCollection(): SettingsCollection {

		return $this->new_settings_collection;
	}

	/**
	 * Creates the instance of the Dimensions' ClassDimensionsSettingsCollection.
	 *
	 * @param ClassBooleanPropertiesCollection $new_bool_props_collection Instance of the Dimensions' ClassBooleanPropertiesCollection.
	 *
	 * @return SettingsCollection
	 */
	protected function newSettingsCollection( $new_bool_props_collection ) {

		return new SettingsCollection( $new_bool_props_collection );
	}
}
