<?php
/**
 * Class ClassBooleanPropertiesCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Dimensions
 */

namespace WPezBlockEditor\ThemeJSONSettings\Dimensions;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesCollectionBase;

/**
 * Class ClassBooleanPropertiesCollection extends ClassBooleanPropertiesCollectionBase.
 */
class ClassBooleanPropertiesCollection extends ClassBooleanPropertiesCollectionBase {
	// It's all about the namespace and having an instance of ClassBooleanPropertiesCollection
	// for this Theme JSON settings type.
}
