<?php
/**
 * Class ClassBooleanPropertiesDefaults
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Dimensions
 */

namespace WPezBlockEditor\ThemeJSONSettings\Dimensions;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesDefaultsBase;

/**
 * Class ClassBooleanPropertiesDefaults extends ClassBooleanPropertiesDefaultsBase.
 */
class ClassBooleanPropertiesDefaults extends ClassBooleanPropertiesDefaultsBase {

	/**
	 * Set property defaults, specialized for this class / Theme JSON settings type.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_defaults = array(

			'bool_min_height' => array(
				'block_prop' => 'minHeight',
				'get'        => 'getMinHeight',
				'default'    => false,
			),
		);
	}
}
