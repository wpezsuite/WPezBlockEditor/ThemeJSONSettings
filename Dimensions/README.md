## Theme JSON Settings: Dimensions

__Settings related to Dimensions.__

https://developer.wordpress.org/block-editor/reference-guides/theme-json-reference/theme-json-living/#dimensions

### A Basic Example

Dimensions is very basic, like Background. Please see the README for Background.

The only difference is that Dimensions uses the property $bool_min_height and associated set and get methods, while background is bool_background_image, etc.