<?php
/**
 * Class ClassBooleanProperties
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Layout
 */

namespace WPezBlockEditor\ThemeJSONSettings\Dimensions;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesBase;

/**
 * Class ClassBooleanProperties
 */
class ClassBooleanProperties extends ClassBooleanPropertiesBase {

	/**
	 * Flag for allowEditing property Theme JSON settings: layout.
	 *
	 * @var boolean
	 */
	protected $bool_min_height;

	/**
	 * Sets the bool_min_height property.
	 *
	 * @param boolean $bool_min_height The value of the bool_min_height property.
	 *
	 * @return object $this
	 */
	public function setMinHeight( bool $bool_min_height = true ): object {

		$this->bool_min_height = $bool_min_height;
		return $this;
	}

	/**
	 * Gets the bool_min_height property.
	 *
	 * @return boolean
	 */
	public function getMinHeight(): bool {

		return $this->bool_min_height;
	}
}
