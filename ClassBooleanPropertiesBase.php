<?php
/**
 * Class ClassBooleanPropertiesBase
 *
 * @package WPezBlockEditor\ThemeJSONSettings
 */

namespace WPezBlockEditor\ThemeJSONSettings;

use WPezBlockEditor\ThemeJSONSettings\InterfaceBooleanPropertiesDefaultsBase;

/**
 * Abstract Base class for managing Boolean properties.
 */
abstract class ClassBooleanPropertiesBase {

	/**
	 * An array of the properties and associated key => value pairs.
	 *  - prop: (string) The property name WordPress will use in the JSON.
	 *  - get: (string) The method name in the class to get the value of the property.
	 *  - default: (boolean) The default value of the property.
	 *
	 * @var array
	 */
	protected $arr_defaults;


	/**
	 * Undocumented variable
	 *
	 * @var object
	 */
	protected $obj_boolean_properties_defaults;


	/**
	 * The class constructor.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $obj_boolean_properties_defaults instance that implements of InterfaceBooleanPropertiesDefaultBase.
	 */
	public function __construct( InterfaceBooleanPropertiesDefaultsBase $obj_boolean_properties_defaults ) {

		$this->obj_boolean_properties_defaults = $obj_boolean_properties_defaults;

		$this->setPropertyDefaults();
		$this->setProperties();
	}

	/**
	 *  Define your arr_defaults.
	 *
	 * @return void Nothing returned
	 */
	protected function setPropertyDefaults() {

		$this->arr_defaults = $this->obj_boolean_properties_defaults->getAll();
	}

	/**
	 * Sets the various Bool properties for the class / instance using the arr_properties array.
	 *
	 * @return void Nothing returned
	 */
	protected function setProperties() {

		foreach ( $this->arr_defaults as $this_prop => $arr_args ) {

			if ( ! is_array( $arr_args ) || ! $this->isValid( $this_prop, $arr_args ) ) {
				continue;
			}
			$this->{$this_prop} = $arr_args['default'];
		}
	}


	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param string $this_prop The property name.
	 * @param array  $arr_args  The additional arguments / values of the collection being added.
	 *
	 * @return bool
	 */
	protected function isValid( string $this_prop, array $arr_args ): bool {

		if ( isset( $arr_args['default'] ) && is_bool( $arr_args['default'] ) && property_exists( $this, $this_prop ) ) {
			return true;
		}
		return false;
	}

	/**
	 * A wrapper for the setProperties() method.
	 *
	 * @return $this
	 */
	public function resetProperties(): object {

		$this->setProperties();
		return $this;
	}

	/**
	 * TODO - Remove? Update the $arr_defaults array via wp_parse_args().
	 *
	 * @param array $arr_args The array of args to be used to update the array.
	 *
	 * @return object $this
	 */
	protected function X_setDefaults( array $arr_args ) {

		// TODO - validation?
		$this->arr_defaults = wp_parse_args( $arr_args, $this->arr_defaults );
		$this->setProperties();
		return $this;
	}

	/**
	 * TODO - Remove? Get the $arr_defaults array.
	 *
	 * @return array
	 */
	protected function X_getDefaultArgs(): array {

		return $this->arr_defaults;
	}

	/**
	 * Gets an array of the properties and associated key => value pairs.
	 *
	 * @return array
	 */
	public function getBooleans(): array {

		$arr_return = array();
		foreach ( $this->arr_defaults as $prop => $arr_args ) {
			if ( ! isset( $arr_args['block_prop'], $arr_args['get'] ) || ! method_exists( $this, $arr_args['get'] ) ) {
				continue;
			}
			$arr_return[ $arr_args['block_prop'] ] = $this->{$arr_args['get']}();
		}

		return $arr_return;
	}
}
