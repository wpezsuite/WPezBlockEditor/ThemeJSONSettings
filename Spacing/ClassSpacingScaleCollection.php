<?php
/**
 * Class ClassPresetsCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Spacing
 */

namespace WPezBlockEditor\ThemeJSONSettings\Spacing;

use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;

/**
 * Class ClassPresetsCollection extends ClassCollectionBase.
 */
class ClassSpacingScaleCollection extends ClassCollectionBase {

	/**
	 * Adds a new Sizes (content and wide) to the collection.
	 *
	 * @param string $key  The unique identifier for collection being added.
	 * @param array  $args The associative array of additional arguments / values for the presets.
	 * - 'steps' (integer): The number of steps.
	 * - 'operator' (string): Optional - The operator.
	 * - 'increment' (float): Optional - The increment.
	 * - 'med_step' (float): Optional - The medium step.
	 * - 'unit' (string): Optional - The unit.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {
		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}

		$arr_valid_keys = array(
			'steps',
			'operator',
			'increment',
			'med_step',
			'unit',
		);

		$args = array_intersect_key( $args, array_flip( $arr_valid_keys ) );

		if ( $this->isValid( $args ) ) {

			if ( 1 === count( $args ) ) {

				$this->arr_collection[ $key ] = array(
					'steps' => 0,
				);

			} else {

				$this->arr_collection[ $key ] = array(
					'steps'      => $args['steps'],
					'operator'   => $args['operator'],
					'increment'  => $args['increment'],
					'mediumStep' => $args['med_step'],
					'unit'       => $args['unit'],
				);
			}
		}
		return $this;
	}

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The associative array of additional arguments / values for the duotone.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		// From the WP core code:
		// "If theme authors want to prevent the generation of the core spacing scale they can set their theme.json spacingScale.steps to 0."
		// File: wp-includes / class-wp-theme-json.php / WP_Theme_JONS / method: set_spacing_sizes() .
		if ( isset( $args['steps'] ) && 1 === count( $args ) ) {
			// There's only a steps key but it's not 0 so return false.
			if ( 0 !== $args['steps'] ) {
				return false;
			}
			return true;
		}

		// Otherwise, we expect all the keys to be set. TODO - verify this is true. Maybe "partical" keys is ok.
		if ( ! isset( $args['steps'], $args['med_step'], $args['unit'], $args['operator'], $args['increment'] )
			|| ! is_int( $args['steps'] ) || is_string( $args['med_step'] ) || ! is_numeric( $args['med_step'] ) || is_string( $args['increment'] ) || ! is_numeric( $args['increment'] ) || ! is_string( $args['unit'] ) || ! is_string( $args['operator'] ) ) {
			return false;
		}
		return true;
	}
}
