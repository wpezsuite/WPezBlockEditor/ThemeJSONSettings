<?php
/**
 * Class ClassPresetsCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Spacing
 */

namespace WPezBlockEditor\ThemeJSONSettings\Spacing;

use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;

/**
 * Class ClassPresetsCollection extends ClassCollectionBase.
 */
class ClassSpacingSizesItemCollection extends ClassCollectionBase {

	/**
	 * Adds a new Spacing Sizes to the collection.
	 *
	 * @param string $key  The unique identifier for collection being added.
	 * @param array  $args The associative array of additional arguments / values for the presets.
	 * - 'name' (string): The name
	 * - 'slug' (string): The slug.
	 * - 'size' (string): The size.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {
		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}

		if ( $this->isValid( $args ) ) {

			$this->arr_collection[ $key ] = array(
				'name' => $args['name'],
				'size' => $args['size'],
				'slug' => $args['slug'],
			);
		}
		return $this;
	}

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The associative array of additional arguments / values for the duotone.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['name'], $args['size'], $args['slug'] )
			|| ! is_string( $args['name'] ) || ! is_string( $args['size'] ) || ! is_string( $args['slug'] ) ) {
			return false;
		}
		return true;
	}
}
