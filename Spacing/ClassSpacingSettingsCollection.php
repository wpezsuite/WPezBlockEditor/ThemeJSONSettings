<?php
/**
 * Class ClassSpacingSettingsCollection manages a collection for the Theme JSON settings: Spacing.
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Spacing
 */

namespace WPezBlockEditor\ThemeJSONSettings\Spacing;

use WPezBlockEditor\ThemeJSONSettings\InterfaceCollectionBase;
use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;

/**
 * ClassSpacingSettingsCollection extends ClassCollectionBase.
 */
class ClassSpacingSettingsCollection extends ClassCollectionBase {

	/**
	 * Instance of a class that implements InterfaceCollectionBase.
	 *
	 * @var object
	 */
	protected $obj_booleans;

	/**
	 * Instance of a class that implements InterfaceCollectionBase.
	 *
	 * @var object
	 */
	protected $obj_units;

	/**
	 * Instance of a class that implements InterfaceCollectionBase.
	 *
	 * @var object
	 */
	protected $obj_spacing_sizes;

	/**
	 * Instance of a class that implements InterfaceCollectionBase.
	 *
	 * @var object
	 */
	protected $obj_spacing_scale;

	/**
	 * Class constructor.
	 *
	 * @param InterfaceCollectionBase $booleans Instance of a class that implements InterfaceCollectionBase.
	 * @param InterfaceCollectionBase $units Instance of a class that implements InterfaceCollectionBase.
	 * @param InterfaceCollectionBase $spacing_sizes Instance of a class that implements InterfaceCollectionBase.
	 * @param InterfaceCollectionBase $spacing_scale Instance of a class that implements InterfaceCollectionBase.
	 */
	public function __construct( InterfaceCollectionBase $booleans, InterfaceCollectionBase $units, InterfaceCollectionBase $spacing_sizes, InterfaceCollectionBase $spacing_scale ) {

		$this->obj_booleans      = $booleans;
		$this->obj_units         = $units;
		$this->obj_spacing_sizes = $spacing_sizes;
		$this->obj_spacing_scale = $spacing_scale;

		$this->setProperties();
	}

	/**
	 * Adds an item to the collection.
	 *
	 * @param string $key The array index key of the collection being added.
	 * @param array  $args The additional arguments / values of the collection being added.
	 * - 'key_bools' (string): The key of the boolean collection.
	 * - 'key_units' (string): The key of the units collection.
	 * - 'key_sizes' (string): Array of keys of the spacing sizes collection.
	 * - 'key_scale' (string): Array of keys of the spacing scale collection.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {

		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}
		if ( $this->isValid( $args ) ) {

			$arr_props                    = $this->obj_booleans->get( $args['key_bools'] );
			$arr_props['units']           = $this->obj_units->get( $args['key_units'] );
			$arr_props['spacingSizes']    = $this->obj_spacing_sizes->get( $args['key_sizes'] );
			$arr_props['spacingScale']    = $this->obj_spacing_scale->get( $args['key_scale'] );
			$this->arr_collection[ $key ] = $arr_props;
		}
		return $this;
	}

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The additional arguments / values of the collection being added.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['key_bools'], $args['key_units'], $args['key_sizes'], $args['key_scale'] )
		|| ! is_string( $args['key_bools'] ) || ! is_string( $args['key_units'] ) || ! is_string( $args['key_sizes'] ) || ! is_string( $args['key_scale'] ) ) {
			return false;
		}
		return true;
	}
}
