<?php
/**
 * Class ClassBooleanProperties
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Spacing
 */

namespace WPezBlockEditor\ThemeJSONSettings\Spacing;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesBase;

/**
 * Class ClassBooleanProperties
 */
class ClassBooleanProperties extends ClassBooleanPropertiesBase {

	/**
	 * Flag for defaultPresets property Theme JSON settings: Spacing.
	 *
	 * @var boolean
	 */
	protected $bool_block_gap;

	/**
	 * Flag for margin property Theme JSON settings: Spacing.
	 *
	 * @var boolean
	 */
	protected $bool_margin;

	/**
	 * Flag for padding property Theme JSON settings: Spacing.
	 *
	 * @var boolean
	 */
	protected $bool_padding;

	/**
	 * Flag for customSpacingSize property Theme JSON settings: Spacing.
	 *
	 * @var boolean
	 */
	protected $bool_custom_spacing_size;

	/**
	 * Set property bool_block_gap.
	 *
	 * @param boolean $val The value to set.
	 *
	 * @return object $this
	 */
	public function setBlockGap( bool $val = true ): object {
		$this->bool_block_gap = $val;
		return $this;
	}

	/**
	 * Get property bool_block_gap.
	 *
	 * @return bool
	 */
	public function getBlockGap(): bool {
		return $this->bool_block_gap;
	}

	/**
	 * Set property bool_margin.
	 *
	 * @param boolean $val The value to set.
	 *
	 * @return object $this
	 */
	public function setMargin( bool $val = true ): object {
		$this->bool_margin = $val;
		return $this;
	}

	/**
	 * Get property bool_margin.
	 *
	 * @return bool
	 */
	public function getMargin(): bool {
		return $this->bool_margin;
	}

	/**
	 * Set property bool_padding.
	 *
	 * @param boolean $val The value to set.
	 *
	 * @return object $this
	 */
	public function setPadding( bool $val = true ): object {
		$this->bool_padding = $val;
		return $this;
	}

	/**
	 * Get property bool_padding.
	 *
	 * @return bool
	 */
	public function getPadding(): bool {
		return $this->bool_padding;
	}

	/**
	 * Set property bool_custom_spacing_size.
	 *
	 * @param boolean $val The value to set.
	 *
	 * @return object $this
	 */
	public function setCustomSpacingSize( bool $val = true ): object {
		$this->bool_custom_spacing_size = $val;
		return $this;
	}

	/**
	 * Get property bool_custom_spacing_size.
	 *
	 * @return bool
	 */
	public function getCustomSpacingSize(): bool {
		return $this->bool_custom_spacing_size;
	}

}
