# ## Theme JSON Settings: Spacing

__Settings related to Spacing.__

https://developer.wordpress.org/block-editor/reference-guides/theme-json-reference/theme-json-living/#spacing

### A Basic Example

```

use WPezBlockEditor\ThemeJSONSettings\Spacing\ClassSpacingFactory;

add_filter( 'wp_theme_json_data_user', __NAMESPACE__ . '\fn_wp_theme_json_data_user' );

fn_wp_theme_json_data_user( $theme_json ) {

    $new_space_fact = new ClassSpacingFactory();

    // If you look at the ClassSpacingFactory class, you'll see that this creates will automatically instantiate all the necessary classes.
	// Then use the appropriate get* method to get the collection you want to use. 
    $new_space_set_coll = $new_space_fact->createSettingsCollection();

    $new_space_bool_props = $new_space_fact->getBoolProps();

    $new_space_bool_props_coll = $new_space_fact->getBoolPropsCollection();

    $new_space_bool_props_coll->add( 'bools_1', $new_space_bool_props->getBooleans() );

    $new_space_bool_props->setMargin( true );

    $new_space_bool_props_coll->add( 'bools_2', $new_space_bool_props->getBooleans() );

    // Rather than have a variable and assign $new_space_fact->getUnitsCollection() to it, we can just chain the method calls.
	// Note: For 'units_1' the '123' will be ignored because it won't pass the validation check and I want to show an example of that.
    $new_space_fact->getUnitsCollection()
	->add(
        'units_1',
        array(
            'units' => array( 'px', 'em', '123' ),
        )
    )->add(
        'units_2',
        array(
            'units' => array( 'px', 'em', 'rem' ),
        )
    );

    $new_space_sizes_item = $new_space_fact->getSpacingSizesItemCollection();

    // Note: This could also be done with an array and a foreach loop.
    $new_space_sizes_item->add(
        'name_1',
        array(
            'name' => '1',
            'slug' => '10',
            'size' => '1rem',
        )
    )->add(
       'name_2',
        array(
            'name' => '2',
            'slug' => '20',
            'size' => 'min(1.5rem, 2vw)',
        )
    )->add(
        'name_3',
        array(
            'name' => '3',
            'slug' => '30',
            'size' => 'min(2.5rem, 3vw)',
        )
    )->add(
        'name_4',
        array(
            'name' => '4',
            'slug' => '40',
            'size' => 'min(4rem, 5vw)',
        )
    )->add(
        'name_5',
        array(
            'name' => '5',
            'slug' => '50',
            'size' => 'min(6.5rem, 8vw)',
        )
    )->add(
        'name_6',
        array(
            'name' => '6',
            'slug' => '60',
            'size' => 'min(10.5rem, 13vw)',
        )
    );

    $new_space_sizes = $new_space_fact->getSpacingSizesCollection();

    $new_space_sizes->add(
        'odd',
        array( 'items' => array( 'name_1', 'name_3', 'name_5' ) )
    )->add(
        'even',
        array( 'items' => array( 'name_2', 'name_4', 'name_6' ) )
    );

    $new_space_scale = $new_space_fact->getSpacingScaleCollection();

    $new_space_scale->add(
        'scale_1',
        array(
            'steps' => 0,
        )
    )->add(
        'scale_2',
        array(
            'steps'     => 0,
            'operator'  => '*',
            'increment' => 1.5,
            'med_step'  => 3.5,
            'unit'      => 'rem',
        )
    );

    $new_space_set_coll->add(
        'space_1',
        array(
            'key_bools' => 'bools_1',
            'key_units' => 'units_1',
            'key_sizes' => 'odd',
            'key_scale' => 'scale_2',
        )
    )->add(
        'space_2',
        array(
            'key_bools' => 'bools_2',
            'key_units' => 'units_2',
            'key_sizes' => 'even',
            'key_scale' => 'scale_1',
        )
    );

    // Add to the theme.json
    $new_space_set_coll->get( 'space_2' );

}

```