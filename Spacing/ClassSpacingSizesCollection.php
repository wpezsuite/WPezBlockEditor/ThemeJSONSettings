<?php
/**
 * Class ClassPresetsCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Spacing
 */

namespace WPezBlockEditor\ThemeJSONSettings\Spacing;

use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;
use WPezBlockEditor\ThemeJSONSettings\InterfaceCollectionBase;

/**
 * Class ClassPresetsCollection extends ClassCollectionBase.
 */
class ClassSpacingSizesCollection extends ClassCollectionBase {

	/**
	 * Instance of a class that implements InterfaceCollectionBase.
	 *
	 * @var object
	 */
	protected $obj_item;

	/**
	 * The class constructor.
	 *
	 * @param InterfaceCollectionBase $item Instance of the ClassPresetsItemCollection.
	 */
	public function __construct( InterfaceCollectionBase $item ) {

		$this->obj_item = $item;
		$this->setProperties();
	}

	/**
	 * Adds a new Sizes (content and wide) to the collection.
	 *
	 * @param string $key  The unique identifier for collection being added.
	 * @param array  $args The associative array of additional arguments / values for the presets.
	 *  - 'items' (array): And array of item keys from the Presets Item Collection to be combined into a Presets collection.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {

		if ( $this->isValid( $args ) ) {

			foreach ( $args['items'] as $item_key ) {
				if ( ! is_string( $item_key ) || empty( $this->obj_item->get( $item_key ) ) ) {
					continue;
				}
				$this->arr_collection[ $key ][] = $this->obj_item->get( $item_key );
			}
		}
		return $this;
	}

	/**
	 * Validates the arguments for the sub-collection being added.
	 *
	 * @param array $args The associative array of additional arguments / values for the duotone.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['items'] ) || ! is_array( $args['items'] ) ) {
			return false;
		}
		return true;
	}
}
