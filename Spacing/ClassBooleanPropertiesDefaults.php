<?php
/**
 * Class ClassBooleanPropertiesDefaults
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Shadow
 */

namespace WPezBlockEditor\ThemeJSONSettings\Spacing;

use WPezBlockEditor\ThemeJSONSettings\ClassBooleanPropertiesDefaultsBase;

/**
 * Class ClassBooleanPropertiesDefaults extends ClassBooleanPropertiesDefaultsBase.
 */
class ClassBooleanPropertiesDefaults extends ClassBooleanPropertiesDefaultsBase {

	/**
	 * Set property defaults, specialized for this class / Theme JSON settings type.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_defaults = array(

			'bool_block_gap'           => array(
				'block_prop' => 'blockGap',
				'get'        => 'getBlockGap',
				'default'    => false,
			),
			'bool_margin'              => array(
				'block_prop' => 'margin',
				'get'        => 'getMargin',
				'default'    => false,
			),
			'bool_padding'             => array(
				'block_prop' => 'padding',
				'get'        => 'getPadding',
				'default'    => false,
			),
			'bool_custom_spacing_size' => array(
				'block_prop' => 'customSpacingSize',
				'get'        => 'getCustomSpacingSize',
				'default'    => false,
			),
		);
	}
}
