<?php
/**
 * Class ClassSpacingFactory, manages the creation of the various classes for Theme JSON settings: Spacing.
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Spacing
 */

namespace WPezBlockEditor\ThemeJSONSettings\Spacing;

// Base.
use WPezBlockEditor\ThemeJSONSettings\ClassFactoryBase;
use WPezBlockEditor\ThemeJSONSettings\InterfaceBooleanPropertiesDefaultsBase;

// Settings' Boolean.
use WPezBlockEditor\ThemeJSONSettings\Spacing\ClassBooleanPropertiesDefaults;
use WPezBlockEditor\ThemeJSONSettings\Spacing\ClassBooleanProperties;
use WPezBlockEditor\ThemeJSONSettings\Spacing\ClassBooleanPropertiesCollection;

// Settings' Unique.
use WPezBlockEditor\ThemeJSONSettings\Spacing\ClassSpacingScaleCollection;
use WPezBlockEditor\ThemeJSONSettings\Spacing\ClassSpacingSizesItemCollection;
use WPezBlockEditor\ThemeJSONSettings\Spacing\ClassSpacingSizesCollection;
use WPezBlockEditor\ThemeJSONSettings\Spacing\ClassUnitsCollection;
use WPezBlockEditor\ThemeJSONSettings\Spacing\ClassSpacingSettingsCollection as SettingsCollection;


/**
 * ClassBackgroundFactory
 */
class ClassSpacingFactory extends ClassFactoryBase {

	/**
	 * Instance of the Spacing's ClassSpacingScaleCollection
	 *
	 * @var ClassSpacingScaleCollection
	 */
	protected $new_spacing_scale_collection;

	/**
	 * Instance of the Spacing's ClassSpacingSizesItemCollection
	 *
	 * @var ClassSpacingSizesItemCollection
	 */
	protected $new_spacing_sizes_item_collection;

	/**
	 * Instance of the Spacing's ClassSpacingSizesCollection
	 *
	 * @var ClassSpacingSizesCollection
	 */
	protected $new_spacing_sizes_collection;

	/**
	 * Instance of the Spacing's ClassUnitsCollection
	 *
	 * @var ClassUnitsCollection
	 */
	protected $new_units_collection;

	/**
	 * Sets the properties for the class.
	 *
	 * @return void
	 */
	protected function setProperties() {

		parent::setProperties();

		$this->new_spacing_scale_collection      = null;
		$this->new_spacing_sizes_item_collection = null;
		$this->new_spacing_sizes_collection      = null;
		$this->new_units_collection              = null;
	}

	/**
	 * Creates the instance of the Spacing's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanPropertiesDefaults
	 */
	protected function newBooleanPropertiesDefaults(): ClassBooleanPropertiesDefaults {

		return new ClassBooleanPropertiesDefaults();
	}

	/**
	 * Creates the instance of the Spacing's ClassBooleanProperties.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults - Instance of the Spacing's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanProperties
	 */
	protected function newBooleanProperties( InterfaceBooleanPropertiesDefaultsBase $new_bool_props_defaults ): ClassBooleanProperties {

		return new ClassBooleanProperties( $new_bool_props_defaults );
	}

	/**
	 * Creates the instance of the Spacing's ClassBooleanPropertiesCollection.
	 *
	 * @param InterfaceBooleanPropertiesDefaultsBase $new_bool_props - Instance of the Spacing's ClassBooleanPropertiesDefaults.
	 *
	 * @return ClassBooleanPropertiesCollection
	 */
	protected function newBooleanPropertiesCollection( InterfaceBooleanPropertiesDefaultsBase $new_bool_props ): ClassBooleanPropertiesCollection {

		return new ClassBooleanPropertiesCollection( $new_bool_props );
	}

	/**
	 * ----- ----- Methods that are unique to Spacing ----- -----
	 */

	/**
	 * Returns the property value of new_spacing_item_collection which is an instance of SpacingScaleCollection.
	 *
	 * @return ClassSpacingScaleCollection
	 */
	public function createSpacingScaleCollection(): ClassSpacingScaleCollection {

		$this->new_spacing_scale_collection = $this->newSpacingScaleCollection();
		return $this->new_spacing_scale_collection;
	}

	/**
	 * Instantiates ClassSpacingScaleCollection and returns an instance.
	 *
	 * @return ClassSpacingScaleCollection {
	 */
	protected function newSpacingScaleCollection(): ClassSpacingScaleCollection {

		return new ClassSpacingScaleCollection();
	}

	/**
	 * Returns the property value of new_spacing_item_collection which is an instance of ClassSpacingScaleCollection.
	 *
	 * @return ClassSpacingScaleCollection
	 */
	public function getSpacingScaleCollection(): ClassSpacingScaleCollection {

		return $this->new_spacing_scale_collection;
	}

	/**
	 * Returns the property value of new_spacing_item_collection which is an instance of ClassSpacingSizesItemCollection.
	 *
	 * @return ClassSpacingSizesItemCollection
	 */
	public function createSpacingSizesItemCollection(): ClassSpacingSizesItemCollection {

		$this->new_spacing_sizes_item_collection = $this->newSpacingSizesItemCollection();
		return $this->new_spacing_sizes_item_collection;
	}

	/**
	 * Instantiates ClassSpacingSizesItemCollection and returns an instance.
	 *
	 * @return ClassSpacingSizesItemCollection {
	 */
	protected function newSpacingSizesItemCollection(): ClassSpacingSizesItemCollection {

		return new ClassSpacingSizesItemCollection();
	}

	/**
	 * Returns the property value of new_spacing_item_collection which is an instance of ClassSpacingSizesItemCollection.
	 *
	 * @return ClassSpacingSizesItemCollection
	 */
	public function getSpacingSizesItemCollection(): ClassSpacingSizesItemCollection {

		return $this->new_spacing_sizes_item_collection;
	}

// -----------------------------------------

	/**
	 * Returns the property value of new_spacing_item_collection which is an instance of ClassSpacingSizesCollection.
	 * 
	 * @param ClassSpacingSizesItemCollection|null $new_spacing_sizes_item_collection - An instance of $ClassSpacingSizesItemCollection.
	 *
	 * @return ClassSpacingSizesCollection
	 */
	public function createSpacingSizesCollection( ClassSpacingSizesItemCollection $new_spacing_sizes_item_collection = null ): ClassSpacingSizesCollection {

		if ( null === $new_spacing_sizes_item_collection ) {

			if ( null === $this->new_spacing_sizes_item_collection ) {
				$this->new_spacing_sizes_item_collection = $this->createSpacingSizesItemCollection();
			}

			$new_spacing_sizes_item_collection = $this->new_spacing_sizes_item_collection;
		}
		$this->new_spacing_sizes_collection = $this->newSpacingSizesCollection( $new_spacing_sizes_item_collection );
		return $this->new_spacing_sizes_collection;
	}

	/**
	 * Instantiates ClassSpacingSizesItemCollection and returns an instance.
	 *
	 * @param ClassSpacingSizesItemCollection $new_spacing_sizes_item_collection - Instance of ClassSpacingSizesItemCollection.
	 *
	 * @return ClassSpacingSizesCollection {
	 */
	protected function newSpacingSizesCollection( ClassSpacingSizesItemCollection $new_spacing_sizes_item_collection ): ClassSpacingSizesCollection {

		return new ClassSpacingSizesCollection( $new_spacing_sizes_item_collection );
	}

	/**
	 * Returns the property value of new_spacing_collection which is an instance of ClassSpacingSizesCollection.
	 *
	 * @return ClassSpacingSizesCollection
	 */
	public function getSpacingSizesCollection(): ClassSpacingSizesCollection {

		return $this->new_spacing_sizes_collection;
	}


	/**
	 * Returns the property value of new_spacing_item_collection which is an instance of ClassUnitsCollection.
	 *
	 * @return ClassUnitsCollection
	 */
	public function createUnitsCollection(): ClassUnitsCollection {

		$this->new_units_collection = $this->newUnitsCollection();
		return $this->new_units_collection;
	}

	/**
	 * Instantiates ClassUnitsCollection and returns an instance.
	 *
	 * @return ClassUnitsCollection {
	 */
	protected function newUnitsCollection(): ClassUnitsCollection {

		return new ClassUnitsCollection();
	}

	/**
	 * Returns the property value of new_spacing_item_collection which is an instance of ClassUnitsCollection.
	 *
	 * @return ClassUnitsCollection
	 */
	public function getUnitsCollection(): ClassUnitsCollection {

		return $this->new_units_collection;
	}

	/**
	 * Creates an instance of ClassSpacingSettingsCollection, assigns it to $new_settings_collection, and returns that instance.
	 *
	 * @param ClassBooleanPropertiesCollection|null $new_bool_props_collection    - An instance of ClassBooleanPropertiesCollection.
	 * @param ClassUnitsCollection|null             $new_units_collection         - An instance of ClassUnitsCollection.
	 * @param ClassSpacingSizesCollection|null      $new_spacing_sizes_collection - An instance of ClassSpacingSizesCollection.
	 * @param ClassSpacingScaleCollection|null      $new_spacing_scale_collection - An instance of ClassSpacingScaleCollection.
	 *
	 * @return ClassSpacingSettingsCollection
	 */
	public function createSettingsCollection( ClassBooleanPropertiesCollection $new_bool_props_collection = null, ClassUnitsCollection $new_units_collection = null, ClassSpacingSizesCollection $new_spacing_sizes_collection = null, ClassSpacingScaleCollection $new_spacing_scale_collection = null ): ClassSpacingSettingsCollection {

		if ( null === $new_bool_props_collection ) {

			// Has Bool Props Collection been created yet?
			if ( null === $this->new_bool_props_collection ) {
				$this->new_bool_props_collection = $this->newBooleanPropertiesCollection( $this->new_bool_props_defaults );
			}
			$new_bool_props_collection = $this->new_bool_props_collection;
		}

		if ( null === $new_units_collection ) {

			// Has Presets Collection been created yet?
			if ( null === $this->new_units_collection ) {
				$this->new_units_collection = $this->createUnitsCollection();
			}
			$new_units_collection = $this->new_units_collection;
		}

		if ( null === $new_spacing_sizes_collection ) {

			// Has Presets Collection been created yet?
			if ( null === $this->new_spacing_sizes_collection ) {
				$this->new_spacing_sizes_collection = $this->createSpacingSizesCollection();
			}
			$new_spacing_sizes_collection = $this->new_spacing_sizes_collection;
		}

		if ( null === $new_spacing_scale_collection ) {

			// Has Presets Collection been created yet?
			if ( null === $this->new_spacing_scale_collection ) {
				$this->new_spacing_scale_collection = $this->createSpacingScaleCollection();
			}
			$new_spacing_scale_collection = $this->new_spacing_scale_collection;
		}

		$this->new_settings_collection = $this->newSettingsCollection( $new_bool_props_collection, $new_units_collection, $new_spacing_sizes_collection, $new_spacing_scale_collection );

		return $this->new_settings_collection;
	}

	/**
	 * Returns the property value of new_settings_collection which is an instance of ClassSpacingSettingsCollection.
	 *
	 * @return SettingsCollection
	 */
	public function getSettingsCollection(): ClassSpacingSettingsCollection {

		return $this->new_settings_collection;
	}

	/**
	 * Creates the instance of ClassSpacingSettingsCollection.
	 *
	 * @param ClassBooleanPropertiesCollection $new_bool_props_collection    - Instance of ClassBooleanPropertiesCollection.
	 * @param ClassUnitsCollection             $new_units_collection         - Instance of ClassUnitsCollection.
	 * @param ClassSpacingSizesCollection      $new_spacing_sizes_collection - Instance of ClassSpacingSizesCollection.
	 * @param ClassSpacingScaleCollection      $new_spacing_scale_collection - Instance of ClassSpacingScaleCollection.
	 *
	 * @return ClassSpacingSettingsCollection
	 */
	protected function newSettingsCollection( ClassBooleanPropertiesCollection $new_bool_props_collection, ClassUnitsCollection $new_units_collection, ClassSpacingSizesCollection $new_spacing_sizes_collection, ClassSpacingScaleCollection $new_spacing_scale_collection ): ClassSpacingSettingsCollection {

		return new ClassSpacingSettingsCollection( $new_bool_props_collection, $new_units_collection, $new_spacing_sizes_collection, $new_spacing_scale_collection );
	}
}
