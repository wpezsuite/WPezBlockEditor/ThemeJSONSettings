<?php
/**
 * Class ClassUnitsCollection
 *
 * @package WPezBlockEditor\ThemeJSONSettings\Spacing
 */

namespace WPezBlockEditor\ThemeJSONSettings\Spacing;

use WPezBlockEditor\ThemeJSONSettings\ClassCollectionBase;

/**
 * Class ClassPresetsCollection extends ClassCollectionBase.
 */
class ClassUnitsCollection extends ClassCollectionBase {

	/**
	 * Adds a new Sizes (content and wide) to the collection.
	 *
	 * @param string $key  The unique identifier for collection being added.
	 * @param array  $args The associative array of additional arguments / values for the presets.
	 * - 'units' (array): An array of the units for the collection being added.
	 *
	 * @return $this
	 */
	public function add( string $key, array $args ): object {
		if ( isset( $this->arr_collection[ $key ] ) && ! $this->bool_overwrite_dupe ) {
			return $this;
		}

		if ( $this->isValid( $args ) ) {

			$arr_valid_units = array(
				'%',
				'ch',
				'cm',
				'em',
				'in',
				'mm',
				'pc',
				'pt',
				'px',
				'rem',
				'vh',
				'vmax',
				'vmin',
				'vw',
			);

			// We'll only add units that are valid.
			$this->arr_collection[ $key ] = array_intersect( $args['units'], $arr_valid_units );

		}
		return $this;
	}

	/**
	 * Validates the arguments for the collection being added.
	 *
	 * @param array $args The associative array of additional arguments / values for the duotone.
	 *
	 * @return bool
	 */
	public function isValid( array $args ): bool {

		if ( ! isset( $args['units'] ) || ! is_array( $args['units'] ) ) {
			return false;
		}
		return true;
	}
}
